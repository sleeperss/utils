#pragma once


#include "savable.hpp"

#include <regex>
#include <stdexcept>
#include <string>
#include <utility>
#include <vector>


// -----------------------------------------------------------------

template<class Parent_>
template<typename... Args_>
savable<Parent_>::savable(std::string const& path, Args_&&... args) :
   Parent_(std::forward<Args_>(args)...),
   m_path(path) {
   reload();
}

template<class Parent_>
savable<Parent_>::~savable() {
   save();
}

template<class Parent_>
void savable<Parent_>::save() const {
   std::filesystem::create_directories(std::regex_replace(m_path, std::regex { "^(.+)/[^/]+" }, "$1"));
   std::ofstream file { m_path + ".swp" };
   std::filesystem::permissions(m_path + ".swp", std::filesystem::perms::owner_all, std::filesystem::perm_options::replace);

   if (!file)
      throw std::runtime_error("impossible to open file : \"" + m_path + "\"");

   std::vector<uint8_t> data {};
   this->serialize(data);

   file.write(reinterpret_cast<char const*>(data.data()), data.size());//fixme
   std::filesystem::rename(m_path + ".swp", m_path);
}

template<class Parent_>
void savable<Parent_>::reload() {
   std::ifstream file { m_path };

   if (file) {
      std::vector<uint8_t> data {};
      file.seekg(0, std::ios::end);
      auto const size = file.tellg();
      file.seekg(0, std::ios::beg);

      data.resize(size);
      file.read(reinterpret_cast<char*>(data.data()), data.size());
      this->unserialize(data);
   }
}

// -----------------------------------------------------------------
