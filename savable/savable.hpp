#pragma once

#include <string>


// -----------------------------------------------------------------

template<class Slz_>
class savable : public Slz_ {//todo check init order
private:
   std::string m_path;

public:
   template<class... Args_>
   savable(std::string const& path, Args_&&...);
   virtual ~savable();

   void save() const;
   void reload();
};

// -----------------------------------------------------------------
