// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------
// --  █████╗ ██╗     ███████╗██╗  ██╗    ██╗  ██╗ ██████╗ ███╗   ███╗███████╗ ----
// -- ██╔══██╗██║     ██╔════╝╚██╗██╔╝    ██║  ██║██╔═══██╗████╗ ████║██╔════╝ ----
// -- ███████║██║     █████╗   ╚███╔╝     ███████║██║   ██║██╔████╔██║█████╗   ----
// -- ██╔══██║██║     ██╔══╝   ██╔██╗     ██╔══██║██║   ██║██║╚██╔╝██║██╔══╝   ----
// -- ██║  ██║███████╗███████╗██╔╝ ██╗    ██║  ██║╚██████╔╝██║ ╚═╝ ██║███████╗ ----
// -- ╚═╝  ╚═╝╚══════╝╚══════╝╚═╝  ╚═╝    ╚═╝  ╚═╝ ╚═════╝ ╚═╝     ╚═╝╚══════╝ ----
// --                                                                          ----
// -----------------------------------------------------    frag_file          ----
// --------------------------------------------------------------------------------
//
//      Copyright 2022, Alexandre GARCIN, All rights reserved.
//
//                                                   Created on 24-09-2022 22:18:50

#pragma once

#include "frag_file.hpp"

#include <fstream>
#include <string>

// clang-format off
/*####################*/namespace frag                      {/*###################*/
// clang-format on

class chunk_t : public slz::slz<chunk_t> {
public:
   ~chunk_t() override;

private:
   explicit chunk_t(std::shared_ptr<file_t> const& file, index_t const& begin);
   using sharable_type_t::create;

   bool post_validate() override;

   std::shared_ptr<file_t> file_;
   indexes_t indexes_;

   friend sharable_type_t;
   friend class file_t;
};

// clang-format off
/*###################*/}// namespace frag                    /*###################*/
// clang-format on