// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------
// --  █████╗ ██╗     ███████╗██╗  ██╗    ██╗  ██╗ ██████╗ ███╗   ███╗███████╗ ----
// -- ██╔══██╗██║     ██╔════╝╚██╗██╔╝    ██║  ██║██╔═══██╗████╗ ████║██╔════╝ ----
// -- ███████║██║     █████╗   ╚███╔╝     ███████║██║   ██║██╔████╔██║█████╗   ----
// -- ██╔══██║██║     ██╔══╝   ██╔██╗     ██╔══██║██║   ██║██║╚██╔╝██║██╔══╝   ----
// -- ██║  ██║███████╗███████╗██╔╝ ██╗    ██║  ██║╚██████╔╝██║ ╚═╝ ██║███████╗ ----
// -- ╚═╝  ╚═╝╚══════╝╚══════╝╚═╝  ╚═╝    ╚═╝  ╚═╝ ╚═════╝ ╚═╝     ╚═╝╚══════╝ ----
// --                                                                          ----
// -----------------------------------------------------    frag_file          ----
// --------------------------------------------------------------------------------
//
//      Copyright 2022, Alexandre GARCIN, All rights reserved.
//
//                                                   Created on 24-09-2022 22:18:50

#include "chunk.hpp"

// clang-format off
/*####################*/namespace frag                      {/*###################*/
// clang-format on

chunk_t::chunk_t(std::shared_ptr<file_t> const& file, index_t const& begin) :
    file_(file) {

   std::vector<std::byte> data;
   data.resize(file_t::chunk_size);

   file_->read(data, begin);
}

chunk_t::~chunk_t() {
}

bool chunk_t::post_validate() {
}

// clang-format off
/*###################*/}// namespace frag                    /*###################*/
// clang-format on