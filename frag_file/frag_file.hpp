// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------
// --  █████╗ ██╗     ███████╗██╗  ██╗    ██╗  ██╗ ██████╗ ███╗   ███╗███████╗ ----
// -- ██╔══██╗██║     ██╔════╝╚██╗██╔╝    ██║  ██║██╔═══██╗████╗ ████║██╔════╝ ----
// -- ███████║██║     █████╗   ╚███╔╝     ███████║██║   ██║██╔████╔██║█████╗   ----
// -- ██╔══██║██║     ██╔══╝   ██╔██╗     ██╔══██║██║   ██║██║╚██╔╝██║██╔══╝   ----
// -- ██║  ██║███████╗███████╗██╔╝ ██╗    ██║  ██║╚██████╔╝██║ ╚═╝ ██║███████╗ ----
// -- ╚═╝  ╚═╝╚══════╝╚══════╝╚═╝  ╚═╝    ╚═╝  ╚═╝ ╚═════╝ ╚═╝     ╚═╝╚══════╝ ----
// --                                                                          ----
// -----------------------------------------------------    frag_file          ----
// --------------------------------------------------------------------------------
//
//      Copyright 2022, Alexandre GARCIN, All rights reserved.
//
//                                                   Created on 24-09-2022 22:18:50

#pragma once

#include "utils/utils.hpp"

#include <fstream>
#include <string>

// clang-format off
/*####################*/namespace frag                      {/*###################*/
// clang-format on

using index_t = std::size_t;
struct indexes_t {
   index_t begin;
   index_t end;
   index_t next;
};

class chunk_t;

class file_t : public sharable<file_t> {
public:
   static constexpr std::size_t chunk_size = 100;

   ~file_t() override;
   using sharable_type_t::create;

   void read(std::vector<std::byte>& data, index_t begin);

private:
   explicit file_t(std::string_view file);

   bool post_validate() override;

   std::weak_ptr<chunk_t> header_;
   std::fstream file_;

   friend sharable_type_t;
};

// clang-format off
/*###################*/}// namespace frag                    /*###################*/
// clang-format on