#pragma once

#include <string>

class Database {
private:
   std::string m_path;

public:
   Database(std::string const& path);
   virtual ~Database();
};

