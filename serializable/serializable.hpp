#pragma once

#include <bit>
#include <utils/utils.inl>

#include <algorithm>
#include <array>
#include <cmath>
#include <condition_variable>
#include <cstddef>
#include <functional>
#include <initializer_list>
#include <map>
#include <memory>
#include <mutex>
#include <ostream>
#include <shared_mutex>
#include <source_location>
#include <stdexcept>
#include <string>
#include <tuple>
#include <type_traits>
#include <unordered_map>
#include <utility>
#include <vector>

// -----------------------------------------------------------------

template<uint8_t size_>
struct size_to_type_;

template<uint8_t size_>
using size_to_type = typename size_to_type_<size_>::type;

template<uint8_t size_>
using bit_size_to_type = size_to_type<static_cast<uint8_t>(std::ceil(size_ / 8.f))>;

template<>
struct size_to_type_<1> {
   using type = uint8_t;
};

template<>
struct size_to_type_<2> {
   using type = uint16_t;
};

template<>
struct size_to_type_<3> {
   using type = uint32_t;
};

template<>
struct size_to_type_<4> {
   using type = uint32_t;
};

template<>
struct size_to_type_<5> {
   using type = uint64_t;
};

template<>
struct size_to_type_<6> {
   using type = uint64_t;
};

template<>
struct size_to_type_<7> {
   using type = uint64_t;
};

template<>
struct size_to_type_<8> {
   using type = uint64_t;
};

/*####################*/ namespace slz { /*###################*/

// -----------------------------------------------------------------

class SerializableBase;

// -----------------------------------------------------------------

template<class Type_>
static constexpr bool is_serializable =
    std::is_base_of_v<SerializableBase, Type_>;

template<class... Types_>
static constexpr bool are_serializable =
    (is_serializable<std::remove_reference_t<Types_>> && ...);

template<class Type_>
concept _serializable = is_serializable<Type_>;

// -----------------------------------------------------------------

template<bool, class...>
struct BitFieldsStorage;

class SerializableBase {
public:
   std::vector<std::byte> serialize() const;
   std::vector<std::byte>& serialize(std::vector<std::byte>& res) const;
   std::size_t unserialize(std::vector<std::byte> const& data);

   virtual std::size_t size() const = 0;

   SerializableBase() = default;
   virtual ~SerializableBase() = default;

protected:
   virtual void serialize(std::byte*&, std::size_t&) const = 0;
   virtual void unserialize(std::byte const*&, std::size_t&) = 0;

   virtual void serialize_init() const;

   template<class Type>
   static inline void serialize_accessor(Type const& serializable, std::byte*& data, std::size_t& left_size);
   template<class Type>
   static inline void unserialize_accessor(Type& serializable, std::byte const*& data, std::size_t& left_size);
   template<class Type>
   static inline void serialize_init_accessor(Type const& serializable);
   template<class Type>
   static inline std::size_t size_accessor(Type const& serializable);

   template<class, typename>
   friend class Serializable_;
};

// -----------------------------------------------------------------

struct pad;

template<class Type_, uint8_t size_>
struct bits {
   using type = Type_;
   static constexpr uint8_t size = size_;
};

template<class... bits_>
struct bitfield;

template<class>
struct is_bits_ { static constexpr bool value = false; };

template<class Type_, uint8_t size_>
struct is_bits_<slz::bits<Type_, size_>> {
   static constexpr bool value = true;
};

template<class Type_>
static constexpr bool is_bits = is_bits_<std::remove_const_t<std::remove_reference_t<Type_>>>::value;

template<class Type_>
concept _bits = is_bits<std::remove_const_t<std::remove_reference_t<Type_>>>;

template<class Type_>
concept _pad = std::is_same_v<slz::pad, Type_>;

// -----------------------------------------------------------------

template<class Type_>
struct conditional_bits {
   using type = Type_ const&;
};

template<class Type_, uint8_t size_>
struct conditional_bits<bits<Type_, size_>> {
   using type = typename bits<Type_, size_>::type&;
};

template<class Type_>
using arguments_reference_deduction_guide =
    std::conditional_t<std::is_fundamental_v<std::remove_reference_t<Type_>>, Type_&,
                       typename conditional_bits<std::remove_const_t<std::remove_reference_t<Type_>>>::type>;

template<class Type_>
using members_reference_deduction_guide = std::conditional_t<std::is_fundamental_v<std::remove_reference_t<Type_>>,
                                                             Type_&,
                                                             Type_>;

// -----------------------------------------------------------------

/*####################*/ namespace bfpck { /*###################*/

template<class Type_>
struct to_bitfield_helper_ {
   using type = std::tuple<Type_>;
};

template<_bits Type_>
struct to_bitfield_helper_<Type_> {
   using type = std::tuple<slz::bitfield<Type_>>;
};

template<class Type_>
using to_bitfield_helper = typename to_bitfield_helper_<Type_>::type;

template<class bits_, class = void>
static constexpr std::size_t get_size = 0;

template<_bits bits_>
static constexpr std::size_t get_size<bits_> = bits_::size;

template<class Result_, class BitField_, class... Types_>
struct pack_bit_fields_;

// --------------------------------------------------------------------------------------------------------------------------- //

template<class Type_>
struct nullptr_else_tuple {
   using type = std::tuple<Type_>;

   template<class Type__>
   static constexpr type get(Type__&& arg) {
      return { std::forward<Type__>(arg) };
   }
};

template<>
struct nullptr_else_tuple<std::nullptr_t> {
   using type = std::nullptr_t;

   template<class Type__>
   static constexpr type get(Type__&& arg) {
      return arg;
   }
};

// --------------------------------------------------------------------------------------------------------------------------- //

template<class Result_, class BitField_, _bits Type_, class... Types_>
struct pack_bit_fields_<Result_, BitField_, Type_, Types_...> {
   using merger = tmplt::merger<BitField_, std::tuple<Type_>, slz::bitfield>;
   using packer = pack_bit_fields_<Result_, typename merger::type, Types_...>;
   using type = typename packer::type;

   template<class Type__, class... Types__>
   static constexpr type pack(Result_ const& result,
                              BitField_ const& bitfield, Type__&& arg,
                              Types__&&... args) {
      return packer::pack(
          result,
          merger::merge(bitfield, std::tuple<Type__> { std::forward<Type__>(arg) }),
          std::forward<Types__>(args)...);
   }
};

template<class Result_, class BitField_, _pad Type_, class... Types_>
struct pack_bit_fields_<Result_, BitField_, Type_, Types_...> {
   using bitfield_helper = nullptr_else_tuple<BitField_>;
   using merger = tmplt::merger<Result_, typename bitfield_helper::type>;
   using packer = pack_bit_fields_<typename merger::type, std::nullptr_t, Types_...>;
   using type = typename packer::type;

   template<class Type__, class... Types__>
   static constexpr type pack(Result_ const& result, BitField_ const& bitfield, Type__&&, Types__&&... args) {
      return packer::pack(merger::merge(result, bitfield_helper::get(bitfield)), nullptr, std::forward<Types__>(args)...);
   }
};

template<class BitField_>
concept _octal_or_pad_followed = std::is_same_v<BitField_, std::nullptr_t> || !(get_size<BitField_> % 8); // non octal bitfields must be followed by slz::pad

template<class Result_, _octal_or_pad_followed BitField_, class Type_, class... Types_>
requires(!is_bits<Type_> && !std::is_same_v<slz::pad, Type_>) struct pack_bit_fields_<Result_, BitField_, Type_, Types_...> {
   using bitfield_helper = nullptr_else_tuple<BitField_>;
   using merger1 = tmplt::merger<typename bitfield_helper::type, std::tuple<Type_>>;
   using merger2 = tmplt::merger<Result_, typename merger1::type>;
   using packer = pack_bit_fields_<typename merger2::type, std::nullptr_t, Types_...>;
   using type = typename packer::type;

   template<class Type__, class... Types__>
   static constexpr type pack(Result_ const& result, BitField_ const& bitfield, Type__&& arg, Types__&&... args) {
      return packer::pack(merger2::merge(result, merger1::merge(bitfield_helper::get(bitfield), std::tuple<Type__> { std::forward<Type__>(arg) })), nullptr, std::forward<Types__>(args)...);
   }
};

template<class Result_, _octal_or_pad_followed BitField_>
struct pack_bit_fields_<Result_, BitField_> {
   using bitfield_helper = nullptr_else_tuple<BitField_>;
   using merger = tmplt::merger<Result_, typename bitfield_helper::type>;
   using type = typename merger::type;

   static constexpr type pack(Result_ const& result, BitField_ const& bitfield) {
      return merger::merge(result, bitfield_helper::get(bitfield));
   }
};

// --------------------------------------------------------------------------------------------------------------------------- //

template<class Parent_, class... Types_>
struct pack_bit_fields__ {
   using packer = pack_bit_fields_<std::nullptr_t, std::nullptr_t, Types_...>;
   using type = typename packer::type;

   template<class... Args__>
   static constexpr type pack(Args__&&... args) {
      return packer::pack(nullptr, nullptr, std::forward<Args__>(args)...);
   }
};

template<class Parent_, class Type_>
requires std::is_same_v<std::remove_const_t<std::remove_reference_t<Parent_>>, std::remove_const_t<std::remove_reference_t<Type_>>>
struct pack_bit_fields__<Parent_, Type_> {
   static constexpr auto& pack(Parent_ const& parent) {
      return parent.m_members;
   }
};

template<class Parent_, class... Types_>
using pack_bit_fields = pack_bit_fields__<Parent_, Types_...>;

/*###################*/ // namespace bfpck
} // namespace bfpck

// -----------------------------------------------------------------

template<typename... Members_>
class Serializable : public virtual SerializableBase {
public:
   using arg_packer_t = bfpck::pack_bit_fields<Serializable<Members_...>, members_reference_deduction_guide<Members_>...>;
   using members_t = typename bfpck::pack_bit_fields<Serializable<Members_...>, members_reference_deduction_guide<Members_>...>::type;

   Serializable() = delete;

   Serializable(arguments_reference_deduction_guide<Members_>... members) :
       m_members(arg_packer_t::pack(static_cast<arguments_reference_deduction_guide<Members_>>(members)...)) {
      //      static_assert((
      //            ((std::is_reference_v<Members_> && !std::is_const_v<std::remove_reference_t<Members_>>) ||
      //             !std::is_fundamental_v<std::remove_reference_t<Members_>>
      //            ) && ...), "fundamental types must be set as non const reference");todo
   }

   //   Serializable(Serializable&&) = delete;
   //   Serializable(Serializable&) = delete;
   //   Serializable(Serializable const&) = delete;

   ~Serializable() override = default;

   using SerializableBase::serialize;
   using SerializableBase::unserialize;

   std::size_t size() const override { // @suppress("No return")
      std::size_t result {};           // @suppress("Missing const-qualification")
      std::apply([&](auto&&... members) {
         result = (size_accessor(members) + ...);
      },
                 m_members);

      return result;
   }

protected:
   members_t m_members;

   void serialize(std::byte*& data, std::size_t& left_size) const override {
      std::apply([&](auto&&... members) {
         (serialize_accessor(members, data, left_size), ...);
      },
                 m_members);
   }

   void unserialize(std::byte const*& data, std::size_t& left_size) override {
      std::apply([&](auto&&... members) {
         (unserialize_accessor(members, data, left_size), ...);
      },
                 m_members);
   }

   void serialize_init() const override {
      std::apply([&](auto&&... members) {
         (serialize_init_accessor(members), ...);
      },
                 m_members);
   }

   friend class SerializableBase;
};

template<typename Type_>
class fundamental;

template<_not_const Type_>
class fundamental<Type_> : public virtual SerializableBase {
public:
   fundamental() = delete;

   fundamental(Type_& member) :
       m_member(member) {
      static_assert(std::is_fundamental_v<std::remove_reference_t<Type_>>, "fundamental types must be fundamental...");
   }

   ~fundamental() override = default;

   using SerializableBase::serialize;
   using SerializableBase::unserialize;

   std::size_t size() const override {
      return sizeof(Type_);
   }

protected:
   Type_& m_member;

   void serialize(std::byte*& data, std::size_t& left_size) const override {
      auto const size_ = size();

      if (left_size < size_)
         throw std::length_error(""); // todo

      for (uint8_t i = 0; i < size_; ++i, ++data)
         *data = reinterpret_cast<std::byte*>(&m_member)[size_ - 1 - i];

      left_size -= size();
   }

   virtual void unserialize(std::byte const*& data, std::size_t& left_size) override {
      auto const size_ = size();

      if (left_size < size_)
         throw std::length_error(""); // todo

      for (uint8_t i = 0; i < size_; ++i, ++data)
         reinterpret_cast<std::byte*>(&m_member)[size_ - 1 - i] = *data;

      left_size -= size_;
   }

   friend class SerializableBase;
};

template<_const Type_>
class fundamental<Type_> : public virtual SerializableBase {
public:
   fundamental(Type_& member) :
       m_member(member) {
      static_assert(std::is_fundamental_v<std::remove_reference_t<Type_>>, "fundamental types must be fundamental...");
   }

   ~fundamental() override = default;

   using SerializableBase::serialize;
   using SerializableBase::unserialize;

   std::size_t size() const override {
      return sizeof(Type_);
   }

protected:
   Type_& m_member;

   void serialize(std::byte*& data, std::size_t& left_size) const override {
      if (left_size < size())
         throw std::length_error(""); // todo

      for (uint8_t i = 0; i < size(); ++i)
         *(data++) = reinterpret_cast<std::byte const*>(&m_member)[size() - 1 - i];

      left_size -= size();
   }

   void unserialize(std::byte const*&, std::size_t&) override {
      llog::error << "hum, we shouldn't be there " << std::source_location::current().file_name() << " [" << __LINE__ << "]..." << std::endl;
      std::terminate();
   }

   friend class SerializableBase;
};

template<>
class fundamental<void> : public virtual SerializableBase {
public:
   fundamental() {
   }

   ~fundamental() override = default;

   using SerializableBase::serialize;
   using SerializableBase::unserialize;

   std::size_t size() const override {
      return 0;
   }

protected:
   void serialize(std::byte*&, std::size_t&) const override {
   }

   void unserialize(std::byte const*&, std::size_t&) override {
   }

   friend class SerializableBase;
};

template<class Type_>
struct lambda : public virtual SerializableBase {
public:
   lambda(const std::function<Type_(uint8_t const*, std::size_t)>& serialize,
          const std::function<void(Type_ const&, uint8_t const*, std::size_t)>& unserialize) :
       m_serialize(serialize),
       m_unserialize(unserialize) {
   }
   virtual ~lambda() = default;

   std::size_t size() const override {
      return size_accessor(m_serialize(nullptr, 0));
   }

   using SerializableBase::serialize;
   using SerializableBase::unserialize;

protected:
   std::function<Type_(uint8_t const*, std::size_t)> m_serialize;
   std::function<void(Type_ const&, uint8_t const*, std::size_t)> m_unserialize;

   void serialize(std::byte*& data, std::size_t& left_size) const override {
      auto const size_ = size();
      if (left_size < size_)
         throw std::length_error(""); // todo

      serialize_accessor(m_serialize(data, left_size), data, left_size);
   }

   void unserialize(std::byte const*& data, std::size_t& left_size) override {
      if (left_size < sizeof(Type_))
         throw std::length_error(""); // todo

      auto result = m_serialize(nullptr, 0);
      unserialize_accessor(result, data, left_size);
      m_unserialize(result, data, left_size);
   }

   friend class SerializableBase;
};

template<>
struct lambda<void> : public virtual SerializableBase {
public:
   lambda(const std::function<void(std::byte*, std::size_t)>& serialize,
          const std::function<void(std::byte const*, std::size_t)>& unserialize) :
       m_serialize(serialize),
       m_unserialize(unserialize) {
   }
   ~lambda() override = default;

   std::size_t size() const override {
      return 0;
   }

   using SerializableBase::serialize;
   using SerializableBase::unserialize;

protected:
   std::function<void(std::byte*, std::size_t)> m_serialize;
   std::function<void(std::byte const*, std::size_t)> m_unserialize;

   void serialize(std::byte*& data, std::size_t& left_size) const override {
      m_serialize(data, left_size);
   }

   void unserialize(std::byte const*& data, std::size_t& left_size) override {
      m_unserialize(data, left_size);
   }

   friend class SerializableBase;
};

template<class Type_, std::size_t size_>
class refarray : public virtual SerializableBase {
public:
   refarray(std::array<Type_, size_>& member) :
       m_member(member) {
   }

   ~refarray() override = default;

   std::size_t size() const override {
      std::size_t size = 0;
      for (auto& member : m_member)
         size += size_accessor(member);

      return size;
   }

   using SerializableBase::serialize;
   using SerializableBase::unserialize;

protected:
   std::array<Type_, size_>& m_member;

   void serialize(std::byte*& data, std::size_t& left_size) const override {
      for (auto const& value : m_member)
         serialize_accessor(value, data, left_size);
   }

   void unserialize(std::byte const*& data, std::size_t& left_size) override {
      for (auto& value : m_member)
         unserialize_accessor(value, data, left_size);
   }

   virtual void serialize_init() const override {
      for (auto& value : m_member)
         serialize_init_accessor(value);
   }

   friend class SerializableBase;
};

template<class Type_, class SType_, class... Params_>
class generic_refvector_t : public virtual SerializableBase {
public:
   using has_next_t = std::function<bool(generic_refvector_t<Type_, SType_, Params_...> const&, std::size_t, uint8_t const*, std::size_t const)>;
   using initializer_t = std::function<Type_(generic_refvector_t<Type_, SType_, Params_...> const&, std::size_t, uint8_t const*, std::size_t const)>;

   generic_refvector_t(
       std::vector<Type_, Params_...>& member,
       has_next_t const& has_next,
       initializer_t const& initializer = [](generic_refvector_t<Type_, SType_, Params_...> const&, std::size_t, uint8_t const*, std::size_t const) -> Type_ { return {}; }) :
       m_member(member),
       has_next(has_next),
       initializer(initializer) {
   }

   virtual ~generic_refvector_t() = default;

   std::size_t size() const override {
      std::size_t size = 0;
      for (auto& member : m_member)
         size += size_accessor(SType_ { member });

      return size;
   }

   using SerializableBase::serialize;
   using SerializableBase::unserialize;

protected:
   std::vector<Type_, Params_...>& m_member;
   has_next_t has_next;
   initializer_t initializer;

   void serialize(std::byte*& data, std::size_t& left_size) const override {
      for (auto& member : m_member)
         serialize_accessor(SType_ { member }, data, left_size);
   }

   void unserialize(std::byte const*& data, std::size_t& left_size) override {
      m_member.clear();
      std::size_t current_index = 0;

      while (has_next(*this, current_index, data, left_size)) {
         m_member.push_back(initializer(*this, current_index, data, left_size));
         SType_ sMember { m_member.back() };
         unserialize_accessor(sMember, data, left_size); // todo valid
         ++current_index;
      }
   }

   virtual void serialize_init() const override {
      for (auto& member : m_member)
         serialize_init_accessor(SType_ { member });
   }

   friend class SerializableBase;
};

template<class Type_, class... Params_>
class generic_refvector_t<Type_, Type_, Params_...> : public virtual SerializableBase {
public:
   using has_next_t = std::function<bool(generic_refvector_t<Type_, Type_, Params_...> const&, std::size_t, uint8_t const*, std::size_t const)>;
   using initializer_t = std::function<Type_(generic_refvector_t<Type_, Type_, Params_...> const&, std::size_t, uint8_t const*, std::size_t const)>;

   generic_refvector_t(
       std::vector<Type_, Params_...>& member,
       has_next_t const& has_next,
       initializer_t const& initializer = [](generic_refvector_t<Type_, Type_, Params_...> const&, std::size_t, uint8_t const*, std::size_t const) -> Type_ { return {}; }) :
       m_member(member),
       has_next(has_next),
       initializer(initializer) {
   }

   virtual ~generic_refvector_t() = default;

   std::size_t size() const override {
      std::size_t size = 0;
      for (auto& member : m_member)
         size += size_accessor(member);

      return size;
   }

   using SerializableBase::serialize;
   using SerializableBase::unserialize;

protected:
   std::vector<Type_, Params_...>& m_member;
   has_next_t has_next;
   initializer_t initializer;

   void serialize(std::byte*& data, std::size_t& left_size) const override {
      for (auto const& member : m_member)
         serialize_accessor(member, data, left_size);
   }

   void unserialize(std::byte const*& data, std::size_t& left_size) override {
      m_member.clear();
      std::size_t current_index = 0;

      while (has_next(*this, current_index, data, left_size)) {
         m_member.push_back(initializer(*this, current_index, data, left_size));
         unserialize_accessor(m_member.back(), data, left_size); // todo valid
         ++current_index;
      }
   }

   virtual void serialize_init() const override {
      for (auto const& member : m_member)
         serialize_init_accessor(member);
   }

   friend class SerializableBase;
};

template<class Value_, class SValue_, class Size_ = std::size_t, class... Params_>
class generic_sized_refvector_t : public Serializable<lambda<Size_>, generic_refvector_t<Value_, SValue_, Params_...>> {
public:
   using parent_vector_t = generic_refvector_t<Value_, SValue_, Params_...>;
   using parent_t = Serializable<lambda<Size_>, parent_vector_t>;

   generic_sized_refvector_t(std::vector<Value_, Params_...>& member) :
       parent_t({ [&](uint8_t const*, std::size_t) -> std::size_t {
                    return m_size = member.size();
                 },
                  [&](Size_ const& size, uint8_t const*, std::size_t) {
                     m_size = size;
                  } },
                { member,
                  [&](parent_vector_t const&, std::size_t current_index, uint8_t const*, std::size_t) -> bool {
                     return current_index < m_size;
                  } }),
       m_member(member) {
   }

   generic_sized_refvector_t(generic_sized_refvector_t const& sized_refvector) :
       parent_t({ [&](uint8_t const*, std::size_t) -> std::size_t {
                    return m_size = m_member.size();
                 },
                  [&](Size_ const& size, uint8_t const*, std::size_t) {
                     m_size = size;
                  } },
                { sized_refvector.m_member,
                  [&](const parent_vector_t&, std::size_t current_index, uint8_t const*, std::size_t) -> bool {
                     return current_index < m_size;
                  } }),
       m_member(sized_refvector.m_member) {
   }

   generic_sized_refvector_t(generic_sized_refvector_t&& sized_refvector) :
       generic_sized_refvector_t(sized_refvector) {
   }
   generic_sized_refvector_t(generic_sized_refvector_t& sized_refvector) :
       generic_sized_refvector_t(const_cast<generic_sized_refvector_t const&>(sized_refvector)) {
   }

   virtual ~generic_sized_refvector_t() = default;

protected:
   Size_ m_size {};
   std::vector<Value_, Params_...>& m_member;

   friend class SerializableBase;
};

template<class Key_, class Value_, class MapType_, class SValue_, class SKey_>
class generic_refmap_t : public virtual SerializableBase {
public:
   using this_t = generic_refmap_t<Key_, Value_, MapType_, SValue_, SKey_>;
   using has_next_t = std::function<bool(const this_t&, std::size_t, uint8_t const*, std::size_t)>;
   using initializer_t = std::function<std::pair<Key_, Value_>(const this_t&, std::size_t, uint8_t const*, std::size_t)>;

   generic_refmap_t(
       MapType_& member,
       has_next_t const& has_next,
       initializer_t const& initializer = [](const this_t&, std::size_t, uint8_t const*, std::size_t) -> std::pair<Key_, Value_> { return {}; }) :
       m_member(member),
       has_next(has_next),
       initializer(initializer) {
   }

   virtual ~generic_refmap_t() = default;

   std::size_t size() const override {
      std::size_t size = 0;
      for (auto& [key, value] : m_member) {
         size += size_accessor(SKey_ { key });
         size += size_accessor(SValue_ { value });
      }

      return size;
   }

   using SerializableBase::serialize;
   using SerializableBase::unserialize;

protected:
   MapType_& m_member;
   has_next_t has_next;
   initializer_t initializer;

   void serialize(std::byte*& data, std::size_t& left_size) const override {
      for (auto& [key, value] : m_member) {
         serialize_accessor(SKey_ { key }, data, left_size);
         serialize_accessor(SValue_ { value }, data, left_size);
      }
   }

   void unserialize(std::byte const*& data, std::size_t& left_size) override {
      m_member.clear();
      std::size_t current_index = 0;
      while (has_next(*this, current_index, data, left_size)) {
         auto [key, value] = initializer(*this, current_index, data, left_size);
         SKey_ sKey { key };
         SValue_ sValue { value };
         unserialize_accessor(sKey, data, left_size);
         unserialize_accessor(sValue, data, left_size); // todo valid

         m_member.emplace(key, value);
         ++current_index;
      }
   }

   virtual void serialize_init() const override {
      for (auto& [key, value] : m_member) {
         serialize_init_accessor(SKey_ { key });
         serialize_init_accessor(SValue_ { value });
      }
   }

   friend class SerializableBase;
};

template<class Key_, class Value_, class MapType_>
class generic_refmap_t<Key_, Value_, MapType_, Value_, Key_> : public virtual SerializableBase {
public:
   using this_t = generic_refmap_t<Key_, Value_, MapType_, Value_, Key_>;
   using has_next_t = std::function<bool(const this_t&, std::size_t, uint8_t const*, std::size_t)>;
   using initializer_t = std::function<std::pair<Key_, Value_>(const this_t&, std::size_t, uint8_t const*, std::size_t)>;

   generic_refmap_t(
       MapType_& member,
       has_next_t const& has_next,
       initializer_t const& initializer = [](const this_t&, std::size_t, uint8_t const*, std::size_t) -> std::pair<Key_, Value_> { return {}; }) :
       m_member(member),
       has_next(has_next),
       initializer(initializer) {
   }

   virtual ~generic_refmap_t() = default;

   std::size_t size() const override {
      std::size_t size = 0;
      for (auto& [key, value] : m_member) {
         size += size_accessor(key);
         size += size_accessor(value);
      }

      return size;
   }

   using SerializableBase::serialize;
   using SerializableBase::unserialize;

protected:
   MapType_& m_member;
   has_next_t has_next;
   initializer_t initializer;

   void serialize(std::byte*& data, std::size_t& left_size) const override {
      for (auto const& [key, value] : m_member) {
         serialize_accessor(key, data, left_size);
         serialize_accessor(value, data, left_size);
      }
   }

   void unserialize(std::byte const*& data, std::size_t& left_size) override {
      m_member.clear();
      std::size_t current_index = 0;
      while (has_next(*this, current_index, data, left_size)) {
         auto [key, value] = initializer(*this, current_index, data, left_size);
         unserialize_accessor(key, data, left_size);
         unserialize_accessor(value, data, left_size); // todo valid

         m_member.emplace(key, value);
         ++current_index;
      }
   }

   virtual void serialize_init() const override {
      for (auto const& [key, value] : m_member) {
         serialize_init_accessor(key);
         serialize_init_accessor(value);
      }
   }

   friend class SerializableBase;
};

template<class Key_, class Value_, class MapType_, class SKey_>
class generic_refmap_t<Key_, Value_, MapType_, Value_, SKey_> : public virtual SerializableBase {
public:
   using this_t = generic_refmap_t<Key_, Value_, MapType_, Value_, SKey_>;
   using has_next_t = std::function<bool(const this_t&, std::size_t, uint8_t const*, std::size_t)>;
   using initializer_t = std::function<std::pair<Key_, Value_>(const this_t&, std::size_t, uint8_t const*, std::size_t)>;

   generic_refmap_t(
       MapType_& member,
       has_next_t const& has_next,
       initializer_t const& initializer = [](const this_t&, std::size_t, uint8_t const*, std::size_t) -> std::pair<Key_, Value_> { return {}; }) :
       m_member(member),
       has_next(has_next),
       initializer(initializer) {
   }

   virtual ~generic_refmap_t() = default;

   std::size_t size() const override {
      std::size_t size = 0;
      for (auto& [key, value] : m_member) {
         size += size_accessor(SKey_ { key });
         size += size_accessor(value);
      }

      return size;
   }

   using SerializableBase::serialize;
   using SerializableBase::unserialize;

protected:
   MapType_& m_member;
   has_next_t has_next;
   initializer_t initializer;

   void serialize(std::byte*& data, std::size_t& left_size) const override {
      for (auto& [key, value] : m_member) {
         serialize_accessor(SKey_ { key }, data, left_size);
         serialize_accessor(value, data, left_size);
      }
   }

   void unserialize(std::byte const*& data, std::size_t& left_size) override {
      m_member.clear();
      std::size_t current_index = 0;
      while (has_next(*this, current_index, data, left_size)) {
         auto [key, value] = initializer(*this, current_index, data, left_size);
         SKey_ sKey { key };
         unserialize_accessor(sKey, data, left_size);
         unserialize_accessor(value, data, left_size); // todo valid

         m_member.emplace(key, value);
         ++current_index;
      }
   }

   virtual void serialize_init() const override {
      for (auto& [key, value] : m_member) {
         serialize_init_accessor(SKey_ { key });
         serialize_init_accessor(value);
      }
   }

   friend class SerializableBase;
};

template<class Key_, class Value_, class MapType_, class SValue_>
class generic_refmap_t<Key_, Value_, MapType_, SValue_, Key_> : public virtual SerializableBase {
public:
   using this_t = generic_refmap_t<Key_, Value_, MapType_, SValue_, Key_>;
   using has_next_t = std::function<bool(const this_t&, std::size_t, uint8_t const*, std::size_t)>;
   using initializer_t = std::function<std::pair<Key_, Value_>(const this_t&, std::size_t, uint8_t const*, std::size_t)>;

   generic_refmap_t(
       MapType_& member,
       has_next_t const& has_next,
       initializer_t const& initializer = [](const this_t&, std::size_t, uint8_t const*, std::size_t) -> std::pair<Key_, Value_> { return {}; }) :
       m_member(member),
       has_next(has_next),
       initializer(initializer) {
   }

   virtual ~generic_refmap_t() = default;

   std::size_t size() const override {
      std::size_t size = 0;
      for (auto& [key, value] : m_member) {
         size += size_accessor(key);
         size += size_accessor(SValue_ { value });
      }

      return size;
   }

   using SerializableBase::serialize;
   using SerializableBase::unserialize;

protected:
   MapType_& m_member;
   has_next_t has_next;
   initializer_t initializer;

   void serialize(std::byte*& data, std::size_t& left_size) const override {
      for (auto& [key, value] : m_member) {
         serialize_accessor(key, data, left_size);
         serialize_accessor(SValue_ { value }, data, left_size);
      }
   }

   void unserialize(std::byte const*& data, std::size_t& left_size) override {
      m_member.clear();
      std::size_t current_index = 0;
      while (has_next(*this, current_index, data, left_size)) {
         auto [key, value] = initializer(*this, current_index, data, left_size);
         SValue_ sValue { value };
         unserialize_accessor(key, data, left_size);
         unserialize_accessor(sValue, data, left_size); // todo valid

         m_member.emplace(key, value);
         ++current_index;
      }
   }

   virtual void serialize_init() const override {
      for (auto& [key, value] : m_member) {
         serialize_init_accessor(key);
         serialize_init_accessor(SValue_ { value });
      }
   }

   friend class SerializableBase;
};

template<class Key_, class Value_, class MapType_, class Size_ = std::size_t, class SValue_ = typename MapType_::value_type, class SKey_ = typename MapType_::key_type>
class generic_sized_refmap_t : public Serializable<lambda<Size_>, generic_refmap_t<Key_, Value_, MapType_, SValue_, SKey_>> {
public:
   using parent_map_t = generic_refmap_t<Key_, Value_, MapType_, SValue_, SKey_>;
   using parent_t = Serializable<lambda<Size_>, parent_map_t>;

   generic_sized_refmap_t(MapType_& member) :
       parent_t({ [&](uint8_t const*, std::size_t) -> std::size_t {
                    return m_size = m_member.size();
                 },
                  [&](Size_ const& size, uint8_t const*, std::size_t) {
                     m_size = size;
                  } },
                { member,
                  [&](const parent_map_t&, std::size_t current_index, uint8_t const*, std::size_t) -> bool {
                     return current_index < m_size;
                  } }),
       m_member(member) {
   }

   generic_sized_refmap_t(generic_sized_refmap_t&& sized_refmap) = delete;
   generic_sized_refmap_t(generic_sized_refmap_t const& sized_refmap) :
       parent_t({ [&](uint8_t const*, std::size_t) -> std::size_t {
                    return m_size = m_member.size();
                 },
                  [&](Size_ const& size, uint8_t const*, std::size_t) {
                     m_size = size;
                  } },
                { sized_refmap.m_member,
                  [&](const parent_map_t&, std::size_t current_index, uint8_t const*, std::size_t) -> bool {
                     return current_index < m_size;
                  } }),
       m_member(sized_refmap.m_member) {
   }

   generic_sized_refmap_t& operator=(generic_sized_refmap_t&& sized_refmap) = delete;
   generic_sized_refmap_t& operator=(generic_sized_refmap_t const& sized_refmap) = delete;

   virtual ~generic_sized_refmap_t() {
   }

protected:
   Size_ m_size {};
   MapType_& m_member;

   friend class SerializableBase;
};

template<class Key_, class Value_, class MapType_, class SValue_ = typename MapType_::value_type, class SKey_ = typename MapType_::key_type>
class generic_map_t : public MapType_, public generic_refmap_t<Key_, Value_, MapType_, SValue_, SKey_> {
public:
   using parent_t = generic_refmap_t<Key_, Value_, MapType_, SValue_, SKey_>;

   generic_map_t(
       typename parent_t::has_next_t const& has_next,
       typename parent_t::initializer_t const& initializer = [](const parent_t&, std::size_t, uint8_t const*, std::size_t) -> std::pair<Key_, Value_> { return {}; }) :
       parent_t(*this, has_next, initializer) {
   }

   generic_map_t(generic_map_t const& map) :
       parent_t(*this, map.has_next, map.initializer) {
   }

   generic_map_t(generic_map_t&& map) :
       parent_t(*this, std::move(map.has_next), std::move(map.initializer)) {
   }

   virtual ~generic_map_t() = default;

   using parent_t::serialize;
   using parent_t::serialize_init;
   using parent_t::size;
   using parent_t::unserialize;
};

template<class Key_, class Value_, class MapType_, class Size_ = std::size_t, class SValue_ = typename MapType_::value_type, class SKey_ = typename MapType_::key_type>
class generic_sized_map_t : public MapType_, public generic_sized_refmap_t<Key_, Value_, MapType_, Size_, SValue_, SKey_> {
public:
   using parent_t = generic_sized_refmap_t<Key_, Value_, MapType_, Size_, SValue_, SKey_>;

   template<class... Args_>
   generic_sized_map_t(Args_&&... args) :
       MapType_(std::forward<Args_>(args)...),
       parent_t(static_cast<MapType_&>(*this)) {
   }

   template<class... Args_>
   generic_sized_map_t(std::initializer_list<typename MapType_::value_type> __l, Args_&&... args) :
       MapType_(__l, std::forward<Args_>(args)...),
       parent_t(static_cast<MapType_&>(*this)) {
   }

   template<class... Args_>
   generic_sized_map_t(generic_sized_map_t const& map) :
       MapType_(map),
       parent_t(static_cast<MapType_&>(*this)) {
   }

   template<class... Args_>
   generic_sized_map_t(generic_sized_map_t&& map) :
       MapType_(std::move(map)),
       parent_t(static_cast<MapType_&>(*this)) {
   }

   virtual ~generic_sized_map_t() = default;

   using parent_t::serialize;
   using parent_t::serialize_init;
   using parent_t::size;
   using parent_t::unserialize;
};

template<class Type_>
struct default_ {
   using type_t = Type_;
};

template<class Type_>
using default_t = typename default_<Type_>::type_t;

template<class Value_, class... Params_>
struct default_<std::vector<Value_, Params_...>> {
   using type_t = generic_sized_refvector_t<Value_, default_t<Value_>, uint32_t, Params_...>;
};

template<class Key_, class Value_, class... Args_>
struct default_<std::map<Key_, Value_, Args_...>> {
   using type_t = generic_sized_refmap_t<Key_, Value_, std::map<Key_, Value_, Args_...>, uint32_t, default_t<Value_>, default_t<Key_>>;
};

template<class Key_, class Value_, class... Args_>
struct default_<std::unordered_map<Key_, Value_, Args_...>> {
   using type_t = generic_sized_refmap_t<Key_, Value_, std::unordered_map<Key_, Value_, Args_...>, uint32_t, default_t<Value_>, default_t<Key_>>;
};

// --------------------------------------------------------------------------------

template<class Value_, class SValue_ = default_t<Value_>, class... Params_>
using refvector_t = generic_refvector_t<Value_, SValue_, Params_...>;

template<class Value_, class Size_ = std::size_t, class SValue_ = default_t<Value_>, class... Params_>
using sized_refvector_t = generic_sized_refvector_t<Value_, SValue_, Size_, Params_...>;

// --------------------------------------------------------------------------------

template<class Key_, class Value_, class SValue_ = default_t<Value_>, class SKey_ = default_t<Key_>, typename Compare_ = std::less<Key_>, typename Alloc_ = std::allocator<std::pair<const Key_, Value_>>>
using refmap_t = generic_refmap_t<Key_, Value_, std::map<Key_, Value_, Compare_, Alloc_>, SValue_, SKey_>;

template<class Key_, class Value_, class SValue_ = default_t<Value_>, class SKey_ = default_t<Key_>, typename Compare_ = std::less<Key_>, typename Alloc_ = std::allocator<std::pair<const Key_, Value_>>>
using refunordered_map_t = generic_refmap_t<Key_, Value_, std::unordered_map<Key_, Value_, Compare_, Alloc_>, SValue_, SKey_>;

template<class Key_, class Value_, class Size_ = std::size_t, class SValue_ = default_t<Value_>, class SKey_ = default_t<Key_>, typename Compare_ = std::less<Key_>, typename Alloc_ = std::allocator<std::pair<const Key_, Value_>>>
using sized_refmap_t = generic_sized_refmap_t<Key_, Value_, std::map<Key_, Value_, Compare_, Alloc_>, Size_, SValue_, SKey_>;

template<class Key_, class Value_, class Size_ = std::size_t, class SValue_ = default_t<Value_>, class SKey_ = default_t<Key_>, typename Compare_ = std::less<Key_>, typename Alloc_ = std::allocator<std::pair<const Key_, Value_>>>
using sized_unordered_refmap_t = generic_sized_refmap_t<Key_, Value_, std::unordered_map<Key_, Value_, Compare_, Alloc_>, Size_, SValue_, SKey_>;

// --------------------------------------------------------------------------------

template<class Key_, class Value_, class SValue_ = default_t<Value_>, class SKey_ = default_t<Key_>, typename Compare_ = std::less<Key_>, typename Alloc_ = std::allocator<std::pair<const Key_, Value_>>>
using map_t = generic_map_t<Key_, Value_, std::map<Key_, Value_, Compare_, Alloc_>, SValue_, SKey_>;

template<class Key_, class Value_, class SValue_ = default_t<Value_>, class SKey_ = default_t<Key_>, typename Compare_ = std::less<Key_>, typename Alloc_ = std::allocator<std::pair<const Key_, Value_>>>
using unordered_map_t = generic_map_t<Key_, Value_, std::unordered_map<Key_, Value_, Compare_, Alloc_>, SValue_, SKey_>;

template<class Key_, class Value_, class Size_ = std::size_t, class SValue_ = default_t<Value_>, class SKey_ = default_t<Key_>, typename Compare_ = std::less<Key_>, typename Alloc_ = std::allocator<std::pair<const Key_, Value_>>>
using sized_map_t = generic_sized_map_t<Key_, Value_, std::map<Key_, Value_, Compare_, Alloc_>, Size_, SValue_, SKey_>;

template<class Key_, class Value_, class Size_ = std::size_t, class SValue_ = default_t<Value_>, class SKey_ = default_t<Key_>, typename Compare_ = std::less<Key_>, typename Alloc_ = std::allocator<std::pair<const Key_, Value_>>>
using sized_unordered_map_t = generic_sized_map_t<Key_, Value_, std::unordered_map<Key_, Value_, Compare_, Alloc_>, Size_, SValue_, SKey_>;

template<class Size_ = std::size_t, std::size_t size_max_ = std::numeric_limits<std::size_t>::max(), std::size_t size_min_ = 0, bool carriage_return_ = true>
class string;

template<class Size_, std::size_t size_max_>
class string<Size_, size_max_, 0, true> : public virtual SerializableBase {
public:
   using size_getter_t = std::function<std::size_t()>;

   string(std::string& member) :
       m_member(member) {
   }

   virtual ~string() = default;

   std::size_t size() const override {
      m_member.resize(clip<0, size_max_>(m_member.size()));
      return m_member.size() + 1;
   }

   template<class Type_>
   bool operator<(const Type_& right) const {
      return m_member < right;
   }

   template<class Type_>
   bool operator>(const Type_& right) const {
      return m_member > right;
   }

   using SerializableBase::serialize;
   using SerializableBase::unserialize;

protected:
   std::string& m_member;

   void serialize(std::byte*& data, std::size_t& left_size) const override {
      m_member.resize(clip<0, size_max_>(m_member.size()));

      if (left_size < m_member.size() + 1)
         throw std::length_error(""); // todo

      data = std::copy(m_member.c_str(), m_member.c_str() + m_member.size() + 1, data);
      left_size -= m_member.size() + 1;
   }

   void unserialize(std::byte const*& data, std::size_t& left_size) override {
      if (!left_size)
         throw std::length_error(""); // todo

      std::size_t index = 0;
      for (; index < left_size && std::bit_cast<char const&>(data[index]) != '\0'; ++index)
         ;
      if (index == left_size)
         throw std::length_error(""); // todo

      if (clip<0, size_max_>(index) != index)
         throw std::length_error(""); // todo

      m_member.resize(index);
      std::copy(data, data + index + 1, m_member.data());

      left_size -= index + 1;
      data += index + 1;
   }

   friend class SerializableBase;
};

template<class Size_, std::size_t size_max_, std::size_t size_min_>
class string<Size_, size_max_, size_min_, false> : public virtual SerializableBase {
public:
   string(
       std::string& member,
       const std::function<Size_()>& size_getter = []() { return size_min_; }) :
       // todo
       m_member(member),
       get_size(size_getter) {
      static_assert(!std::is_same_v<size_max_, size_max_>, "not yet implemented");
   }

   virtual ~string() = default;

   std::size_t size() const override {
      m_member.resize(clip<size_min_, size_max_>(m_member.size()));
      return m_member.size();
   }

   template<class Type_>
   bool operator<(const Type_& right) const {
      return m_member < right;
   }

   template<class Type_>
   bool operator>(const Type_& right) const {
      return m_member > right;
   }

   using SerializableBase::serialize;
   using SerializableBase::unserialize;

protected:
   std::string& m_member;
   std::function<Size_()> get_size;

   void serialize(std::byte*& data, std::size_t& left_size) const override {
      m_member.resize(clip<size_min_, size_max_>(m_member.size()));

      if (left_size < m_member.size())
         throw std::length_error(""); // todo

      data = std::copy(m_member.begin(), m_member.end(), data);
      left_size -= m_member.size();
   }

   void unserialize(std::byte const*& data, std::size_t& left_size) override {
      if (left_size < size_min_)
         throw std::length_error(""); // todo

      auto size = get_size();

      if (clip<size_min_, size_max_>(size) != size)
         throw std::length_error(""); // todo

      m_member = std::string(reinterpret_cast<char const*>(data), size);

      left_size -= m_member.size();
      data += m_member.size();
   }

   friend class SerializableBase;
};

// -----------------------------------------------------------------

template<uint8_t bits_, std::size_t bits_offset_>
struct bitfield_struct {
   bit_size_to_type<bits_offset_> : bits_offset_;
   bit_size_to_type<bits_> value : bits_;
} PACKED;

template<uint8_t bits_>
struct bitfield_struct<bits_, 0> {
   bit_size_to_type<bits_> value : bits_;
} PACKED;

template<class Type_, uint8_t...>
struct forward {
   using type = Type_;
};

template<class... bits_>
struct bitfield : SerializableBase, public std::tuple<typename bits_::type&...> {
public:
   template<class... Members_>
   bitfield(Members_&... members) :
      std::tuple<typename bits_::type&...>(members...) {
      static_assert(((bits_::size <= sizeof(members) * 8) && ...), "member size must cover bitfield one's");
      static_assert((std::is_fundamental_v<std::remove_reference_t<Members_>> && ...), "member must be fundamental");
   }

   bitfield(bitfield<bits_...>&&) = default;
   bitfield(bitfield<bits_...>&) = default;
   bitfield(const bitfield<bits_...>&) = default;

   virtual ~bitfield() = default;

   std::size_t size() const override {
      return size_constexpr();
   }

   static constexpr std::size_t size_constexpr() { // @suppress("No return")
      return std::ceil(bit_size_constexpr() / 8.f);
   }

   static constexpr std::size_t bit_size_constexpr() { // @suppress("No return")
      return (bits_::size + ...);
   }

   using SerializableBase::serialize;
   using SerializableBase::unserialize;

protected:
   template<std::size_t bit_size_, std::size_t bit_offset_, bool const_>
   static auto& getBitField_(conditional_const_t<const_, uint8_t>* data) {
      static_assert(size_constexpr() * 8 > bit_offset_);
      static constexpr std::size_t bit_offset = size_constexpr() * 8 - (bit_offset_ + bit_size_);
      static constexpr std::size_t uint8_t_offset = bit_offset / 8;

      return reinterpret_cast<conditional_const_t<const_,
         bitfield_struct<bit_size_, bit_offset - uint8_t_offset * 8>>&>(data[uint8_t_offset]);
   }

   template<std::size_t bit_size_, std::size_t bit_offset_>
   static auto& getBitField(uint8_t* data) {
      return getBitField_<bit_size_, bit_offset_, false>(data);
   }

   template<std::size_t bit_size_, std::size_t bit_offset_>
   static auto const& getBitField(uint8_t const* data) {
      return getBitField_<bit_size_, bit_offset_, true>(data);
   }

   template<std::size_t index_, std::size_t bit_offset_, class bits__, class... bits___>
   struct helper {
      static void serialize(bitfield const& this_, uint8_t* data) {
         getBitField<bits__::size, bit_offset_>(data).value = std::get<index_>(this_);
         helper<index_ + 1, bit_offset_ + bits__::size, bits___...>::serialize(this_, data);
      }

      static void unserialize(bitfield& this_, uint8_t const* data) {
         std::get<index_>(this_) = getBitField<bits__::size, bit_offset_>(data).value;
         helper<index_ + 1, bit_offset_ + bits__::size, bits___...>::unserialize(this_, data);
      }
   };

   template<std::size_t index_, std::size_t bit_offset_, class bits__>
   struct helper<index_, bit_offset_, bits__> {
      static void serialize(bitfield const& this_, uint8_t* data) {
         getBitField<bits__::size, bit_offset_>(data).value = std::get<index_>(this_);
      }

      static void unserialize(bitfield this_, uint8_t const* data) {
         std::get<index_>(this_) = getBitField<bits__::size, bit_offset_>(data).value;
      }
   };

   void serialize(std::byte*& data, std::size_t& left_size) const override {
      if (left_size < size_constexpr())
         throw std::length_error(""); // todo

      helper<0, 0, bits_...>::serialize(*this, data);

      for (unsigned int i = 0; i < size_constexpr() / 2; ++i)
         std::swap(data[i], data[size_constexpr() - i - 1]);

      left_size -= size_constexpr();
      data += size_constexpr();
   }

   void unserialize(std::byte const*& data, std::size_t& left_size) override { // todo static constexpr
      if (left_size < size_constexpr())
         throw std::length_error(""); // todo

      alignas(16) std::array<uint8_t, size_constexpr()> leddian_data {};

      for (unsigned int i = 0; i < size_constexpr(); ++i)
         leddian_data[i] = data[size_constexpr() - i - 1];

      helper<0, 0, bits_...>::unserialize(*this, leddian_data.data());
      left_size -= size_constexpr();
      data += size_constexpr();
   }

   friend class SerializableBase;
};

#define SLZ_FINAL(TYPE)                                                                                              \
   void serialize(std::byte*& data, std::size_t& left_size) const override { TYPE::serialize(data, left_size); }     \
   virtual void serialize_init() const override { TYPE::serialize_init(); }                                          \
   void unserialize(std::byte const*& data, std::size_t& left_size) override { TYPE::unserialize(data, left_size); } \
   std::size_t size() const override { return TYPE::size(); }

/*###################*/}// namespace slz         /*###################*/

template<class Type_, class Size_, std::size_t size_max_, std::size_t size_min_, bool carriage_return_>
bool operator<(Type_ const& left, slz::string<Size_, size_max_, size_min_, carriage_return_> const& right) {
   return right > left;
}

#define BF(type_name, size) \
   union {                  \
      struct {              \
         type_name : size;  \
      };                    \
      type_name##_raw;      \
   }
