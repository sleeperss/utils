#include "serializable.hpp"
#include "serializable.inl"

#include <cstddef>
#include <vector>

// -----------------------------------------------------------------

/*####################*/namespace slz        {/*###################*/

void SerializableBase::serialize_init() const {
}

std::vector<std::byte> SerializableBase::serialize() const {
   std::vector<std::byte> res;
   return serialize(res);
}

std::vector<std::byte>& SerializableBase::serialize(std::vector<std::byte>& res) const {
   serialize_init();

   res.resize(size());
   auto size = res.size();
   auto* data = res.data();
   serialize(data, size);
   return res;
}

std::size_t SerializableBase::unserialize(std::vector<std::byte> const& data) {
   auto size = data.size();
   auto const* data_ = data.data();
   unserialize(data_, size);

   return data.size() - size;
}

/*###################*/}// namespace slz      /*###################*/

// -----------------------------------------------------------------
