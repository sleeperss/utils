#pragma once

#include "serializable.hpp"

#include <cstddef>
#include <memory>
#include <type_traits>
#include <utility>

// -----------------------------------------------------------------

/*####################*/namespace slz        {/*###################*/

/*####################*/namespace            {/*###################*/

template<class Type>
constexpr bool is_ptr { false };

template<class... Types>
constexpr bool is_ptr<std::shared_ptr<Types...>> { true };

template<class... Types>
constexpr bool is_ptr<std::unique_ptr<Types...>> { true };

template<class Type>
constexpr bool is_ptr<Type*> { true };

template<class Type>
concept _ptr = is_ptr<std::remove_const_t<std::remove_reference<Type>>>;

template<class Type_>
struct serializable_helper {
};

template<typename Type_>
concept _fundamental = std::is_fundamental_v<std::remove_const_t<std::remove_reference_t<Type_>>>;

template<_fundamental Type_>
struct serializable_helper<Type_> {
   static inline fundamental<Type_> get_as_serializable(Type_&& object) {
      return std::forward<Type_>(object);
   }
};

template<_serializable Type_>
struct serializable_helper<Type_> {
   static inline Type_&& get_as_serializable(Type_&& object) {
      return std::forward<Type_>(object);
   }
};

template<_ptr Type_>
struct serializable_helper<Type_> {
   static inline auto&& get_as_serializable(Type_&& object) {
      return serializable_helper<decltype(*std::declval<Type_>())>::get_as_serializable(*object);
   }
};

/*###################*/}// namespace          /*###################*/

template<class Type_>
void SerializableBase::serialize_init_accessor(Type_ const& serializable) {
   serializable_helper<Type_ const&>::get_as_serializable(serializable).serialize_init();
}

template<class Type_>
void SerializableBase::serialize_accessor(Type_ const& serializable, std::byte*& data, std::size_t& left_size) {
   serializable_helper<Type_ const&>::get_as_serializable(serializable).serialize(data, left_size);
}

template<class Type_>
void SerializableBase::unserialize_accessor(Type_& serializable, std::byte const*& data, std::size_t& left_size) {
   serializable_helper<Type_&>::get_as_serializable(serializable).unserialize(data, left_size);
}

template<class Type_>
std::size_t SerializableBase::size_accessor(Type_ const& serializable) {
   return serializable_helper<Type_ const&>::get_as_serializable(serializable).size();
}

/*###################*/}// namespace slz      /*###################*/
