// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------
// --  █████╗ ██╗     ███████╗██╗  ██╗    ██╗  ██╗ ██████╗ ███╗   ███╗███████╗ ----
// -- ██╔══██╗██║     ██╔════╝╚██╗██╔╝    ██║  ██║██╔═══██╗████╗ ████║██╔════╝ ----
// -- ███████║██║     █████╗   ╚███╔╝     ███████║██║   ██║██╔████╔██║█████╗   ----
// -- ██╔══██║██║     ██╔══╝   ██╔██╗     ██╔══██║██║   ██║██║╚██╔╝██║██╔══╝   ----
// -- ██║  ██║███████╗███████╗██╔╝ ██╗    ██║  ██║╚██████╔╝██║ ╚═╝ ██║███████╗ ----
// -- ╚═╝  ╚═╝╚══════╝╚══════╝╚═╝  ╚═╝    ╚═╝  ╚═╝ ╚═════╝ ╚═╝     ╚═╝╚══════╝ ----
// --                                                                          ----
// -----------------------------------------------------    db                 ----
// --------------------------------------------------------------------------------
//
//      Copyright 2022, Alexandre GARCIN, All rights reserved.
//
//                                                   Created on 24-09-2022 22:18:50

#pragma once

#include "db/db.hpp"
#include "utils/utils.hpp"

#include <string>

// clang-format off
/*####################*/namespace db                        {/*###################*/
// clang-format on

class blob_t : public sharable<table_t> {
public:
   struct indexes_t {
      db_t::index_t begin;
      db_t::index_t end;
      db_t::index_t next;
   };

   ~blob_t() override;

private:
   explicit blob_t(std::shared_ptr<db_t> const& db, indexes_t const& indexes);

   using sharable_type_t::create;
   bool post_validate() override;

   std::shared_ptr<db_t> db_;
   std::string name_;
   indexes_t index_;

   friend sharable_type_t;
   friend ::db_t;
};

// clang-format off
/*###################*/}// namespace db                      /*###################*/
// clang-format on