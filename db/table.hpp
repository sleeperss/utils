// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------
// --  █████╗ ██╗     ███████╗██╗  ██╗    ██╗  ██╗ ██████╗ ███╗   ███╗███████╗ ----
// -- ██╔══██╗██║     ██╔════╝╚██╗██╔╝    ██║  ██║██╔═══██╗████╗ ████║██╔════╝ ----
// -- ███████║██║     █████╗   ╚███╔╝     ███████║██║   ██║██╔████╔██║█████╗   ----
// -- ██╔══██║██║     ██╔══╝   ██╔██╗     ██╔══██║██║   ██║██║╚██╔╝██║██╔══╝   ----
// -- ██║  ██║███████╗███████╗██╔╝ ██╗    ██║  ██║╚██████╔╝██║ ╚═╝ ██║███████╗ ----
// -- ╚═╝  ╚═╝╚══════╝╚══════╝╚═╝  ╚═╝    ╚═╝  ╚═╝ ╚═════╝ ╚═╝     ╚═╝╚══════╝ ----
// --                                                                          ----
// -----------------------------------------------------    db                 ----
// --------------------------------------------------------------------------------
//
//      Copyright 2022, Alexandre GARCIN, All rights reserved.
//
//                                                   Created on 24-09-2022 22:18:50

#pragma once

#include "db/db.hpp"
#include "utils/utils.hpp"

#include <memory>
#include <string>

// clang-format off
/*####################*/namespace db                        {/*###################*/
// clang-format on

class table_t : public sharable<table_t> {
public:
   ~table_t() override;

private:
   explicit table_t(std::shared_ptr<db_t> const& db, std::string const& name, std::shared_ptr<blob_t> const& blob);

   using sharable_type_t::create;
   bool post_validate() override;

   std::shared_ptr<db_t> db_;
   std::string name_;
   std::shared_ptr<blob_t> blob_;

   friend sharable_type_t;
   friend ::db_t;
};

// clang-format off
/*###################*/}// namespace db                      /*###################*/
// clang-format on