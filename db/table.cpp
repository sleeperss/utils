// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------
// --  █████╗ ██╗     ███████╗██╗  ██╗    ██╗  ██╗ ██████╗ ███╗   ███╗███████╗ ----
// -- ██╔══██╗██║     ██╔════╝╚██╗██╔╝    ██║  ██║██╔═══██╗████╗ ████║██╔════╝ ----
// -- ███████║██║     █████╗   ╚███╔╝     ███████║██║   ██║██╔████╔██║█████╗   ----
// -- ██╔══██║██║     ██╔══╝   ██╔██╗     ██╔══██║██║   ██║██║╚██╔╝██║██╔══╝   ----
// -- ██║  ██║███████╗███████╗██╔╝ ██╗    ██║  ██║╚██████╔╝██║ ╚═╝ ██║███████╗ ----
// -- ╚═╝  ╚═╝╚══════╝╚══════╝╚═╝  ╚═╝    ╚═╝  ╚═╝ ╚═════╝ ╚═╝     ╚═╝╚══════╝ ----
// --                                                                          ----
// -----------------------------------------------------    db                 ----
// --------------------------------------------------------------------------------
//
//      Copyright 2022, Alexandre GARCIN, All rights reserved.
//
//                                                   Created on 24-09-2022 22:18:50

#include "table.hpp"

// clang-format off
/*####################*/namespace db                        {/*###################*/
// clang-format on

table_t::table_t(std::shared_ptr<db_t> const& db, std::string const& name, std::shared_ptr<blob_t> const& blob) :
    db_(db),
    name_(name),
    blob_(blob) {
}

table_t::~table_t() {
   db_->tables_.erase(name_);
}

bool table_t::post_validate() {
   return db_->tables_.try_emplace(name_, weak()).second;
}

// clang-format off
/*###################*/}// namespace db                      /*###################*/
// clang-format on