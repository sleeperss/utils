// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------
// --  █████╗ ██╗     ███████╗██╗  ██╗    ██╗  ██╗ ██████╗ ███╗   ███╗███████╗ ----
// -- ██╔══██╗██║     ██╔════╝╚██╗██╔╝    ██║  ██║██╔═══██╗████╗ ████║██╔════╝ ----
// -- ███████║██║     █████╗   ╚███╔╝     ███████║██║   ██║██╔████╔██║█████╗   ----
// -- ██╔══██║██║     ██╔══╝   ██╔██╗     ██╔══██║██║   ██║██║╚██╔╝██║██╔══╝   ----
// -- ██║  ██║███████╗███████╗██╔╝ ██╗    ██║  ██║╚██████╔╝██║ ╚═╝ ██║███████╗ ----
// -- ╚═╝  ╚═╝╚══════╝╚══════╝╚═╝  ╚═╝    ╚═╝  ╚═╝ ╚═════╝ ╚═╝     ╚═╝╚══════╝ ----
// --                                                                          ----
// -----------------------------------------------------    db                 ----
// --------------------------------------------------------------------------------
//
//      Copyright 2022, Alexandre GARCIN, All rights reserved.
//
//                                                   Created on 24-09-2022 22:18:50

#pragma once

#include "utils/utils.hpp"

#include <fstream>
#include <string>

namespace db {
class table_t;
class blob_t;
} // namespace db

class db_t : public sharable<db_t> {
public:
   using key_t = std::string;
   using index_t = std::size_t;
   using tables_t = std::map<key_t, std::weak_ptr<db::table_t>, std::less<>>;
   using blobs_t = std::map<index_t, std::weak_ptr<db::blob_t>, std::less<>>;

   ~db_t() override = default;

   using sharable_type_t::create;

   bool add_table(std::string const& name);
   bool remove_table(std::string const& name);

private:
   explicit db_t(std::string const& path);
   bool post_validate() override;

   std::string const& path_;
   std::fstream file_;
   tables_t tables_;
   blobs_t blobs_;

   friend class db::table_t;
   friend class db::blob_t;
};