#include "logger.h"

#include <jobs/Job.h>

#include <iomanip>
#include <iostream>
#include <iterator>
#include <mutex>
#include <string>
#include <utility>
#include <vector>

/*####################*/namespace llog       {/*###################*/

logger_t::logger_t(std::string&& prefix, color_e default_color) :
   m_buffer(std::move(prefix), default_color) {
   m_oldBuffer = rdbuf(&m_buffer);
}

logger_t::~logger_t() {
   rdbuf(m_oldBuffer);
}

std::streamsize buffer_t::xsputn(char const* s, std::streamsize n) { // @suppress("Missing const-qualification")
   for (std::streamsize it = 0, size = 1; it < n; ++it, ++size) {
      if (s[it] == '\n') {
         if (m_buffer.size() && m_buffer.back() == '\n')
            m_buffer.append("\t");
         write_style();
         m_buffer.append(s + it + 1 - size, size);
         size = 0;
      } else if (it + 1 == n) {
         if (m_buffer.size() && m_buffer.back() == '\n')
            m_buffer.append("\t");
         write_style();
         m_buffer.append(s + it + 1 - size, size);
      }
   }
   return n;
}

void buffer_t::write_style() {
   if (!m_style_changed)
      return;

   m_buffer += "\033[" + std::to_string(m_style) + ";" + std::to_string(m_forground_color) + ";" + std::to_string(m_background_color + 10) + "m";

   m_style_changed = false;
}

int buffer_t::overflow(int c) { // @suppress("Missing const-qualification")
   char const ch = traits_type::to_char_type(c);
   xsputn(&ch, 1);
   return c;
}

int buffer_t::sync() {
   static std::mutex mutex {};
   std::lock_guard<std::mutex> const lock { mutex };
   if (!m_buffer.size())
      return 0;

   auto const time = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
   auto const timeinfo = std::localtime(&time);
   auto begin = std::string(line_begin ? "\033[0m" : "\n\033[0m") +
         "[" + (timeinfo->tm_mday < 10 ? " " : "") +
               std::to_string(timeinfo->tm_mday) + "/" + (timeinfo->tm_mon < 9 ? "0" : "") +
               std::to_string(timeinfo->tm_mon + 1) + "/" +
               std::to_string(timeinfo->tm_year - 100) + " " + (timeinfo->tm_hour < 10 ? " " : "") +
               std::to_string(timeinfo->tm_hour) + ":" + (timeinfo->tm_min < 10 ? "0" : "") +
               std::to_string(timeinfo->tm_min) + ":" + (timeinfo->tm_sec < 10 ? "0" : "") +
               std::to_string(timeinfo->tm_sec) +
         "][\033[" + std::to_string(m_default_color) + "m" + m_prefix + "\033[0m][\033[90m" + job::Scheduler::worker_name() + ":" + job::Scheduler::current_job_name() + "\033[0m]";

   for (const auto& tag : m_tags)
      begin += "[" + tag + "]";

   m_buffer = begin + ": \033[" + std::to_string(m_default_color) + "m" + m_buffer;

   if (m_buffer.back() == '\n') {
      line_begin = true;
      m_buffer.resize(m_buffer.size() - 1);
      m_buffer += "\033[0m\n";
   }

   std::cout << std::resetiosflags(std::ios::showbase);
   std::cout << m_buffer <<  "\033[0m" << std::flush;
   std::cout << std::resetiosflags(std::ios::showbase);
   m_buffer.clear();
   return 0;
}

// -----------------------------------------------------------------

buffer_t::buffer_t(std::string&& prefix, color_e default_color) :
   m_prefix(std::move(prefix)), m_default_color(std::move(default_color)) {
}

// -----------------------------------------------------------------

tag_t::tag_t(std::string const& tag, color_e const color) :
   tag(tag),
   color(color) {
}

// -----------------------------------------------------------------

tag_remove_t::tag_remove_t(std::string const& tag) :
   tag(tag) {
}

// -----------------------------------------------------------------

tagger_t::tagger_t(std::string const& tag, color_e const color) {
   for (auto logger : loggers)
      try {
         *logger << tag_t { tag, color };
         m_removers.emplace_back([logger, tag](){
            *logger << tag_remove_t { tag };
         });
      } catch (duplicate_tag const&) {
      }
}

tagger_t::~tagger_t() {
   for (auto const& remover : m_removers)
      remover();
}

// -----------------------------------------------------------------

std::ostream& operator <<(std::ostream& ostream, tag_t&& tag) {
   if (logger_t& logger = dynamic_cast<logger_t&>(ostream)) {
      if (auto const it = logger.m_buffer.m_tag_ids.emplace(tag.tag, logger.m_buffer.m_tags.end()); it.second) {
         logger.m_buffer.m_tags.emplace_back("\033[2;" + std::to_string(tag.color) + "m" + tag.tag + "\033[0m");
         it.first->second = std::prev(logger.m_buffer.m_tags.end());
      } else
         throw duplicate_tag();
   }
   return ostream;
}

std::ostream& operator <<(std::ostream& ostream, tag_remove_t&& tag) {
   if (logger_t& logger = dynamic_cast<logger_t&>(ostream))
      if (auto const it = logger.m_buffer.m_tag_ids.find(tag.tag); it != logger.m_buffer.m_tag_ids.end()) {
         logger.m_buffer.m_tags.erase(it->second);
         logger.m_buffer.m_tag_ids.erase(it);
      }
   return ostream;
}

std::ostream& operator<<(std::ostream& ostream, fgd_color_t const& color) {
   if (logger_t& logger = dynamic_cast<logger_t&>(ostream)) {
      logger.m_buffer.m_forground_color = color.color;
      logger.m_buffer.m_style_changed = true;
   }
   return ostream;
}

std::ostream& operator<<(std::ostream& ostream, bgd_color_t const& color) {
   if (logger_t& logger = dynamic_cast<logger_t&>(ostream)) {
      logger.m_buffer.m_background_color = color.color;
      logger.m_buffer.m_style_changed = true;
   }
   return ostream;
}

std::ostream& operator<<(std::ostream& ostream, style_t const& style) {
   if (logger_t& logger = dynamic_cast<logger_t&>(ostream)) {
      logger.m_buffer.m_style = style.style;
      logger.m_buffer.m_style_changed = true;
   }
   return ostream;
}

std::ostream& operator<<(std::ostream& ostream, style_reset_t const&) {
   if (logger_t& logger = dynamic_cast<logger_t&>(ostream)) {
      logger.m_buffer.m_style = Normal;
      logger.m_buffer.m_background_color = Default;
      logger.m_buffer.m_forground_color = logger.m_buffer.m_default_color;
      logger.m_buffer.m_style_changed = true;
   }
   return ostream;
}

thread_local logger_t info    { "I", Cyan        };
thread_local logger_t trace   { "T", Blue        };
thread_local logger_t warning { "W", LightYellow };
thread_local logger_t error   { "E", LightRed    };
thread_local logger_t debug   { "D", DarkGrey    }; // @suppress("Missing const-qualification")

thread_local std::vector<logger_t*> const loggers = {
      &info,
      &trace,
      &warning,
      &error,
      &debug
};

void init() {
   loggers; // @suppress("Statement has no effect")
}

bool buffer_t::line_begin = true;

style_reset_t const stl_rst {};

fgd_color_t const fgd_black = fgd_color_t { Black };
fgd_color_t const fgd_red = fgd_color_t { Red };
fgd_color_t const fgd_green = fgd_color_t { Green };
fgd_color_t const fgd_yellow = fgd_color_t { Yellow };
fgd_color_t const fgd_blue = fgd_color_t { Blue };
fgd_color_t const fgd_magenta = fgd_color_t { Magenta };
fgd_color_t const fgd_cyan = fgd_color_t { Cyan };
fgd_color_t const fgd_light_grey = fgd_color_t { LightGrey };
fgd_color_t const fgd_dark_grey = fgd_color_t { DarkGrey };
fgd_color_t const fgd_light_red = fgd_color_t { LightRed };
fgd_color_t const fgd_light_green = fgd_color_t { LightGreen };
fgd_color_t const fgd_light_yellow = fgd_color_t { LightYellow };
fgd_color_t const fgd_light_blue = fgd_color_t { LightBlue };
fgd_color_t const fgd_light_magenta = fgd_color_t { LightMagenta };
fgd_color_t const fgd_light_cyan = fgd_color_t { LightCyan };
fgd_color_t const fgd_white = fgd_color_t { White };

bgd_color_t const bgd_black = bgd_color_t { Black };
bgd_color_t const bgd_red = bgd_color_t { Red };
bgd_color_t const bgd_green = bgd_color_t { Green };
bgd_color_t const bgd_yellow = bgd_color_t { Yellow };
bgd_color_t const bgd_blue = bgd_color_t { Blue };
bgd_color_t const bgd_magenta = bgd_color_t { Magenta };
bgd_color_t const bgd_cyan = bgd_color_t { Cyan };
bgd_color_t const bgd_light_grey = bgd_color_t { LightGrey };
bgd_color_t const bgd_dark_grey = bgd_color_t { DarkGrey };
bgd_color_t const bgd_light_red = bgd_color_t { LightRed };
bgd_color_t const bgd_light_green = bgd_color_t { LightGreen };
bgd_color_t const bgd_light_yellow = bgd_color_t { LightYellow };
bgd_color_t const bgd_light_blue = bgd_color_t { LightBlue };
bgd_color_t const bgd_light_magenta = bgd_color_t { LightMagenta };
bgd_color_t const bgd_light_cyan = bgd_color_t { LightCyan };
bgd_color_t const bgd_white = bgd_color_t { White };

style_t const stl_nrm = style_t { Normal };
style_t const stl_blink = style_t { Blink };
style_t const stl_bold = style_t { Bold };
style_t const stl_dim = style_t { Dim };
style_t const stl_hidden = style_t { Hidden };
style_t const stl_inv = style_t { Inverted };
style_t const stl_udln = style_t { Underlined };

/*###################*/} // namespace llog     /*###################*/
