#pragma once


#include <functional>
#include <list>
#include <ostream>
#include <string>
#include <unordered_map>
#include <vector>

/*####################*/namespace llog       {/*###################*/

struct duplicate_tag : std::exception {
};

using operator_t = std::ostream& (*)(std::ostream&);

enum color_e {
   Black = 30,
   Red,
   Green,
   Yellow,
   Blue,
   Magenta,
   Cyan,
   LightGrey,
   Default = 39,
   DarkGrey = 90,
   LightRed,
   LightGreen,
   LightYellow,
   LightBlue,
   LightMagenta,
   LightCyan,
   White,
};

enum style_e {
   Normal = 0,
   Blink = 5,
   Bold = 1,
   Dim = 2,
   Hidden = 8,
   Inverted = 7,
   Underlined = 4,
};

struct fgd_color_t {
   color_e color;
};

struct bgd_color_t {
   color_e color;
};

struct style_t {
   style_e style;
};

struct tag_t {
   std::string tag;
   color_e color;
   tag_t(std::string const&, color_e const = White);
};

struct tag_remove_t {
   std::string tag;
   tag_remove_t(std::string const&);
};

struct style_reset_t {
};

class tagger_t {
private:
   std::vector<std::function<void()>> m_removers;

public:
   tagger_t(std::string const&, color_e const = White);
   virtual ~tagger_t();
};

class buffer_t : private std::streambuf {
private:
   std::string m_buffer {};
   std::string m_prefix;
   color_e m_default_color;
   std::list<std::string> m_tags {};
   std::unordered_map<std::string, decltype(m_tags)::const_iterator> m_tag_ids {};
   color_e m_forground_color { Default };
   color_e m_background_color { Default };
   style_e m_style { Normal };
   bool m_style_changed { false };

   static bool line_begin;

   buffer_t(std::string&&, color_e);

   void write_style();

public:
   virtual std::streamsize xsputn(const char* s, std::streamsize n) final override;
   virtual int overflow(int c) final override;
   virtual int sync() final override;

   friend class logger_t;
   friend std::ostream& operator<<(std::ostream&, tag_t&&);
   friend std::ostream& operator<<(std::ostream&, tag_remove_t&&);
   friend std::ostream& operator<<(std::ostream&, fgd_color_t const&);
   friend std::ostream& operator<<(std::ostream&, bgd_color_t const&);
   friend std::ostream& operator<<(std::ostream&, style_t const&);
   friend std::ostream& operator<<(std::ostream&, style_reset_t const&);
};

class logger_t : public std::ostream {
private:
   buffer_t m_buffer;
   std::streambuf* m_oldBuffer;

public:
   logger_t(std::string&& prefix, color_e default_color);
   virtual ~logger_t();

   friend std::ostream& operator<<(std::ostream&, tag_t&&);
   friend std::ostream& operator<<(std::ostream&, tag_remove_t&&);
   friend std::ostream& operator<<(std::ostream&, fgd_color_t const&);
   friend std::ostream& operator<<(std::ostream&, bgd_color_t const&);
   friend std::ostream& operator<<(std::ostream&, style_t const&);
   friend std::ostream& operator<<(std::ostream&, style_reset_t const&);
};

extern thread_local logger_t info;
extern thread_local logger_t trace;
extern thread_local logger_t warning;
extern thread_local logger_t error;
extern thread_local logger_t debug; // @suppress("Missing const-qualification")
extern thread_local std::vector<logger_t*> const loggers;

std::ostream& operator<<(std::ostream&, tag_t&&);
std::ostream& operator<<(std::ostream&, tag_remove_t&&);

std::ostream& operator<<(std::ostream&, fgd_color_t const&);
std::ostream& operator<<(std::ostream&, bgd_color_t const&);
std::ostream& operator<<(std::ostream&, style_t const&);

std::ostream& operator<<(std::ostream&, style_reset_t const&);

extern fgd_color_t const fgd_black;
extern fgd_color_t const fgd_red;
extern fgd_color_t const fgd_green;
extern fgd_color_t const fgd_yellow;
extern fgd_color_t const fgd_blue;
extern fgd_color_t const fgd_magenta;
extern fgd_color_t const fgd_cyan;
extern fgd_color_t const fgd_light_grey;
extern fgd_color_t const fgd_dark_grey;
extern fgd_color_t const fgd_light_red;
extern fgd_color_t const fgd_light_green;
extern fgd_color_t const fgd_light_yellow;
extern fgd_color_t const fgd_light_blue;
extern fgd_color_t const fgd_light_magenta;
extern fgd_color_t const fgd_light_cyan;
extern fgd_color_t const fgd_white;

extern bgd_color_t const bgd_black;
extern bgd_color_t const bgd_red;
extern bgd_color_t const bgd_green;
extern bgd_color_t const bgd_yellow;
extern bgd_color_t const bgd_blue;
extern bgd_color_t const bgd_magenta;
extern bgd_color_t const bgd_cyan;
extern bgd_color_t const bgd_light_grey;
extern bgd_color_t const bgd_dark_grey;
extern bgd_color_t const bgd_light_red;
extern bgd_color_t const bgd_light_green;
extern bgd_color_t const bgd_light_yellow;
extern bgd_color_t const bgd_light_blue;
extern bgd_color_t const bgd_light_magenta;
extern bgd_color_t const bgd_light_cyan;
extern bgd_color_t const bgd_white;

extern style_t const stl_nrm;
extern style_t const stl_blink;
extern style_t const stl_bold;
extern style_t const stl_dim;
extern style_t const stl_hidden;
extern style_t const stl_inv;
extern style_t const stl_udln;

extern style_reset_t const stl_rst;

void init();

/*###################*/}// namespace llog     /*###################*/
