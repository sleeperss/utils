/*
 * utils.inl
 *
 *  Created on: Nov 12, 2019
 *      Author: sleeper
 */

#ifndef SRC_UTILS_INL_
#define SRC_UTILS_INL_

#include "utils.hpp"

#include <jobs/Scheduler.h>

#include <cassert>
#include <chrono>
#include <cstddef>
#include <exception>
#include <filesystem>
#include <fstream>
#include <initializer_list>
#include <memory>
#include <mutex>
#include <ratio>
#include <regex>
#include <shared_mutex>
#include <stdexcept>
#include <string>
#include <tuple>
#include <utility>
#include <vector>

// -----------------------------------------------------------------

//template<class Type>
//ip6_bf_t::ip6_bf_t(Type value) : // @suppress("Not initialized")
//      _(value) {
//}
//
//template<class Type>
//ip6_bf_t& ip6_bf_t::operator=(Type value) {
//   return _ = value, *this;
//}
//
//template<class Type>
//bool ip6_bf_t::operator<(Type value) const {
//   return _ < value;
//}
//
//template<class Type>
//bool ip6_bf_t::operator<=(Type value) const {
//   return _ <= value;
//}
//
//template<class Type>
//bool ip6_bf_t::operator>(Type value) const {
//   return _ > value;
//}
//
//template<class Type>
//bool ip6_bf_t::operator>=(Type value) const {
//   return _ >= value;
//}
//
//template<class Type>
//bool ip6_bf_t::operator==(Type value) const {
//   return _ == value;
//}
//
//template<class Type>
//bool ip6_bf_t::operator!=(Type value) const {
//   return _ != value;
//}
//
//template<class Type>
//ip6_bf_t ip6_bf_t::operator++(Type) {
//   ip6_bf_t res;
//   return ++(*this), res;
//}
//
//template<class Type>
//ip6_bf_t ip6_bf_t::operator--(Type) {
//   ip6_bf_t res;
//   return --(*this), res;
//}
//
//template<class Type>
//ip6_bf_t ip6_bf_t::operator-(Type value) const {
//   ip6_bf_t res;
//   return res -= value, res;
//}
//
//template<class Type>
//ip6_bf_t ip6_bf_t::operator+(Type value) const {
//   ip6_bf_t res;
//   return res += value, res;
//}
//
//template<class Type>
//ip6_bf_t& ip6_bf_t::operator+=(Type value) {
//   return _ += value, *this;
//}
//
//template<class Type>
//ip6_bf_t& ip6_bf_t::operator-=(Type value) {
//   return _ -= value, *this;
//}
//
//template<class Type>
//ip6_bf_t& ip6_bf_t::operator&=(Type value) {
//   return _ &= value, *this;
//}
//
//template<class Type>
//ip6_bf_t& ip6_bf_t::operator|=(Type value) {
//   return _ |= value, *this;
//}
//
//template<class Type>
//ip6_bf_t ip6_bf_t::operator&(Type value) const {
//   ip6_bf_t res(*this);
//   return res &= value, res;
//}
//
//template<class Type>
//ip6_bf_t ip6_bf_t::operator|(Type value) const {
//   ip6_bf_t res(*this);
//   return res |= value, res;
//}

// -----------------------------------------------------------------

struct invalid_instantiation : public std::exception {
   virtual char const*
   what() const _GLIBCXX_TXN_SAFE_DYN _GLIBCXX_NOTHROW {
      return "Unauthorized create function call";
   }
};

//// -----------------------------------------------------------------
//
//template<class Type_, class... Args_>
//struct smart_reference_helper {
// static std::unique_ptr<Type_> init_ptr(smart_reference<Type_>*, Args_&&... args) {
//    return std::make_unique<Type_>(std::move(args)...);
// }
//
// static Type_& init_reference(smart_reference<Type_>* this_ptr, Args_&&...) {
//    return *this_ptr->ptr;
// }
//};
//
//template<class Type_>
//struct smart_reference_helper<Type_, Type_&> {
//   static std::unique_ptr<Type_> init_ptr(smart_reference<Type_>*, Type_&&) {
//      return nullptr;
//   }
//
//   static Type_& init_reference(smart_reference<Type_>*, Type_&& reference) {
//      return reference;
//   }
//};
//
//template<class Type>
//template<class... Args_>
//smart_reference<Type>::smart_reference(Args_&&... args) :
//    ptr(smart_reference_helper<Type, Args_...>::init_ptr(this, std::forward<Args_>(args)...)), reference(
//          smart_reference_helper<Type, Args_...>::init_reference(this, std::forward<Args_>(args)...)) {
//}

// -----------------------------------------------------------------

template<class Base_, class Friend_, class... Friends_>
template<class... Args_>
friends_t<Base_, Friend_, Friends_...>::friends_t(Args_&& ... args) :
      friends_t<Base_, Friends_...>(std::forward<Args_>(args)...) {
}

template<class Base_, class Friend_>
template<class... Args_>
friends_t<Base_, Friend_>::friends_t(Args_&& ... args) :
      Base_(std::forward<Args_>(args)...) {
}

template<class Base>
template<class... Args>
friends_t<Base>::friends_t(Args&& ... args) :
      Base(std::forward<Args>(args)...) {
}

// -----------------------------------------------------------------

template<class Type, class BaseType, class SuperType, class PointerType, class WeakPointer>
template<typename... Args_>
sharable<Type, BaseType, SuperType, PointerType, WeakPointer>::sharable(Args_&&... args) :
   BaseType(std::forward<Args_>(args)...) {
}

template<class Type, class BaseType, class SuperType, class PointerType, class WeakPointer>
template<class ... Args_>
PointerType sharable<Type, BaseType, SuperType, PointerType, WeakPointer>::create(Args_&&... args) {
   struct private_construct_t : public Type {
      private_construct_t(Args_&&... args) : Type(std::forward<Args_>(args)...) {}
   };

   auto ptr = std::make_shared<private_construct_t>(std::forward<Args_>(args)...);

   auto& ref = dynamic_cast<Type&>(*ptr);
   ref.this_ptr = ptr;

   if (static_cast<post_instantiation&>(ref).post_validate())
      return ptr;

   throw invalid_instantiation();
}

template<class Type, class BaseType, class SuperType, class PointerType, class WeakPointer>
template<class ... Args>
std::tuple<PointerType, Type&> sharable<Type, BaseType, SuperType, PointerType, WeakPointer>::lock_create(Args&&... args) {
   auto ptr = create(std::forward<Args>(args)...);
   return { ptr, *ptr };
}

template<class Type, class BaseType, class SuperType, class PointerType, class WeakPointer>
template<class Type_>
Type_& sharable<Type, BaseType, SuperType, PointerType, WeakPointer>::getAs() {
   return dynamic_cast<Type_&>(*this_ptr.lock());
}

template<class Type, class BaseType, class SuperType, class PointerType, class WeakPointer>
template<class Type_>
Type_ const& sharable<Type, BaseType, SuperType, PointerType, WeakPointer>::getAs() const {
   return dynamic_cast<Type_&>(*this_ptr.lock());
}

template<class Type, class BaseType, class SuperType, class PointerType, class WeakPointer>
PointerType sharable<Type, BaseType, SuperType, PointerType, WeakPointer>::lock() const {
   return this_ptr.lock();
}

template<class Type, class BaseType, class SuperType, class PointerType, class WeakPointer>
WeakPointer sharable<Type, BaseType, SuperType, PointerType, WeakPointer>::weak() const {
   return this_ptr;
}

//template<class Type, class BaseType, class SuperType, class PointerType, class WeakPointer>
//bool sharable<Type, BaseType, SuperType, PointerType, WeakPointer>::post_validate() {
//   return true;
//}

template<class Sharable>
template<class... Args_>
sharable_construct_t<Sharable>::sharable_construct_t(Args_&&... args) :
   std::shared_ptr<Sharable>(Sharable::create(std::forward<Args_>(args)...)) {
}

// -----------------------------------------------------------------


template<class Key, class Value, std::size_t defaultDuration, class TimeType, template<class, class, class ... > class MapType, class... MapTypes>
TimedMap<Key, Value, defaultDuration, TimeType, MapType, MapTypes...>::TimedMap(TimedMap::this_type&& timedMap) :
   parent_type(std::move(timedMap.m_container)),
   m_timestamps(std::move(timedMap.m_timestamps)),
   m_duration(timedMap.m_duration) {
}

template<class Key, class Value, std::size_t defaultDuration, class TimeType, template<class, class, class ... > class MapType, class... MapTypes>
template<class... Pairs>
TimedMap<Key, Value, defaultDuration, TimeType, MapType, MapTypes...>::TimedMap(Pairs&&... pair) :
   parent_type(std::initializer_list<typename container_type::value_type>{{std::forward<typename Pairs::first_type>(pair.first), {std::forward<typename Pairs::second_type>(pair.second),
            std::chrono::time_point_cast<TimeType>(std::chrono::system_clock::now())}}...}) {
}

template<class Key, class Value, std::size_t defaultDuration, class TimeType, template<class, class, class...> class MapType, class... MapTypes>
TimedMap<Key, Value, defaultDuration, TimeType, MapType, MapTypes...>::TimedMap(TimeType duration) :
   m_duration(duration) {
}

template<class Key, class Value, std::size_t defaultDuration, class TimeType, template<class, class, class...> class MapType, class... MapTypes>
void TimedMap<Key, Value, defaultDuration, TimeType, MapType, MapTypes...>::garbage_collector() {
   std::lock_guard<std::recursive_mutex> const lock1 { m_writting_mutex };
   std::lock_guard<shared_recursive_mutex_t> const lock { m_mutex };

   if (!m_timestamps.size())
      return;

   auto const time = std::chrono::time_point_cast<TimeType>(std::chrono::system_clock::now() - m_duration);

   auto const timestamp_end = m_timestamps.upper_bound(time);
   for (auto timestamp = m_timestamps.begin(); timestamp != timestamp_end; timestamp = m_timestamps.erase(timestamp))
      m_container.erase(timestamp->second);

   m_next_garbage = std::chrono::time_point<std::chrono::system_clock> {};

   if (m_timestamps.size())
      run_garbage(m_timestamps.begin()->first + m_duration);
}

template<class Key, class Value, std::size_t defaultDuration, class TimeType, template<class, class, class...> class MapType, class... MapTypes>
void TimedMap<Key, Value, defaultDuration, TimeType, MapType, MapTypes...>::run_garbage(std::chrono::time_point<std::chrono::system_clock> const& next) {
   auto next_garbage = m_next_garbage.load();
   bool exchanged = false;

   while ((next_garbage == decltype(next_garbage){} || next < next_garbage) && !(exchanged = m_next_garbage.compare_exchange_strong(next_garbage, next)));

   if (exchanged) {
      job::Scheduler::get() <<
         job::nodrop <<
         job::op::priority_t {{ next, next + m_duration / 4 }} <<
         job::op::name_t { "tmap_clean" } <<
         job::op::job_t {[&, weak = this->weak(), next]() {
            auto const lock = weak.lock();
            if (!lock || m_next_garbage.load() != next)
               return;

            garbage_collector();

         }} << job::flush;
   }
}

template<class Key, class Value, std::size_t defaultDuration, class TimeType, template<class, class, class...> class MapType, class... MapTypes>
TimedMap<Key, Value, defaultDuration, TimeType, MapType, MapTypes...>::~TimedMap() {
}

template<class Key, class Value, std::size_t defaultDuration, class TimeType, template<class, class, class...> class MapType, class... MapTypes>
template<class Key_>
bool TimedMap<Key, Value, defaultDuration, TimeType, MapType, MapTypes...>::contains(Key_&& key) const {
   static_assert(std::is_constructible_v<Key, decltype(key)>, "Incorrect Key type");

   std::shared_lock<shared_recursive_mutex_t> const lock { m_mutex };
   return m_container.find(std::forward<Key_>(key)) != m_container.end();
}

template<class Key, class Value, std::size_t defaultDuration, class TimeType, template<class, class, class...> class MapType, class... MapTypes>
template<class Key_>
typename TimedMap<Key, Value, defaultDuration, TimeType, MapType, MapTypes...>::const_iterator
TimedMap<Key, Value, defaultDuration, TimeType, MapType, MapTypes...>::find(Key_&& key) const {
   static_assert(std::is_constructible_v<Key, decltype(key)>, "Incorrect Key type");

   std::shared_lock<shared_recursive_mutex_t> const lock { m_mutex };
   return m_container.find(std::forward<Key_>(key));
}

template<class Key, class Value, std::size_t defaultDuration, class TimeType, template<class, class, class...> class MapType, class... MapTypes>
template<class Key_>
typename TimedMap<Key, Value, defaultDuration, TimeType, MapType, MapTypes...>::const_iterator
TimedMap<Key, Value, defaultDuration, TimeType, MapType, MapTypes...>::lower_bound(Key_&& key) const {
   static_assert(std::is_constructible_v<Key, decltype(key)>, "Incorrect Key type");

   std::shared_lock<shared_recursive_mutex_t> const lock { m_mutex };
   return m_container.lower_bound(std::forward<Key_>(key));
}

template<class Key, class Value, std::size_t defaultDuration, class TimeType, template<class, class, class...> class MapType, class... MapTypes>
template<class Key_>
typename TimedMap<Key, Value, defaultDuration, TimeType, MapType, MapTypes...>::const_iterator
TimedMap<Key, Value, defaultDuration, TimeType, MapType, MapTypes...>::upper_bound(Key_&& key) const {
   static_assert(std::is_constructible_v<Key, decltype(key)>, "Incorrect Key type");

   std::shared_lock<shared_recursive_mutex_t> const lock { m_mutex };
   return m_container.upper_bound(std::forward<Key_>(key));
}

template<class Key, class Value, std::size_t defaultDuration, class TimeType, template<class, class, class...> class MapType, class... MapTypes>
typename TimedMap<Key, Value, defaultDuration, TimeType, MapType, MapTypes...>::const_iterator
TimedMap<Key, Value, defaultDuration, TimeType, MapType, MapTypes...>::begin() const {
   return m_container.begin();
}

template<class Key, class Value, std::size_t defaultDuration, class TimeType, template<class, class, class...> class MapType, class... MapTypes>
typename TimedMap<Key, Value, defaultDuration, TimeType, MapType, MapTypes...>::const_iterator
TimedMap<Key, Value, defaultDuration, TimeType, MapType, MapTypes...>::end() const {
   return m_container.end();
}

template<class Iterator>
struct get_iterator {
   static constexpr inline auto get(Iterator const& iterator) {
      return iterator;
   }
};

template<class Value>
struct get_iterator<std::pair<Value, bool>> {
   static constexpr inline auto get(const std::pair<Value, bool>& iterator) {
      return iterator.first;
   }
};

template<class Key, class Value, std::size_t defaultDuration, class TimeType, template<class, class, class...> class MapType, class... MapTypes>
template<class Key_, class Value_>
auto TimedMap<Key, Value, defaultDuration, TimeType, MapType, MapTypes...>::insert(Key_&& key, Value_&& value) {
   static_assert(std::is_constructible_v<typename container_type::value_type::second_type::first_type,
         std::remove_reference_t<std::remove_const_t<decltype(value)>>>, "Incorrect Value type");
   static_assert(std::is_constructible_v<Key, decltype(key)>, "Incorrect Key type");

   std::lock_guard<std::recursive_mutex> const lock1 { m_writting_mutex };
   std::lock_guard<shared_recursive_mutex_t> const lock { m_mutex };
   auto const time = std::chrono::time_point_cast<TimeType>(std::chrono::system_clock::now());

   m_timestamps.insert( {time, std::forward<Key_>(key)});
   run_garbage(time + m_duration);

   return m_container.insert( {key, {std::forward<Value_>(value), time}});
}

template<class Key, class Value, std::size_t defaultDuration, class TimeType, template<class, class, class...> class MapType, class... MapTypes>
template<class Key_, class Value_>
std::pair<typename TimedMap<Key, Value, defaultDuration, TimeType, MapType, MapTypes...>::const_iterator, typename TimedMap<Key, Value, defaultDuration, TimeType, MapType, MapTypes...>::InsertorType>
TimedMap<Key, Value, defaultDuration, TimeType, MapType, MapTypes...>::replace(Key_&& key, Value_&& value) {
   static_assert(std::is_constructible_v<Value, decltype(value)>, "Incorrect Value type");
   static_assert(std::is_constructible_v<Key, decltype(key)>, "Incorrect Key type");

   std::lock_guard<std::recursive_mutex> const lock1 { m_writting_mutex };
   std::lock_guard<shared_recursive_mutex_t> const lock { m_mutex };
   auto const time = std::chrono::time_point_cast<TimeType>(std::chrono::system_clock::now());

   InsertorType exist = Inserted;

   auto mapIt = m_container.equal_range(key);
   while (mapIt.first != mapIt.second) {
      auto timestampIt = m_timestamps.equal_range(mapIt.first->second.second);

      while (timestampIt.first != timestampIt.second)
         if (timestampIt.first->second == mapIt.first->first)
            timestampIt.first = m_timestamps.erase(timestampIt.first);
         else
            ++timestampIt.first;

      if (value != mapIt.first->second.first)
         exist = Replaced;
      else if (exist == Inserted)
         exist = Unchanged;

      mapIt.first = m_container.erase(mapIt.first);
   }

   auto inserted_elem_ = m_container.insert( {key, {std::forward<Value_>(value), time}});
   auto inserted_elem = get_iterator<decltype(inserted_elem_)>::get(inserted_elem_);
   m_timestamps.insert( {time, std::forward<Key_>(key)});
   run_garbage(time + m_duration);

   return {inserted_elem, exist};
}

template<class Key, class Value, std::size_t defaultDuration, class TimeType, template<class, class, class...> class MapType, class... MapTypes>
template<class Key_>
Value const& TimedMap<Key, Value, defaultDuration, TimeType, MapType, MapTypes...>::at(Key_&& key) const {
   static_assert(std::is_constructible_v<Key, decltype(key)>, "Incorrect Key type");

   std::shared_lock<shared_recursive_mutex_t> const lock { m_mutex };
   auto const& elem = m_container.at(std::forward<Key_>(key));

   // todo update time if < max //if (elem.second )

   return elem.first;
}

template<class Key, class Value, std::size_t defaultDuration, class TimeType, template<class, class, class...> class MapType, class... MapTypes>
std::size_t TimedMap<Key, Value, defaultDuration, TimeType, MapType, MapTypes...>::size() const {
   std::shared_lock<shared_recursive_mutex_t> const lock { m_mutex };
   return m_container.size();
}

template<class Key, class Value, std::size_t defaultDuration, class TimeType, template<class, class, class...> class MapType, class... MapTypes>
std::shared_lock<shared_recursive_mutex_t> TimedMap<Key, Value, defaultDuration, TimeType, MapType, MapTypes...>::shared_lock() const {
   return std::shared_lock<shared_recursive_mutex_t>(m_mutex);
}

template<class Key, class Value, std::size_t defaultDuration, class TimeType, template<class, class, class...> class MapType, class... MapTypes>
std::lock_guard<shared_recursive_mutex_t> TimedMap<Key, Value, defaultDuration, TimeType, MapType, MapTypes...>::lock_guard() const {
   return std::lock_guard<shared_recursive_mutex_t>(m_mutex);
}

template<class Key, class Value, std::size_t defaultDuration, class TimeType, template<class, class, class...> class MapType, class... MapTypes>
std::lock_guard<std::recursive_mutex> TimedMap<Key, Value, defaultDuration, TimeType, MapType, MapTypes...>::writting_lock_guard() const {
   return std::lock_guard<std::recursive_mutex>(m_writting_mutex);
}

// -----------------------------------------------------------------

template<class Value, std::size_t defaultDuration, class TimeType, template<class, class, class...> class SetType, class... SetTypes>
void TimedSet<Value, defaultDuration, TimeType, SetType, SetTypes...>::garbage_collector() {
   std::lock_guard<std::recursive_mutex> const lock1 { m_writting_mutex };
   std::lock_guard<shared_recursive_mutex_t> const lock { m_mutex };

   if (!m_timestamps.size())
      return;

   auto const time = std::chrono::time_point_cast<TimeType>(std::chrono::system_clock::now() - m_duration);

   auto const timestamp_end = m_timestamps.upper_bound(time);
   for (auto timestamp = m_timestamps.begin(); timestamp != timestamp_end; ++timestamp)
      m_container.erase(timestamp->second);

   m_timestamps.erase(m_timestamps.begin(), timestamp_end);

   m_next_garbage = std::chrono::time_point<std::chrono::system_clock> {};

   if (m_timestamps.size())
      run_garbage(m_timestamps.begin()->first + m_duration);
}

template<class Value, std::size_t defaultDuration, class TimeType, template<class, class, class...> class SetType, class... SetTypes>
void TimedSet<Value, defaultDuration, TimeType, SetType, SetTypes...>::run_garbage(std::chrono::time_point<std::chrono::system_clock> const& next) {
   auto next_garbage = m_next_garbage.load();
   bool exchanged = false;

   while ((next_garbage == decltype(next_garbage){} || next < next_garbage) && !(exchanged = m_next_garbage.compare_exchange_strong(next_garbage, next)));

   if (exchanged) {
      job::Scheduler::get() <<
         job::nodrop <<
         job::op::priority_t {{ next, next + m_duration / 4 }} <<
         job::op::name_t { "tset_clean" } <<
         job::op::job_t {[&, weak = this->weak(), next]() {
            auto const lock = weak.lock();
            if (!lock || m_next_garbage.load() != next)
               return;

            garbage_collector();

         }} << job::flush;
   }
}

template<class Value, std::size_t defaultDuration, class TimeType, template<class, class, class ... > class SetType, class... SetTypes>
inline TimedSet<Value, defaultDuration, TimeType, SetType, SetTypes...>::TimedSet(TimedSet::this_type&& timedSet) :
   parent_type(std::move(timedSet)),
   find_members_type(parent_type::m_container, m_mutex) {
}

template<class Value, std::size_t defaultDuration, class TimeType, template<class, class, class...> class SetType, class... SetTypes>
TimedSet<Value, defaultDuration, TimeType, SetType, SetTypes...>::TimedSet(TimeType duration) :
   find_members_type(parent_type::m_container, m_mutex, m_writting_mutex),
   m_duration(duration) {
}

template<class Value, std::size_t defaultDuration, class TimeType, template<class, class, class...> class SetType, class... SetTypes>
TimedSet<Value, defaultDuration, TimeType, SetType, SetTypes...>::~TimedSet() {
}

template<class Value, std::size_t defaultDuration, class TimeType, template<class, class, class...> class SetType, class... SetTypes>
template<class Value_>
auto TimedSet<Value, defaultDuration, TimeType, SetType, SetTypes...>::insert(Value_&& value) {
   static_assert(std::is_constructible_v<typename container_type::value_type::first_type, decltype(value)>, "Incorrect Value type");
   std::lock_guard<std::recursive_mutex> const lock1 { m_writting_mutex };
   std::lock_guard<shared_recursive_mutex_t> const lock { m_mutex };
   auto const time = std::chrono::time_point_cast<TimeType>(std::chrono::system_clock::now());

   auto element = std::pair<Value_, decltype(time)>{std::forward<Value_>(value), time};

   m_timestamps.insert({time, element});
   run_garbage(time + m_duration);

   return m_container.insert(element);
}

template<class Value, std::size_t defaultDuration, class TimeType, template<class, class, class...> class SetType, class... SetTypes>
bool TimedSet<Value, defaultDuration, TimeType, SetType, SetTypes...>::erase(
      typename TimedSet<Value, defaultDuration, TimeType, SetType, SetTypes...>::const_iterator it) const {
   std::lock_guard<std::recursive_mutex> const lock1 { m_writting_mutex };
   std::lock_guard<shared_recursive_mutex_t> const lock { m_mutex };
//
// bool found = false;
//
// for (auto timestamp = m_timestamps.equal_range(it->second); timestamp.first != timestamp.second; ++timestamp.first)
//    if (timestamp.first->second == it) {
//       m_timestamps.erase(timestamp.first);
//       found = true;
//       break;
//    }
//
// if (!found)
//    return false;
//
// m_container.erase(it);
// return true;
   throw;//todo
}

template<class Value, std::size_t defaultDuration, class TimeType, template<class, class, class...> class SetType, class... SetTypes>
Value const& TimedSet<Value, defaultDuration, TimeType, SetType, SetTypes...>::front() const {
   std::shared_lock<shared_recursive_mutex_t> const lock { m_mutex };
   return m_container.front();
}

template<class Value, std::size_t defaultDuration, class TimeType, template<class, class, class...> class SetType, class... SetTypes>
typename TimedSet<Value, defaultDuration, TimeType, SetType, SetTypes...>::const_iterator
TimedSet<Value, defaultDuration, TimeType, SetType, SetTypes...>::begin() const {
   std::shared_lock<shared_recursive_mutex_t> const lock { m_mutex };
   return m_container.begin();
}

template<class Value, std::size_t defaultDuration, class TimeType, template<class, class, class...> class SetType, class... SetTypes>
typename TimedSet<Value, defaultDuration, TimeType, SetType, SetTypes...>::const_iterator
TimedSet<Value, defaultDuration, TimeType, SetType, SetTypes...>::end() const {
   std::shared_lock<shared_recursive_mutex_t> const lock { m_mutex };
   return m_container.end();
}

template<class Value, std::size_t defaultDuration, class TimeType, template<class, class, class...> class SetType, class... SetTypes>
Value const& TimedSet<Value, defaultDuration, TimeType, SetType, SetTypes...>::at(std::size_t pos) const {
   std::shared_lock<shared_recursive_mutex_t> const lock { m_mutex };
   return m_container.at(pos).first;
}

template<class Value, std::size_t defaultDuration, class TimeType, template<class, class, class...> class SetType, class... SetTypes>
std::size_t TimedSet<Value, defaultDuration, TimeType, SetType, SetTypes...>::size() const {
   std::shared_lock<shared_recursive_mutex_t> const lock { m_mutex };
   return m_container.size();
}

template<class Value, std::size_t defaultDuration, class TimeType, template<class, class, class...> class SetType, class... SetTypes>
std::shared_lock<shared_recursive_mutex_t> TimedSet<Value, defaultDuration, TimeType, SetType, SetTypes...>::shared_lock() const {
   return std::shared_lock<shared_recursive_mutex_t> { m_mutex };
}

template<class Value, std::size_t defaultDuration, class TimeType, template<class, class, class...> class SetType, class... SetTypes>
std::lock_guard<shared_recursive_mutex_t> TimedSet<Value, defaultDuration, TimeType, SetType, SetTypes...>::lock_guard() const {
   return std::lock_guard<shared_recursive_mutex_t> { m_mutex };
}

template<class Value, std::size_t defaultDuration, class TimeType, template<class, class, class...> class SetType, class... SetTypes>
std::lock_guard<std::recursive_mutex> TimedSet<Value, defaultDuration, TimeType, SetType, SetTypes...>::writting_lock_guard() const {
   return std::lock_guard<std::recursive_mutex> { m_writting_mutex };
}

// -----------------------------------------------------------------

template<class Type, bool enable>
FindMembers<Type, enable>::FindMembers(Type&, shared_recursive_mutex_t&, std::recursive_mutex&) {
}

template<class Type>
FindMembers<Type, true>::FindMembers(Type& container, shared_recursive_mutex_t& mutex, std::recursive_mutex& writting_mutex) :
      m_container(container), m_mutex(mutex), m_writting_mutex(writting_mutex) {
}

template<class Type>
template<class Value>
auto FindMembers<Type, true>::find(Value&& value) const {
   static_assert(std::is_constructible_v<typename Type::value_type::first_type, decltype(value)>, "Incorrect Value type");

   std::shared_lock<shared_recursive_mutex_t> const lock { m_mutex };
   return m_container.find({std::forward<Value>(value), {}});
}

template<class Type>
template<class Value>
auto FindMembers<Type, true>::find(Value&& value) {
   static_assert(std::is_constructible_v<typename Type::value_type::first_type, decltype(value)>, "Incorrect Value type");

   std::shared_lock<shared_recursive_mutex_t> const lock { m_mutex };
   return m_container.find({std::forward<Value>(value), {}});
}

template<class Type>
template<class Value>
auto FindMembers<Type, true>::erase(Value&& value) const {
   static_assert(std::is_constructible_v<typename Type::value_type::first_type, decltype(value)>, "Incorrect Value type");

   std::lock_guard<std::recursive_mutex> const lock1 { m_writting_mutex };
   std::lock_guard<shared_recursive_mutex_t> const lock { m_mutex };
   return m_container.erase({std::forward<Value>(value), {}});
}

// -----------------------------------------------------------------

template<typename First, typename Sec>
inline bool degraded_pair<First, Sec>::operator ==(degraded_pair const& pair) const {
   return std::pair<First, Sec>::first == pair.first;
}

template<typename First, typename Sec>
inline bool degraded_pair<First, Sec>::operator <(degraded_pair const& pair) const {
   return std::pair<First, Sec>::first < pair.first;
}

template<typename First, typename Sec>
inline bool degraded_pair<First, Sec>::operator >(degraded_pair const& pair) const {
   return std::pair<First, Sec>::first > pair.first;
}

template<typename First, typename Sec>
inline bool degraded_pair<First, Sec>::operator <=(degraded_pair const& pair) const {
   return std::pair<First, Sec>::first <= pair.first;
}

template<typename First, typename Sec>
inline bool degraded_pair<First, Sec>::operator >=(degraded_pair const& pair) const {
   return std::pair<First, Sec>::first >= pair.first;
}

// -----------------------------------------------------------------

template<class ThisType_, class Type_>
template<class PointerType_>
decorator_t<ThisType_, Type_>::decorator_t(PointerType_&& parent) :
      m_parent(std::forward<PointerType_>(parent)) {
   assert(parent);
   static_assert(!std::is_same_v<PointerType_, std::nullptr_t>, "decorator must decorate a valid decorable !");
}

template<class ThisType_, class Type_>
typename decorator_t<ThisType_, Type_>::pointer_type_t
decorator_t<ThisType_, Type_>::base() const {
   assert(m_parent);
   return m_parent->base();
}

template<class ThisType_, class Type_>
typename decorator_t<ThisType_, Type_>::pointer_type_t
decorator_t<ThisType_, Type_>::parent() const {
   assert(m_parent);
   return m_parent;
}


template<class ThisType_, class Type_>
bool decorator_t<ThisType_, Type_>::post_validate() {
   parent()->add_child(this->lock());
   return true;
}

// -----------------------------------------------------------------

template<class ThisType_, class Type_>
void decorable_t<ThisType_, Type_>::add_child(pointer_type_t const& child) {
   m_childs.push_back(child);
}

template<class ThisType_, class Type_>
const typename decorable_t<ThisType_, Type_>::childs_t&
decorable_t<ThisType_, Type_>::childs() const {
   return m_childs;
}

template<class ThisType_, class Type_>
typename decorable_t<ThisType_, Type_>::pointer_type_t
decorable_t<ThisType_, Type_>::base() const {
   return this->lock();
}

template<class ThisType_, class Type_>
typename decorable_t<ThisType_, Type_>::pointer_type_t
decorable_t<ThisType_, Type_>::parent() const {
   return nullptr;
}

// -----------------------------------------------------------------

template<class T, bool enable>
template<class... Args>
ReverseMembers<T, enable>::ReverseMembers(Args&& ... args) :
      m_container(std::forward<Args>(args)...) {
}

template<class T>
typename ReverseMembers<T, true>::const_reverse_iterator ReverseMembers<T, true>::rbegin() const {
   return m_container.rbegin();
}

template<class T>
typename ReverseMembers<T, true>::const_reverse_iterator ReverseMembers<T, true>::rend() const {
   return m_container.rend();
}

// -----------------------------------------------------------------

class PacketForwarder;

template<std::size_t N>
struct unpack {
   template<class Func, class Tuple, class ... Args>
   static constexpr inline auto right(Func& func, Tuple&& tuple, Args&& ... args) {
      return unpack<N - 1>::template right(func, std::forward<Tuple>(tuple),
            std::forward<Args>(args)..., std::forward<decltype(std::get<N-1>(std::forward<Tuple>(tuple)))>(std::get<N-1>(tuple)));
   }
   template<class Func, class Tuple, class ... Args>
   static constexpr inline auto left(Func& func, Tuple&& tuple, Args&& ... args) {
      return unpack<N - 1>::template left(func, std::forward<Tuple>(tuple), std::forward<decltype(std::get<N-1>(std::forward<Tuple>(tuple)))>(std::get<N-1>(tuple)),
            std::forward<Args>(args)...);
   }
};

template<>
struct unpack<1> {
   template<class Func, class Tuple, class ... Args>
   static constexpr inline auto right(Func& func, Tuple&& tuple, Args&& ... args) {
      return func(std::forward<Args>(args)..., std::forward<decltype(std::get<0>(std::forward<Tuple>(tuple)))>(std::get<0>(tuple)));
   }
   template<class Func, class Tuple, class ... Args>
   static constexpr inline auto left(Func& func, Tuple&& tuple, Args&& ... args) {
      return func(std::forward<decltype(std::get<0>(std::forward<Tuple>(tuple)))>(std::get<0>(tuple)), std::forward<Args>(args)...);
   }
};

template<>
struct unpack<0> {
   template<class Func, class Tuple>
   static constexpr inline auto right(Func& func, Tuple&& tuple) {
      return func();
   }
   template<class Func, class Tuple>
   static constexpr inline auto left(Func& func, Tuple&& tuple) {
      return func();
   }
};

template<class Func, class Tuple>
static constexpr inline auto unpack_right(Func& func, Tuple&& tuple) {
   return unpack<std::tuple_size_v<std::remove_reference_t<Tuple>>>::right(func, std::forward<Tuple>(tuple));
}

template<class Func, class Tuple>
static constexpr inline auto unpack_left(Func& func, Tuple&& tuple) {
   return unpack<std::tuple_size_v<std::remove_reference_t<Tuple>>>::left(func, std::forward<Tuple>(tuple));
}

// -----------------------------------------------------------------

template<typename... Args_>
std::size_t observable_t::notify(Args_ const&... args) { // @suppress("Missing const-qualification")
   std::size_t nbNotified = 0;
   std::shared_lock<std::shared_mutex> const lock { m_mutex };

   for (auto const& observer : m_observers) {
      assert(observer);
      nbNotified += observer->on_notify(args...);
   }

   return nbNotified;
}

// -----------------------------------------------------------------

template<class... Observables_>
observer_base_t::observer_base_t(Observables_&&... observables) {
   (m_observables.emplace_back(std::forward<Observables_>(observables)),...);
}

template<class... Args_>
bool observer_base_t::on_notify(Args_ const&... args) { // @suppress("Missing const-qualification")
   if (auto* this_weak = dynamic_cast<weak_observer_proxy_t*>(this); this_weak)
      return this_weak->on_notify<Args_...>(args...);
    else if (auto* this_ = dynamic_cast<observer_method_t<Args_ const&...>*>(this); this_)
      return this_->on_notify(args...);
   return false;
}

// -----------------------------------------------------------------

template<class... Observables_>
weak_observer_base_t::weak_observer_base_t(Observables_&&... observables) :
   m_proxy(weak_observer_proxy_t::create(std::forward<Observables_>(observables)...)) {
}

template<class Type_>
bool weak_observer_t<Type_>::post_validate() {
   std::dynamic_pointer_cast<weak_observer_proxy_t>(weak_observer_base_t::m_proxy)->m_observer = weak();
   return sharable<Type_, weak_observer_base_t>::post_validate();
}

// -----------------------------------------------------------------

template<class... Args_>
bool weak_observer_proxy_t::on_notify(Args_ const&... args) { // @suppress("Missing const-qualification") todo suppressed before parent on_notify
   auto const observer = m_observer.lock();
   if (!observer)
      return false;
   if (auto const this_ = std::dynamic_pointer_cast<observer_method_t<Args_ const&...>>(observer); this_)
      return this_->on_notify(args...);
   return false;
}

// -----------------------------------------------------------------

#endif //SRC_UTILS_INL_
