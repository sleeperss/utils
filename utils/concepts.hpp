// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------
// --  █████╗ ██╗     ███████╗██╗  ██╗    ██╗  ██╗ ██████╗ ███╗   ███╗███████╗ ----
// -- ██╔══██╗██║     ██╔════╝╚██╗██╔╝    ██║  ██║██╔═══██╗████╗ ████║██╔════╝ ----
// -- ███████║██║     █████╗   ╚███╔╝     ███████║██║   ██║██╔████╔██║█████╗   ----
// -- ██╔══██║██║     ██╔══╝   ██╔██╗     ██╔══██║██║   ██║██║╚██╔╝██║██╔══╝   ----
// -- ██║  ██║███████╗███████╗██╔╝ ██╗    ██║  ██║╚██████╔╝██║ ╚═╝ ██║███████╗ ----
// -- ╚═╝  ╚═╝╚══════╝╚══════╝╚═╝  ╚═╝    ╚═╝  ╚═╝ ╚═════╝ ╚═╝     ╚═╝╚══════╝ ----
// --                                                                          ----
// -----------------------------------------------------    concepts           ----
// --------------------------------------------------------------------------------
//
//      Copyright 2022, Alexandre GARCIN, All rights reserved.
//
//                                                   Created on 24-09-2022 13:51:30

#pragma once

#include <concepts>
#include <type_traits>

// ----------------------- const  -------------------------------------------------

template<typename Type_>
concept _not_const = !std::is_const_v<std::remove_reference_t<Type_>>;
template<typename Type_>
concept _const = std::is_const_v<std::remove_reference_t<Type_>>;

// ----------------------- fundamental --------------------------------------------

template<typename Type_>
concept _fundamental = std::is_fundamental_v<std::remove_const_t<std::remove_reference_t<Type_>>>;
template<typename Type_>
concept _not_fundamental = !std::is_fundamental_v<std::remove_const_t<std::remove_reference_t<Type_>>>;

// ----------------------- comparable   -------------------------------------------

template<auto value1, auto value2>
concept _greater = value1 > value2;

template<typename func_>
concept _function = requires(func_ func) {
   func();
};