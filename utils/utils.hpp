// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------
// --  █████╗ ██╗     ███████╗██╗  ██╗    ██╗  ██╗ ██████╗ ███╗   ███╗███████╗ ----
// -- ██╔══██╗██║     ██╔════╝╚██╗██╔╝    ██║  ██║██╔═══██╗████╗ ████║██╔════╝ ----
// -- ███████║██║     █████╗   ╚███╔╝     ███████║██║   ██║██╔████╔██║█████╗   ----
// -- ██╔══██║██║     ██╔══╝   ██╔██╗     ██╔══██║██║   ██║██║╚██╔╝██║██╔══╝   ----
// -- ██║  ██║███████╗███████╗██╔╝ ██╗    ██║  ██║╚██████╔╝██║ ╚═╝ ██║███████╗ ----
// -- ╚═╝  ╚═╝╚══════╝╚══════╝╚═╝  ╚═╝    ╚═╝  ╚═╝ ╚═════╝ ╚═╝     ╚═╝╚══════╝ ----
// --                                                                          ----
// -----------------------------------------------------    utils              ----
// --------------------------------------------------------------------------------
//
//      Copyright 2019, Alexandre GARCIN, All rights reserved.
//
//                                                   Created on 12-09-2019 13:52:31

#pragma once

#include <logger/logger.h>

#include <algorithm>
#include <array>
#include <atomic>
#include <chrono>
#include <condition_variable>
#include <cstddef>
#include <functional>
#include <list>
#include <map>
#include <memory>
#include <mutex>
#include <ostream>
#include <set>
#include <shared_mutex>
#include <string>
#include <thread>
#include <tuple>
#include <type_traits>
#include <unordered_map>
#include <utility>
#include <vector>

#include "concepts.hpp"

#define SEP ,

#define PACKED __attribute__((__packed__))

#define packed(type_name) struct PACKED { /*// @suppress("Unused Macro") */\
   type_name;\
}

#define packed_(type_name, struct_name) struct PACKED struct_name { /*// @suppress("Unused Macro") */\
   type_name;\
}

// -----------------------------------------------------------------

std::string byte_str(std::size_t);

// -----------------------------------------------------------------

template<class Type_>
static inline void**& addr(Type_& ptr) {
   return reinterpret_cast<void**&>(ptr);
}

template<class Type_>
static inline void** const& addr(Type_ const& ptr) {
   return reinterpret_cast<void** const&>(ptr);
}

template<class Type_>
static inline uint8_t*& byte(Type_& ptr) {
   return reinterpret_cast<uint8_t*&>(ptr);
}

template<class Type_>
static inline void*& ptr(Type_& ptr) {
   return reinterpret_cast<void*&>(ptr);
}

template<class Type_>
static inline uint8_t* const& byte(Type_ const& ptr) {
   return reinterpret_cast<uint8_t* const&>(ptr);
}

template<class Type_>
static inline uint8_t const* const& byte(Type_ const* const& ptr) {
   return reinterpret_cast<uint8_t const* const&>(ptr);
}

// -----------------------------------------------------------------

struct string_hash {
  using is_transparent = void; // enables heterogenous lookup

  std::size_t operator()(std::string_view sv) const {
    std::hash<std::string_view> hasher;
    return hasher(sv);
  }
};

// -----------------------------------------------------------------
#ifdef __aarch64__
#define SHADOW_OFFSET 0x1000000000
#else
#define SHADOW_OFFSET 0x7fff8000
#endif

#ifndef MAP_FIXED_NOREPLACE
#define MAP_FIXED_NOREPLACE 0x100000
#endif

using ip4_t = uint32_t;
using ip6_t = uint64_t;
using mac_t = ip6_t;

// struct ip6_bf_t {
//    uint64_t _ :48;
////#pragma GCC diagnostic ignored "-Wuninitialized"
////   uint64_t garbage :16;
//
//   ip6_bf_t() = default; // @suppress("Not initialized")
//   template<class Type>
//   ip6_bf_t(Type);
//   ip6_bf_t(ip6_bf_t const&);
//   template<class Type>
//   ip6_bf_t& operator=(Type);
//   ip6_bf_t& operator=(ip6_bf_t);
//
//   template<class Type>
//   bool operator<(Type) const;
//   bool operator<(ip6_bf_t) const;
//   template<class Type>
//   bool operator<=(Type) const;
//   bool operator<=(ip6_bf_t) const;
//   template<class Type>
//   bool operator>(Type) const;
//   bool operator>(ip6_bf_t) const;
//   template<class Type>
//   bool operator>=(Type) const;
//   bool operator>=(ip6_bf_t) const;
//   template<class Type>
//   bool operator==(Type) const;
//   bool operator==(ip6_bf_t) const;
//   template<class Type>
//   bool operator!=(Type) const;
//   bool operator!=(ip6_bf_t) const;
//   bool operator!() const;
//   ip6_bf_t operator-();
//   ip6_bf_t operator+();
//   ip6_bf_t operator~();
//   ip6_bf_t& operator++();
//   template<class Type>
//   ip6_bf_t operator++(Type);
//   ip6_bf_t& operator--();
//   template<class Type>
//   ip6_bf_t operator--(Type);
//   template<class Type>
//   ip6_bf_t operator-(Type) const;
//   template<class Type>
//   ip6_bf_t operator+(Type) const;
//   template<class Type>
//   ip6_bf_t& operator+=(Type);
//   ip6_bf_t& operator+=(ip6_bf_t);
//   template<class Type>
//   ip6_bf_t& operator-=(Type);
//   ip6_bf_t& operator-=(ip6_bf_t);
//   template<class Type>
//   ip6_bf_t& operator&=(Type);
//   ip6_bf_t& operator&=(ip6_bf_t);
//   template<class Type>
//   ip6_bf_t& operator|=(Type);
//   ip6_bf_t& operator|=(ip6_bf_t);
//   template<class Type>
//   ip6_bf_t operator&(Type) const;
//   template<class Type>
//   ip6_bf_t operator|(Type) const;
//
//   uint64_t const& operator*() const;
//   uint64_t& operator*();
//
//   static ip6_bf_t max();
//   static ip6_bf_t min();
//   static constexpr std::size_t size();
//};
// using mac_bf_t = ip6_bf_t;

///*####################*/namespace std        {/*###################*/
//template<>
//struct numeric_limits<ip6_bf_t> {
//   static _GLIBCXX_USE_CONSTEXPR bool is_specialized = true;
//
//   static ip6_bf_t
//   min() _GLIBCXX_USE_NOEXCEPT { return ip6_bf_t::min(); }
//
//   static ip6_bf_t
//   max() _GLIBCXX_USE_NOEXCEPT { return ip6_bf_t::max(); }
//};
//
//template<>
//struct hash<ip6_bf_t> {
//   size_t operator()(ip6_bf_t const& __s) const noexcept { return hash<uint64_t>()(*__s); }
//};
///*###################*/}// namespace std      /*###################*/

using port_t = uint16_t;
struct ip_port_t {
   ip4_t ip;
   port_t port;
};

// -----------------------------------------------------------------

void mac_copy(uint8_t const[6], mac_t&);
void ip_copy(uint8_t const[4], ip4_t&);

void mac_copy(mac_t const, uint8_t const[6]);
void ip_copy(ip4_t const, uint8_t const[4]);

void mac_copy(uint8_t const src[6], uint8_t dest[6]);
void ip_copy(uint8_t const src[4], uint8_t dest[4]);

mac_t mac_converter(uint8_t const[6]);
ip4_t ip_converter(uint8_t const[4]);

ip4_t ip_converter(std::array<uint8_t, 4> const&);

std::array<uint8_t, 6> mac_converter(mac_t const);
std::array<uint8_t, 4> ip_converter(ip4_t const);
std::array<uint8_t, 6> ip6_converter(ip6_t const);

std::array<uint8_t, 6> mac_converter(std::string const&);
std::array<uint8_t, 4> ip_converter(std::string const&);
std::array<uint8_t, 6> ip6_converter(std::string const&);

std::string mac_converter_s(mac_t const);
std::string ip_converter_s(ip4_t const);
std::string ip6_converter_s(ip6_t);

std::string mac_converter_s(uint8_t const[6]);
std::string ip_converter_s(uint8_t const[4]);
std::string ip6_converter_s(uint8_t const[6]);

// -----------------------------------------------------------------

mac_t r_mac_converter(uint8_t const[6]);
ip4_t r_ip_converter(uint8_t const[4]);

std::array<uint8_t, 6> r_mac_converter(mac_t const);
std::array<uint8_t, 4> r_ip_converter(ip4_t const);
std::array<uint8_t, 6> r_ip6_converter(ip6_t const);

std::array<uint8_t, 6> r_mac_converter(std::string const&);
std::array<uint8_t, 4> r_ip_converter(std::string const&);
std::array<uint8_t, 6> r_ip6_converter(std::string const&);

std::string r_mac_converter_s(mac_t const);
std::string r_ip_converter_s(ip4_t const);
std::string r_ip6_converter_s(ip6_t);

std::string r_mac_converter_s(uint8_t const[6]);
std::string r_ip_converter_s(uint8_t const[4]);
std::string r_ip6_converter_s(uint8_t const[6]);

// -----------------------------------------------------------------

template<_function func_>
struct scoped_t;

template<_function func_>
requires std::is_reference_v<func_>
struct scoped_t<func_> {
   func_& do_;

   explicit __attribute__((always_inline)) inline scoped_t(func_& do_) :
       do_(do_) {
   }

   __attribute__((always_inline)) inline ~scoped_t() {
      do_();
   }
};

template<_function func_>
requires(!std::is_reference_v<func_>) struct scoped_t<func_> {
   func_ do_;

   explicit __attribute__((always_inline)) inline scoped_t(func_&& do_) :
       do_(std::move(do_)) {
   }

   __attribute__((always_inline)) inline ~scoped_t() {
      do_();
   }
};

template<_function func_>
scoped_t(func_) -> scoped_t<func_>;

#define CONCAT_(x, y) x##y
#define CONCAT(x, y) CONCAT_(x, y)

#define SCOPE_EXIT(x) \
   auto const CONCAT(raai_, __COUNTER__) = scoped_t { x }

// -----------------------------------------------------------------

template<std::size_t value_>
static constexpr std::size_t compute = value_;

// -----------------------------------------------------------------

template<class Type_>
static constexpr std::size_t template_size_ { 0 };

template<template<class...> class Type_, class... Types_>
static constexpr std::size_t template_size_<Type_<Types_...>> = std::tuple_size_v<std::tuple<Types_...>>;

template<class Type_>
static constexpr std::size_t template_size = template_size_<std::remove_const_t<std::remove_reference_t<Type_>>>;

/*####################*/namespace tmplt      {/*###################*/
template<class Packer_>
struct unpacker;

template<template<class...> class Packer_, class... PackedTypes_>
struct unpacker<Packer_<PackedTypes_...>> {
    template<template<class...> class Type_, class... Types_>
    using left = Type_<PackedTypes_..., Types_...>;

    template<template<class...> class Type__, class... Types_>
    using right = Type__<Types_..., PackedTypes_...>;
};

template<template<class...> class Type_, class Packer_, class... Types_>
using unpack_left = typename unpacker<Packer_>::template left<Type_, Types_...>;

template<template<class...> class Type_, class Packer_>
using unpack = unpack_left<Type_, Packer_>;

template<template<class...> class Type_, class Packer_, class... Types_>
using unpack_right = typename unpacker<Packer_>::template right<Type_, Types_...>;

// -----------------------------------------------------------------

template<class Type_, class Func_, std::size_t begin_, std::size_t end_ = begin_, std::size_t index_ = begin_>
struct unpack_applier {
   template<class Type__, class... Args__>
   static constexpr auto apply(Type__&& packer, Args__&&... args) {
      return unpack_applier<Type_, Func_, begin_, end_, index_ + 1>::apply(std::forward<Type__>(packer), std::forward<Args__>(args)..., std::get<index_>(packer));
   }
};

template<class Type_, class Func_, std::size_t begin_, std::size_t end_>
struct unpack_applier<Type_, Func_, begin_, end_, end_> {
   template<class Type__, class... Args__>
   static constexpr auto apply(Type__&& packer, Args__&&... args) {
      return Func_::apply(std::forward<Args__>(args)..., std::get<end_>(packer));
   }
};

template<class Type_, class Func_, std::size_t begin_ = 0, std::size_t end_ = template_size<Type_>>
struct unpack_helper {
   template<class Type__, class... Args__>
   static constexpr auto apply(Type__&& packer, Args__&&... args) {
      return unpack_applier<Type_, Func_, begin_, end_ - 1>::apply(std::forward<Type__>(packer), std::forward<Args__>(args)...);
   }
};

// -----------------------------------------------------------------

template<class Type1_, class Type2_, template<class...> class ResultType_ = std::tuple>
struct merger;

template<template<class...> class Type1_, class Type2_, template<class...> class ResultType_, class... Types1_>
requires(!std::is_same_v<Type1_<Types1_...>, std::nullptr_t> && !std::is_same_v<Type2_, std::nullptr_t>) struct merger<Type1_<Types1_...>, Type2_, ResultType_> {
   template<class Type__>
   struct helper;

   template<template<class...> class Type2__, class... Types2__>
   struct helper<Type2__<Types2__...>> {
      using type = ResultType_<Types1_..., Types2__...>;
   };

   using type = typename helper<Type2_>::type;

   struct applier_pack2 {
      template<class... Args_>
      static constexpr auto apply(Args_&&... args) {
         return type { std::forward<Args_>(args)... };
      }
   };

   struct applier_pack1 {
      template<class Pack2__, class... Args__>
      static constexpr type apply(Pack2__&& pack2, Args__&&... args) {
         return unpack_helper<Pack2__, applier_pack2>::apply(std::forward<Pack2__>(pack2), std::forward<Args__>(args)...);
      }
   };

   template<class Pack1__, class Pack2__>
   static constexpr type merge(Pack1__&& pack1, Pack2__&& pack2) {
      return unpack_helper<Pack1__, applier_pack1>::apply(std::forward<Pack1__>(pack1), std::forward<Pack2__>(pack2));
   }
};

template<class Type2_, template<class...> class ResultType_>
requires(!std::is_same_v<Type2_, std::nullptr_t>) struct merger<std::nullptr_t, Type2_, ResultType_> {
   template<class Type2__>
   struct helper;

   template<template <class...> class Type__, class... Types__>
   struct helper<Type__<Types__...>> {
       using type = ResultType_<Types__...>;
   };

   using type = typename helper<Type2_>::type;

   struct applier {
      template<class... Args__>
      static constexpr type apply(Args__&&... args) {
         return type { std::forward<Args__>(args)... };
      }
   };

   template<class Pack1__, class Pack2__>
   static constexpr type merge(Pack1__&&, Pack2__&& pack2) {
      static_assert(std::is_same_v<std::remove_const_t<std::remove_reference_t<Pack1__>>, std::nullptr_t>, "Hum, pack1 must be nullptr");
      return unpack_helper<Pack2__, applier>::apply(std::forward<Pack2__>(pack2));
   }
};

template<class Type1_, template <class...> class ResultType_>
requires(!std::is_same_v<Type1_, std::nullptr_t>) struct merger<Type1_, std::nullptr_t, ResultType_> {
   template<class Type1__>
   struct helper;

   template<template <class...> class Type__, class... Types__>
   struct helper<Type__<Types__...>> {
       using type = ResultType_<Types__...>;
   };

   using type = typename helper<Type1_>::type;

   struct applier {
      template<class... Args__>
      static constexpr type apply(Args__&&... args) {
         return type { std::forward<Args__>(args)... };
      }
   };

   template<class Pack1__, class Pack2__>
   static constexpr type merge(Pack1__&& pack1, Pack2__&&) {
      static_assert(std::is_same_v<std::remove_const_t<std::remove_reference_t<Pack2__>>, std::nullptr_t>, "Hum, pack2 must be nullptr");
      return unpack_helper<Pack1__, applier>::apply(std::forward<Pack1__>(pack1));
   }
};

template<template <class...> class ResultType_>
struct merger<std::nullptr_t, std::nullptr_t, ResultType_> {
   using type = std::nullptr_t;

   template<class Pack1__, class Pack2__>
   static constexpr type merge(Pack1__&&, Pack2__&&) {
      static_assert(std::is_same_v<std::remove_const_t<std::remove_reference_t<Pack2__>>, std::nullptr_t>, "Hum, pack2 must be nullptr");
      static_assert(std::is_same_v<std::remove_const_t<std::remove_reference_t<Pack1__>>, std::nullptr_t>, "Hum, pack1 must be nullptr");
      return nullptr;
   }
};

template<class Type1_, class Type2_, template<class...> class ResultType_ = std::tuple>
using merge = typename merger<Type1_, Type2_, ResultType_>::type;

template<class Tuple_>
using tuple_last = std::tuple_element_t<std::size_t((std::tuple_size_v<Tuple_>) - 1), Tuple_>;

template<class Tuple_>
static constexpr auto get_last_tuple(Tuple_&& tuple) {
   return std::get<std::size_t((std::tuple_size_v<Tuple_>) - 1)>(std::forward<Tuple_>(tuple));
}

/*###################*/}// namespace tmplt    /*###################*/

// -----------------------------------------------------------------

template<bool const_, class Type>
struct conditional_const {
   using type = const Type;
};

template<class Type>
struct conditional_const<false, Type> {
   using type = std::remove_const_t<Type>;
};

template<bool enable, class Type>
using conditional_const_t = typename conditional_const<enable, Type>::type;

// -----------------------------------------------------------------

template<bool const_, class Type>
struct conditional_rref {
   using type = std::remove_reference_t<Type>&&;
};

template<class Type>
struct conditional_rref<false, Type> {
   using type = std::remove_reference_t<Type>;
};

template<bool enable, class Type>
using conditional_rref_t = typename conditional_rref<enable, Type>::type;

// -----------------------------------------------------------------

template<bool const_, class Type>
struct conditional_lref {
   using type = Type&;
};

template<class Type>
struct conditional_lref<false, Type> {
   using type = std::remove_reference_t<Type>;
};

template<bool enable, class Type>
using conditional_lref_t = typename conditional_lref<enable, Type>::type;

// -----------------------------------------------------------------

template<class Type1, class Type2>
using forward_ref = std::conditional_t<std::is_lvalue_reference_v<Type1>,
      Type2&, std::conditional_t<std::is_rvalue_reference_v<Type1>,
      std::remove_reference_t<Type2>&&,
      std::remove_reference_t<Type2>>>;

// -----------------------------------------------------------------

template<class Type1, class Type2>
using forward_const = forward_ref<Type2, conditional_const_t<std::is_const_v<std::remove_reference_t<Type1>>, std::remove_reference_t<Type2>>>;

// -----------------------------------------------------------------

template<class Type, class... Args>
std::unique_ptr<Type> make_unique_enabler(Args&&... args) {
   struct make_unique_enabler: Type {
      make_unique_enabler(Args&&... args) : Type(std::forward<Args>(args)...) {
      }
   };

   return std::make_unique<make_unique_enabler>(args...);
};

// -----------------------------------------------------------------

template<class Type, class... Args>
std::shared_ptr<Type> make_shared_enabler(Args&&... args) {
   struct make_shared_enabler: Type {
      make_shared_enabler(Args&&... args) : Type(std::forward<Args>(args)...) {
      }
   };

   return std::make_shared<make_shared_enabler>(std::forward<Args>(args)...);
};

// -----------------------------------------------------------------

template<std::size_t min_, std::size_t max_>
static constexpr std::size_t clip(std::size_t value) {
   return std::max(std::min(max_, value), min_);
}

// -----------------------------------------------------------------

template<bool cond, class BaseType>
struct inherit_if {
};

template<class BaseType>
struct inherit_if<true, BaseType> : BaseType {
   using BaseType::BaseType;
};

template<class WeakPointer>
struct sharable_member {
protected:
   WeakPointer this_ptr;
};

template<class Type_, class, class, class PointerType_, class>
class sharable;

struct post_instantiation {
   virtual ~post_instantiation() = default;
   virtual bool post_validate();

   template<class Type_, class, class, class PointerType_, class>
   friend class sharable;
};

template<class Type_, class BaseType_ = Type_, class SuperType_ = BaseType_, class PointerType_ = std::shared_ptr<SuperType_>, class WeakPointer_ = std::weak_ptr<SuperType_>>
class sharable : public inherit_if<!std::is_same_v<BaseType_, Type_>, BaseType_>, private virtual sharable_member<WeakPointer_>, protected virtual post_instantiation {
private:
   using sharable_member<WeakPointer_>::this_ptr;

protected:
   using sharable_type_t = sharable<Type_, BaseType_, SuperType_, PointerType_, WeakPointer_>;
   using pointer_type_t = PointerType_;
   using weak_type_t = WeakPointer_;

   using inherit_if<!std::is_same_v<BaseType_, Type_>, BaseType_>::inherit_if;

   sharable() = default;

   template<typename... Args_>
   sharable(Args_&&...);

public:
   sharable(sharable&) = delete;
   sharable(sharable const&) = delete;
   sharable(sharable&&) = delete;

   template<class ... Args>
   static pointer_type_t create(Args&&...);

   template<class... Args>
   static std::tuple<PointerType_, Type_&> lock_create(Args&&...);

   virtual pointer_type_t lock() const;
   virtual weak_type_t weak() const;

   template<class Type__>
   Type__& getAs();
   template<class Type__>
   Type__ const& getAs() const;
};

template<class Sharable>
class sharable_construct_t : public std::shared_ptr<Sharable> {
public:
   template<class... Args_>
   sharable_construct_t(Args_&&...);

   virtual ~sharable_construct_t() = default;
};

// -----------------------------------------------------------------

template<class Base_, class... Friends_>
struct friends_t;

template<class Base_, class Friend_, class... Friends_>
class friends_t<Base_, Friend_, Friends_...> : public friends_t<Base_, Friends_...> {
public:
   template<class... Args>
   friends_t(Args&&...);

   friend Friend_;
};

template<class Base, class Friend>
struct friends_t<Base, Friend> : public Base {
public:
   template<class... Args>
   friends_t(Args&&...);

   friend Friend;
};

template<class Base>
struct friends_t<Base> : public Base {
public:
   template<class... Args>
   friends_t(Args&&...);
};

// -----------------------------------------------------------------

class shared_recursive_mutex_t: private std::shared_mutex {
private:
   std::atomic<std::thread::id> owner {};
   std::size_t count {0};
   std::mutex shared_count_mutex;
   std::unordered_map<std::thread::id, std::size_t> shared_counts;

public:
   using std::shared_mutex::shared_mutex;

   void lock();
   void unlock();

   void lock_shared();
   void unlock_shared();
};

// -----------------------------------------------------------------

template<class U, typename U::const_iterator (U::*)(const typename U::value_type&) const>
struct has_find_member_h {
};

template<class U, class = has_find_member_h<U, &U::find>>
struct has_find_member {
   static constexpr bool value = false;
};

template<class U>
struct has_find_member<U, has_find_member_h<U, &U::find>> {
   static constexpr bool value = true;
};

template<class T>
static constexpr bool has_find_member_v = has_find_member<T>::value;

template<class T, bool enable>
class FindMembers {
protected:
   FindMembers(T&, shared_recursive_mutex_t&, std::recursive_mutex&);
};

template<class T>
class FindMembers<T, true> {
private:
   T& m_container;
   shared_recursive_mutex_t& m_mutex;
   std::recursive_mutex& m_writting_mutex;

protected:
   FindMembers(T&, shared_recursive_mutex_t&, std::recursive_mutex&);

public:
   template<class Value>
   auto find(Value&&) const;

   template<class Value>
   auto find(Value&&);

   template<class Value>
   auto erase(Value&&) const;
};

// -----------------------------------------------------------------

template<class U>
struct has_reverse_iterator {
   static constexpr bool value = false;
};

template<class U>
requires(!std::is_same_v<typename U::const_reverse_iterator, void>) struct has_reverse_iterator<U> {
   static constexpr bool value = true;
};

template<class T>
static constexpr bool has_reverse_iterator_v = has_reverse_iterator<T>::value;

template<class T, bool enable>
class ReverseMembers {
protected:
   using container_type = T;
   container_type m_container;

   ReverseMembers() = default;
   template<class... Args>
   ReverseMembers(Args&&...);
};

template<class T>
class ReverseMembers<T, true> {
protected:
   using container_type = T;
   container_type m_container;

   ReverseMembers() = default;
   template<class... Args>
   ReverseMembers(Args&&...);

public:
   using const_reverse_iterator = typename container_type::const_reverse_iterator;

   const_reverse_iterator rbegin() const;
   const_reverse_iterator rend() const;
};

// -----------------------------------------------------------------

template<class Key, class Value, std::size_t defaultDuration, class DurationType = std::chrono::seconds, template<class, class, class...> class MapType = std::unordered_map, class... MapTypes>
class TimedMap :
      public sharable<TimedMap<Key, Value, defaultDuration, DurationType, MapType, MapTypes...>>,
      public ReverseMembers<MapType<Key, std::pair<Value, std::chrono::time_point<std::chrono::system_clock, DurationType>>, MapTypes...>, has_reverse_iterator_v<MapType<Key, std::pair<Value, time_t>, MapTypes...>>> {
public:
   enum InsertorType {
      Inserted,
      Replaced,
      Unchanged
   };
   static_assert(defaultDuration, "Default duration must be positive");

protected:
   using this_type = TimedMap<Key, Value, defaultDuration, DurationType, MapType, MapTypes...>;
   using parent_type = ReverseMembers<MapType<Key, std::pair<Value, std::chrono::time_point<std::chrono::system_clock, DurationType>>, MapTypes...>, has_reverse_iterator_v<MapType<Key, std::pair<Value, time_t>, MapTypes...>>>;

   using typename parent_type::container_type;
   using parent_type::m_container;
   template<class Key_>
   struct pointer_hash {
      std::size_t operator()(Key_ const& key) const {
         return std::hash<typename Key_::value_type*>()(&*key);
      }
   };

   std::multimap<std::chrono::time_point<std::chrono::system_clock, DurationType>, Key> m_timestamps {};
   DurationType m_duration { DurationType(defaultDuration) };

   mutable shared_recursive_mutex_t m_mutex {};
   mutable std::recursive_mutex m_writting_mutex {};

   TimedMap(DurationType = DurationType(defaultDuration));
   TimedMap(this_type&&);
   template<class... Pairs>
   TimedMap(Pairs&&...);

public:
   using const_iterator = typename container_type::const_iterator;
   using typename sharable<TimedMap>::sharable_type_t;

   using sharable_type_t::create;

   virtual ~TimedMap();

   template<class Key_, class Value_>
   std::pair<const_iterator, InsertorType> replace(Key_&&, Value_&&);
   template<class Key_, class Value_>
   auto insert(Key_&&, Value_&&);

   template<class Key_>
   bool contains(Key_&&) const;

   template<class Key_>
   const_iterator find(Key_&&) const;

   template<class Key_>
   const_iterator lower_bound(Key_&&) const;

   template<class Key_>
   const_iterator upper_bound(Key_&&) const;

   const_iterator begin() const;
   const_iterator end() const;

   std::size_t size() const;

   std::shared_lock<shared_recursive_mutex_t> shared_lock() const;
   std::lock_guard<shared_recursive_mutex_t> lock_guard() const;
   std::lock_guard<std::recursive_mutex> writting_lock_guard() const;

   template<class Key_>
   Value const& at(Key_&&) const;

   Value const& front() const;
   Value const& back() const;

private:
   std::atomic<std::chrono::time_point<std::chrono::system_clock>> m_next_garbage {{}};

   void run_garbage(std::chrono::time_point<std::chrono::system_clock> const& next);
   void garbage_collector();

   friend sharable_type_t;
};

// -----------------------------------------------------------------

template<typename First, typename Sec>
struct degraded_pair : std::pair<First, Sec> {
   using std::pair<First, Sec>::pair;

   bool operator==(degraded_pair const&) const;
   bool operator<(degraded_pair const&) const;
   bool operator>(degraded_pair const&) const;
   bool operator<=(degraded_pair const&) const;
   bool operator>=(degraded_pair const&) const;
};

template<class Value, std::size_t defaultDuration, class DurationType, template<class, class, class...> class SetType = std::set, class... SetTypes>
class TimedSet :
public sharable<TimedSet<Value, defaultDuration, DurationType, SetType, SetTypes...>>,
public ReverseMembers<SetType<degraded_pair<Value, std::chrono::time_point<std::chrono::system_clock, DurationType>>, SetTypes...>, has_reverse_iterator_v<SetType<degraded_pair<Value, std::chrono::time_point<std::chrono::system_clock, DurationType>>, SetTypes...>>>,
public FindMembers<SetType<degraded_pair<Value, std::chrono::time_point<std::chrono::system_clock, DurationType>>, SetTypes...>, has_find_member_v<SetType<degraded_pair<Value, std::chrono::time_point<std::chrono::system_clock, DurationType>>, SetTypes...>>> {
protected:
   using time_type = std::chrono::time_point<std::chrono::system_clock, DurationType>;
   using value_type = degraded_pair<Value, time_type>;
   using this_type = TimedSet<Value, defaultDuration, DurationType, SetType, SetTypes...>;
   using parent_type = ReverseMembers<SetType<value_type, SetTypes...>, has_reverse_iterator_v<SetType<value_type, SetTypes...>>>;
   using find_members_type = FindMembers<SetType<value_type, SetTypes...>, has_find_member_v<SetType<value_type, SetTypes...>>>;

   using typename parent_type::container_type;
   using parent_type::m_container;
   template<class Key_>
   struct pointer_hash {
      std::size_t operator()(Key_ const& key) const {
         return std::hash<typename Key_::value_type*>()(&*key);
      }
   };

   std::multimap<time_type, typename container_type::value_type> m_timestamps;
   DurationType m_duration;

   mutable shared_recursive_mutex_t m_mutex;
   mutable std::recursive_mutex m_writting_mutex;

   TimedSet(DurationType = DurationType(defaultDuration));
   TimedSet(this_type&&);

public:
   using typename sharable<TimedSet>::sharable_type_t;
   using iterator = typename container_type::iterator;
   using const_iterator = typename container_type::const_iterator;

   using sharable_type_t::create;
   virtual ~TimedSet();

   template<class Value_>
   auto insert(Value_&&);

   bool erase(const_iterator) const;
   using find_members_type::erase;
   using find_members_type::find;

   Value const& front() const;

   const_iterator begin() const;
   const_iterator end() const;

   std::size_t size() const;

   std::shared_lock<shared_recursive_mutex_t> shared_lock() const;
   std::lock_guard<shared_recursive_mutex_t> lock_guard() const;
   std::lock_guard<std::recursive_mutex> writting_lock_guard() const;

   Value const& at(std::size_t) const;

private:
   std::atomic<std::chrono::time_point<std::chrono::system_clock>> m_next_garbage {{}};

   void run_garbage(std::chrono::time_point<std::chrono::system_clock> const& next);
   void garbage_collector();

   friend sharable_type_t;
};

// -----------------------------------------------------------------

template<class, class> class decorator_t;
template<class, class> class decorable_t;

template<class Type_>
struct decorator_base_t {
   using pointer_type_t = std::shared_ptr<Type_>;
   using childs_t = std::vector<std::weak_ptr<Type_>>;

private:
   decorator_base_t() = default;

protected:
   virtual pointer_type_t base() const = 0;
   virtual pointer_type_t parent() const = 0;
   virtual childs_t const& childs() const = 0;
   virtual void add_child(pointer_type_t const&) = 0;

public:
   virtual ~decorator_base_t() = default;

   template<class, class> friend class decorator_t;
   template<class, class> friend class decorable_t;
};

// -----------------------------------------------------------------

template<class ThisType_, class Type_ = ThisType_>
class decorable_t :
      protected decorator_base_t<Type_>,
      public sharable<ThisType_, Type_> {
   static constexpr void check() {
      static_assert(std::is_base_of_v<decorable_t<ThisType_, Type_>, ThisType_>, "ThisType_ must inherit from decorable_t !");
   }
protected:
   using typename sharable<ThisType_, Type_>::sharable_type_t;
   using typename decorator_base_t<Type_>::pointer_type_t;
   using typename decorator_base_t<Type_>::childs_t;

private:
   mutable childs_t m_childs;

protected:
   using sharable_type_t::sharable_type_t;

   using sharable_type_t::create;

   virtual pointer_type_t base() const override;
   virtual pointer_type_t parent() const override;

   void add_child(pointer_type_t const&) final override;
   childs_t const& childs() const final override;

   friend sharable_type_t;
   template<class, class> friend class decorator_t;
};

// -----------------------------------------------------------------
//dynamic_cast<decorator_base_t<Type_>&>(*
template<class ThisType_, class Type_>
class decorator_t :
      public sharable<ThisType_, Type_> {
   static_assert(std::is_base_of_v<decorator_base_t<Type_>, Type_>, "Only decorables can be decorated !");
   static constexpr void check() {
      static_assert(std::is_base_of_v<decorator_t<ThisType_, Type_>, ThisType_>, "ThisType_ must inherit from decorator_t !");
   }

protected:
   using typename sharable<ThisType_, Type_>::sharable_type_t;
   using typename decorator_base_t<Type_>::pointer_type_t;
   using typename decorator_base_t<Type_>::childs_t;

private:
   pointer_type_t m_parent;

protected:
   decorator_t() = delete;

   template<class PointerType_>
   decorator_t(PointerType_&& parent);

   using sharable_type_t::create;
   virtual bool post_validate() override;

   pointer_type_t base() const final override;
   pointer_type_t parent() const final override;

   template<class, class> friend class decorable_t;
   friend sharable_type_t;
};

// -----------------------------------------------------------------

class observer_base_t;

class observable_t {
private:
   std::list<std::shared_ptr<observer_base_t>> m_observers;
   std::shared_mutex m_mutex;

public:
   observable_t() = default;
   virtual ~observable_t();

   template<class... Args_>
   std::size_t notify(Args_ const&...);

   friend class observer_base_t;
};

// -----------------------------------------------------------------

class observer_base_t : protected sharable<observer_base_t> {
private:
   std::vector<std::weak_ptr<observable_t>> m_observables {};

protected:
   template<class... Observables_>
   observer_base_t(Observables_&&...);

private:
   template<class... Args_>
   bool on_notify(Args_ const&...);

   using sharable_type_t::create;

protected:
   virtual bool post_validate() override;

public:
   void observe(std::weak_ptr<observable_t> const& observable_);
   void unobserve(observable_t const* = nullptr);

   virtual ~observer_base_t();

   friend class observable_t;
   friend class weak_observer_proxy_t;
};

// -----------------------------------------------------------------

class weak_observer_proxy_t;

class weak_observer_base_t {
private:
   std::shared_ptr<observer_base_t> m_proxy {};

protected:
   template<class... Observables_>
   weak_observer_base_t(Observables_&&...);

public:
   void observe(std::weak_ptr<observable_t> const& observable_);
   void unobserve(observable_t const* = nullptr);

   virtual ~weak_observer_base_t();

   template<class> friend class weak_observer_t;
};

// -----------------------------------------------------------------

template<class Type_>
using observer_t = sharable<Type_, observer_base_t>;

template<class Type_>
class weak_observer_t : public sharable<Type_, weak_observer_base_t> {
private:
   using sharable_t = sharable<Type_, weak_observer_base_t>;

protected:
   using sharable_t::sharable;

   virtual bool post_validate() override;

public:
   using sharable_t::weak;
   using sharable_t::lock;
   using sharable_t::create;

   friend sharable_t;
};

// -----------------------------------------------------------------

class weak_observer_proxy_t : public observer_t<weak_observer_proxy_t> {
private:
   std::weak_ptr<weak_observer_base_t> m_observer;

protected:
   using observer_t<weak_observer_proxy_t>::observer_t;

private:
   template<class... Args_>
   bool on_notify(Args_ const&...);

public:
   virtual ~weak_observer_proxy_t() = default;

   friend class observer_base_t;
   template<class> friend class weak_observer_t;
};

// -----------------------------------------------------------------

template<class... Args_>
struct observer_method_t {
protected:
   virtual bool on_notify(Args_...) = 0;

   virtual ~observer_method_t() = default;

   friend class observer_base_t;
   friend class weak_observer_proxy_t;
};
