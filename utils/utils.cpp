#include "utils.hpp"
#include "utils.inl"

#include <assert.h>
#include <array>
#include <cstddef>
#include <iomanip>
#include <ios>
#include <memory>
#include <sstream>
#include <string>
#include <thread>
#include <typeinfo>

// -----------------------------------------------------------------

//ip6_bf_t ip6_bf_t::max() {
//   return (1ul << 6) - 1;
//}
//
//ip6_bf_t ip6_bf_t::min() {
//   return 0;
//}
//
//constexpr std::size_t ip6_bf_t::size() {
//   return 6;
//}
//
//bool ip6_bf_t::operator!() const {
//   return !_;
//}
//
//ip6_bf_t ip6_bf_t::operator-() {
//   ip6_bf_t res;
//   return res._ = -_, res;
//}
//
//ip6_bf_t ip6_bf_t::operator+() {
//   ip6_bf_t res;
//   return res._ = +_, res;
//}
//
//ip6_bf_t ip6_bf_t::operator~() {
//   ip6_bf_t res;
//   return res._ = ~_, res;
//
//}
//
//ip6_bf_t& ip6_bf_t::operator++() {
//   return ++_, *this;
//}
//
//ip6_bf_t& ip6_bf_t::operator--() {
//   return --_, *this;
//}
//
//uint64_t const& ip6_bf_t::operator*() const {
//   return reinterpret_cast<uint64_t const&>(*this);
//}
//
//uint64_t& ip6_bf_t::operator*() {
//   return reinterpret_cast<uint64_t&>(*this);
//}
//
//ip6_bf_t& ip6_bf_t::operator|=(ip6_bf_t value) {
//   return _ |= value._, *this;
//}
//
//ip6_bf_t& ip6_bf_t::operator&=(ip6_bf_t value) {
//   return _ &= value._, *this;
//}
//
//ip6_bf_t& ip6_bf_t::operator-=(ip6_bf_t value) {
//   return _ -= value._, *this;
//}
//
//ip6_bf_t& ip6_bf_t::operator+=(ip6_bf_t value) {
//   return _ += value._, *this;
//}
//
//ip6_bf_t::ip6_bf_t(ip6_bf_t const& value) : // @suppress("Not initialized")
//      _(value._) {
//}
//
//ip6_bf_t& ip6_bf_t::operator=(ip6_bf_t value) {
//   return _ = value._, *this;
//}
//
//bool ip6_bf_t::operator<(ip6_bf_t value) const {
//   return _ < value._;
//}
//
//bool ip6_bf_t::operator<=(ip6_bf_t value) const {
//   return _ <= value._;
//}
//
//bool ip6_bf_t::operator>(ip6_bf_t value) const {
//   return _ > value._;
//}
//
//bool ip6_bf_t::operator>=(ip6_bf_t value) const {
//   return _ >= value._;
//}
//
//bool ip6_bf_t::operator==(ip6_bf_t value) const {
//   return _ == value._;
//}
//
//bool ip6_bf_t::operator!=(ip6_bf_t value) const {
//   return _ != value._;
//}

// -----------------------------------------------------------------

std::string byte_str(std::size_t size) {
   std::string result;
   if (auto const peta = size >> 50) {
      result = std::to_string(peta) + "PB";
      size -= peta << 50;
   }

   if (auto const peta = size >> 40) {
      result = std::to_string(peta) + "TB";
      size -= peta << 40;
   }

   if (auto const peta = size >> 30) {
      result = std::to_string(peta) + "GB";
      size -= peta << 30;
   }

   if (auto const peta = size >> 20) {
      result = std::to_string(peta) + "MB";
      size -= peta << 20;
   }

   if (auto const peta = size >> 10) {
      result = std::to_string(peta) + "KB";
      size -= peta << 10;
   }

   if (size)
      result += std::to_string(size) + "B";

   return result;
}

// -----------------------------------------------------------------

void mac_copy(mac_t const src, uint8_t dest[6]) { // @suppress("Missing const-qualification")
   reinterpret_cast<mac_t&>(dest[0]) = src;
}

void ip_copy(ip4_t const src, uint8_t dest[4]) { // @suppress("Missing const-qualification")
   reinterpret_cast<uint32_t&>(dest[0]) = src;
}

void mac_copy(uint8_t const src[6], mac_t& dest) {
   mac_copy(src, reinterpret_cast<uint8_t*>(&dest));
}

void ip_copy(uint8_t const src[4], ip4_t& dest) {
   ip_copy(src, reinterpret_cast<uint8_t*>(&dest));
}

void mac_copy(uint8_t const src[6], uint8_t dest[6]) {
   mac_copy(reinterpret_cast<mac_t const&>(src[0]), dest);
}

void ip_copy(uint8_t const src[4], uint8_t dest[4]) {
   ip_copy(reinterpret_cast<ip4_t const&>(src[0]), dest);
}

// -----------------------------------------------------------------

mac_t mac_converter(uint8_t const mac[6]) {
   mac_t res { };
   return res = reinterpret_cast<mac_t const&>(mac[0]), res;
}

ip4_t ip_converter(uint8_t const ip[4]) {
   return reinterpret_cast<ip4_t const&>(ip[0]);
}

ip4_t ip_converter(std::array<uint8_t ,4> const& ip) {
   return ip_converter(ip.data());
}

// -----------------------------------------------------------------

std::array<uint8_t, 6> mac_converter(std::string const& mac) {
   std::array<uint8_t, 6> res { };

   std::string token;
   std::stringstream ss(mac);
   uint8_t i = 0;
   uint16_t tmp { };
   while (std::getline(ss, token, ':')) {
      if (i > 5)
         throw std::bad_cast();

      std::stringstream ss_2("0x" + token);
      ss_2 >> tmp;
      res[5 - i] = tmp;
      ++i;
   }

   return res;

}

std::array<uint8_t, 4> ip_converter(std::string const& ip) {
   std::array<uint8_t, 4> res { };

   std::string token;
   std::stringstream ss(ip);
   uint8_t i = 0;
   uint16_t tmp { };
   while (std::getline(ss, token, '.')) {
      if (i > 3)
         throw std::bad_cast();

      std::stringstream ss_2(token);
      ss_2 >> tmp;
      res[3 - i] = tmp;
      ++i;
   }

   return res;
}

//std::array<uint8_t, 6> ip6_converter(std::string const& ip6) {
//
//}

// -----------------------------------------------------------------

std::array<uint8_t, 6> mac_converter(mac_t const mac) {
   std::array<uint8_t, 6> res { };
   reinterpret_cast<uint32_t&>(res[0]) = reinterpret_cast<uint32_t const&>(mac);
   reinterpret_cast<uint16_t&>(res[4]) = reinterpret_cast<uint16_t const&>(reinterpret_cast<uint8_t const*>(&mac)[4]);
   return res;
}

std::array<uint8_t, 4> ip_converter(ip4_t const ip) {
   std::array<uint8_t, 4> res { };
   reinterpret_cast<ip4_t&>(*res.data()) = ip;
   return res;
}

std::array<uint8_t, 6> ip6_converter(ip6_t const ip) {
   std::array<uint8_t, 6> res { };
   reinterpret_cast<ip6_t&>(*res.data()) = ip;
   return res;
}

// -----------------------------------------------------------------

std::string mac_converter_s(mac_t const mac) {
   auto mac_ = mac_converter(mac);
   return mac_converter_s(mac_.data());
}

std::string ip6_converter_s(ip6_t ip) {
   return ip6_converter_s(ip);
}

std::string ip_converter_s(ip4_t const ip) {
   auto ip_ = ip_converter(ip);
   return ip_converter_s(ip_.data());
}

// -----------------------------------------------------------------

std::string mac_converter_s(uint8_t const mac[6]) {
   std::stringstream ss { };

   ss << std::hex << std::setfill('0');
   for (unsigned int i = 0; i < 6; ++i)
      ss << std::setw(2) << static_cast<unsigned int>(mac[5 - i]) << (i == 5 ? "" : ":");

   return ss.str();
}

std::string ip_converter_s(uint8_t const ip[4]) {
   return std::to_string(ip[3]) + "." + std::to_string(ip[2]) + "." + std::to_string(ip[1]) + "."
         + std::to_string(ip[0]);//todo tap
}

std::string ip6_converter_s(uint8_t const ip[6]) {
   auto ip_ = ip_converter_s(ip + 2);
   return ip_ + std::to_string(ip[1]) + "." + std::to_string(ip[0]);
}

// -----------------------------------------------------------------

mac_t r_mac_converter(uint8_t const mac[6]) {
   mac_t res { };
   return res = reinterpret_cast<mac_t const&>(mac[0]), res;
}

ip4_t r_ip_converter(uint8_t const ip[4]) {
   return reinterpret_cast<ip4_t const&>(ip[0]);
}

// -----------------------------------------------------------------

std::array<uint8_t, 6> r_mac_converter(std::string const& mac) {
   std::array<uint8_t, 6> res { };

   std::string token;
   std::stringstream ss(mac);
   uint8_t i = 0;
   uint16_t tmp { };
   while (std::getline(ss, token, ':')) {
      if (i > 5)
         throw std::bad_cast();

      std::stringstream ss_2("0x" + token);
      ss_2 >> tmp;
      res[i] = tmp;
      ++i;
   }

   return res;

}

std::array<uint8_t, 4> r_ip_converter(std::string const& ip) {
   std::array<uint8_t, 4> res { };

   std::string token;
   std::stringstream ss(ip);
   uint8_t i = 0;
   uint16_t tmp { };
   while (std::getline(ss, token, '.')) {
      if (i > 3)
         throw std::bad_cast();

      std::stringstream ss_2(token);
      ss_2 >> tmp;
      res[i] = tmp;
      ++i;
   }

   return res;
}

// -----------------------------------------------------------------

std::array<uint8_t, 6> r_mac_converter(mac_t const mac) {
   std::array<uint8_t, 6> res { };
   reinterpret_cast<mac_t&>(*res.data()) = mac;
   return res;
}

std::array<uint8_t, 4> r_ip_converter(ip4_t const ip) {
   std::array<uint8_t, 4> res { };
   reinterpret_cast<ip4_t&>(*res.data()) = ip;
   return res;
}

std::array<uint8_t, 6> r_ip6_converter(ip6_t const ip) {
   std::array<uint8_t, 6> res { };
   reinterpret_cast<ip6_t&>(*res.data()) = ip;
   return res;
}

// -----------------------------------------------------------------

std::string r_mac_converter_s(mac_t const mac) {
   auto mac_ = mac_converter(mac);
   return r_mac_converter_s(mac_.data());
}

std::string r_ip6_converter_s(ip6_t ip) {
   return r_ip6_converter_s(ip);
}

std::string r_ip_converter_s(ip4_t const ip) {
   auto ip_ = ip_converter(ip);
   return r_ip_converter_s(ip_.data());
}

// -----------------------------------------------------------------

std::string r_mac_converter_s(uint8_t const mac[6]) {
   std::stringstream ss { };

   ss << std::hex << std::setfill('0');
   for (unsigned int i = 0; i < 6; ++i)
      ss << std::setw(2) << static_cast<unsigned int>(mac[i]) << (i == 5 ? "" : ":");

   return ss.str();
}

std::string r_ip_converter_s(uint8_t const ip[4]) {
   return std::to_string(ip[0]) + "." + std::to_string(ip[1]) + "." + std::to_string(ip[2]) + "."
         + std::to_string(ip[3]);//todo tap
}

std::string r_ip6_converter_s(uint8_t const ip[6]) {
   auto ip_ = r_ip_converter_s(ip);
   return ip_ + std::to_string(ip[4]) + "." + std::to_string(ip[5]);
}

// -----------------------------------------------------------------

std::string mac_converter_s(const std::array<uint8_t, 6>& mac) {
   return mac_converter_s(mac.data());
}

std::string ip_converter_s(const std::array<uint8_t, 4>& ip) {
   return ip_converter_s(ip.data());
}

//// -----------------------------------------------------------------
//
//void sleepy::wakeup() const {
// std::lock_guard<std::mutex> const lock { m_mutex };
// m_cond.notify_one();
//}
//
//// -----------------------------------------------------------------
//
//SleepyThread::~SleepyThread() {
// if (joinable()) {
//    wakeup();
//    join();
// }
//}

// -----------------------------------------------------------------

void shared_recursive_mutex_t::lock() {
#ifdef DEBUG
   {
      std::lock_guard<std::mutex> const lock { shared_count_mutex };
      assert(shared_counts.find(std::this_thread::get_id()) == shared_counts.end());
   }
#endif

   if (std::this_thread::get_id() != owner)
      std::shared_mutex::lock();

   owner = std::this_thread::get_id();
   ++count;
}

void shared_recursive_mutex_t::unlock() {
   assert(std::this_thread::get_id() == owner);
   assert(count);
   assert(shared_counts.find(std::this_thread::get_id()) == shared_counts.end());

   --count;

   if (!count) {
      owner = std::thread::id();
      std::shared_mutex::unlock();
   }
}

void shared_recursive_mutex_t::lock_shared() {
   if (std::this_thread::get_id() == owner)
      ++count;
   else {
      std::lock_guard<std::mutex> const lock { shared_count_mutex };
      auto& count = shared_counts[std::this_thread::get_id()];

      if (!count)
         std::shared_mutex::lock_shared();

      ++count;
   }
}

void shared_recursive_mutex_t::unlock_shared() {
   if (std::this_thread::get_id() == owner) {
      assert(count);
      --count;
      assert(count);
   } else {
      std::lock_guard<std::mutex> const lock { shared_count_mutex };
      auto& count = shared_counts[std::this_thread::get_id()];
      assert(count);

      --count;

      if (!count) {
         shared_counts.erase(std::this_thread::get_id());
         std::shared_mutex::unlock_shared();
      }
   }
}

// -----------------------------------------------------------------

bool post_instantiation::post_validate() {
   return true;
}

// -----------------------------------------------------------------

void observer_base_t::observe(std::weak_ptr<observable_t> const& observable_) {
   auto observable = observable_.lock();
   assert(observable);

   std::lock_guard<std::shared_mutex> const lock { observable->m_mutex };

   for (auto const& observer : observable->m_observers) {
      assert(observer);
      if (observer.get() == this)
         return;
   }

   m_observables.push_back(observable_);
   observable->m_observers.emplace_back(this->lock());
}

bool observer_base_t::post_validate() {
   decltype(m_observables) observables {};
   observables.swap(m_observables);

   for (const auto& observable : observables)
      observe(observable);
   return true;
}

void observer_base_t::unobserve(observable_t const* observable_) {
   for (auto observable_it = m_observables.begin(); observable_it != m_observables.end();) {
      auto observable = observable_it->lock();

      if (!observable) {
         observable_it = m_observables.erase(observable_it);
         continue;
      }

      if (!observable_ || observable.get() == observable_) {
         std::lock_guard<std::shared_mutex> const lock { observable->m_mutex };

         for (auto it = observable->m_observers.begin(); it != observable->m_observers.end(); ++it) {
            assert(*it);
            if (it->get() == this) {
               observable->m_observers.erase(it);

               if (observable_) {
                  m_observables.erase(observable_it);
                  return;
               }

               break;
            }
         }
      }

      ++observable_it;
   }

   if (!observable_)
      m_observables.clear();
}

observer_base_t::~observer_base_t() {
   unobserve();
}

// -----------------------------------------------------------------

void weak_observer_base_t::observe(std::weak_ptr<observable_t> const& observable) {
   m_proxy->observe(observable);
}

void weak_observer_base_t::unobserve(observable_t const* observable) {
   m_proxy->unobserve(observable);
}

weak_observer_base_t::~weak_observer_base_t() {
   m_proxy->unobserve(nullptr);
}

// -----------------------------------------------------------------

observable_t::~observable_t() {
   for (const auto& observer : m_observers) {
      assert(observer);
      observer->unobserve(this);
   }
}


