#include "signalhandler.h"

#include <logger/logger.h>

#include <csignal>
#include <functional>
#include <ostream>
#include <stack>
#include <stdexcept>
#include <string.h>
#include <strings.h>
#include <tuple>
#include <unistd.h>

void SignalHandler::global_t::insert(SigNum const signum, handler_t const& handler) {
    handlers.insert({signum, handler});
}

void SignalHandler::local_t::insert(SigNum const signum, handler_t const& handler) {
    handlers.insert({signum, handler});
}

SignalHandler::SignalHandler() {
    struct sigaction sigaction_ {};
    bzero(&sigaction_, sizeof(sigaction_));
    sigaction_.sa_sigaction = &signalHandler;
    sigaction_.sa_flags = SA_ONSTACK | SA_SIGINFO;

    sigaction(SIGSEGV, &sigaction_, 0);
    sigaction(SIGTERM, &sigaction_, 0);
    sigaction(SIGSTOP, &sigaction_, 0);
    sigaction(SIGINT, &sigaction_, 0);
    sigaction(SIGPIPE, &sigaction_, 0);
}

SignalHandler::~SignalHandler() {
   stack_t segv_stack {};
   segv_stack.ss_sp = nullptr;
   segv_stack.ss_flags = SS_DISABLE;
   segv_stack.ss_size = 0;
   if (int const r = sigaltstack(&segv_stack, nullptr); r < 0) {
      llog::error << "error uninstalling altstack: " << strerror(r) << std::endl;
      std::terminate();
   }
   sigaction(SIGSEGV, nullptr, 0);
   sigaction(SIGTERM, nullptr, 0);
   sigaction(SIGSTOP, nullptr, 0);
   sigaction(SIGINT, nullptr, 0);
   sigaction(SIGPIPE, nullptr, 0);
}

void SignalHandler::setAltStack() {
   if (stack.size())
      throw std::runtime_error("altstack allready set...");

   stack_t segv_stack {};
   stack.resize(MINSIGSTKSZ + 8 * 4096);
   segv_stack.ss_sp = stack.data();
   segv_stack.ss_flags = 0;
   segv_stack.ss_size = stack.size();
   if (int const r = sigaltstack(&segv_stack, nullptr); r < 0) {
      llog::error << "error installing altstack: " << strerror(r) << std::endl;
      std::terminate();
   }
}

SignalHandler& SignalHandler::get() {
   return SignalHandler::instance;
}

void SignalHandler::handle () {
    int call_nb = this->call_nb;
    std::stack<std::function<bool()>> interuption_stack {};
    int next_processed { 0 };

    do {
        call_nb = 0 + this->call_nb;

        for (int index = next_processed; index < call_nb; ++index)
            interuption_stack.push([&, index]() {
                auto& sig_info = sig_infos.at(index);
                if (!std::get<0>(sig_info))
                    return false;

                bool handled = false;

                for (auto handlers = global_t::handlers.equal_range(std::get<1>(sig_info).si_signo); handlers.first != handlers.second; ++handlers.first)
                    (handlers.first->second)(std::get<1>(sig_info), *std::get<2>(sig_info)), handled = true;

                for (auto handlers = instance.local.handlers.equal_range(std::get<1>(sig_info).si_signo); handlers.first != handlers.second; ++handlers.first)
                    (handlers.first->second)(std::get<1>(sig_info), *std::get<2>(sig_info)), handled = true;

                if (std::get<1>(sig_info).si_signo == SIGSEGV && !handled) {
                   llog::error << "Segfault !" << std::endl;
                   std::abort();
                }

                return std::get<0>(sig_info) = false, true;
            });

        next_processed = call_nb;

        if (interuption_stack.size()) {
            if (interuption_stack.top()())
                interuption_stack.pop();
            else
                usleep(10);
            continue;
        }
    } while(!this->call_nb.compare_exchange_strong(call_nb, 0));
}

void SignalHandler::signalHandler(int, siginfo_t* siginfo, void* ucontext) { // @suppress("Missing const-qualification")
   int const call_nb = instance.call_nb++;
    instance.sig_infos.at(call_nb) = { false, *siginfo, reinterpret_cast<ucontext_t*>(ucontext) };
    std::get<0>(instance.sig_infos.at(call_nb)) = true;

    if (!call_nb)
        instance.handle();
}

thread_local SignalHandler SignalHandler::instance {};
std::unordered_multimap<int, SignalHandler::handler_t> SignalHandler::global_t::handlers {};
