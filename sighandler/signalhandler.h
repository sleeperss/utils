#pragma once

#include <ucontext.h>
#include <array>
#include <atomic>
#include <functional>
#include <tuple>
#include <vector>
#include <signal.h>

class SignalHandler {
private:
   static thread_local SignalHandler instance;

public:
   using handler_t = std::function<void(siginfo_t const&, ucontext_t&)>;
   std::vector<uint8_t> stack {};
   std::array<std::tuple<bool, siginfo_t, ucontext_t*>, 10> sig_infos {};
   std::atomic<int> call_nb { 0 };

   enum SigNum {
      SigSegv = SIGSEGV,
      SigKill = SIGKILL,
      SigTerm = SIGTERM,
      SigInt = SIGINT,
      SigPipe = SIGPIPE
   };

   struct global_t {
      static void insert(SigNum const signum, handler_t const& handler);

   protected:
      static std::unordered_multimap<int, handler_t> handlers;

      friend class SignalHandler;
   };

   struct local_t {
      void insert(SigNum const signum, handler_t const& handler);

   protected:
      std::unordered_multimap<int, handler_t> handlers;

      friend class SignalHandler;
   } local;

   void setAltStack();
   static SignalHandler& get();

protected:
   SignalHandler();

   ~SignalHandler();

   void handle ();
   static inline void signalHandler(int signum, siginfo_t* siginfo, void* ucontext);
};
