/*
 * ContainerPtr.h
 *
 *  Created on: Oct 13, 2019
 *      Author: sleeper
 */

#pragma once

#include "ObjectPointer.h"
#include "json.h"
#include "exception/InvalidJsonAccess.h"

#include <memory>

#include <string>
#include <type_traits>
#include <typeinfo>

#include <utility>
#include <vector>

namespace json {

template<class Type_>
Type_ ObjectPointer::value() const {
   if (auto ptr = std::dynamic_pointer_cast<json::String>(m_ptr))
      return ptr->template getValue<Type_>();
   if (auto ptr = std::dynamic_pointer_cast<json::Float>(m_ptr))
      return ptr->template getValue<Type_>();
   if (auto ptr = std::dynamic_pointer_cast<json::Integer>(m_ptr))
      return ptr->template getValue<Type_>();
      
   throw InvalidJsonAccess();
}

template<class Type_>
Type_& ObjectPointer::cast() const {
   if (auto ptr = std::dynamic_pointer_cast<Type_>(m_ptr); ptr)
      return *ptr;
   llog::error << "invalid cast ..." << std::endl;
   throw std::bad_cast();
}

template<class Type_>
bool ObjectPointer::is() const {
   if (std::dynamic_pointer_cast<Type_>(m_ptr))
      return true;
   return false;
}

template<class Type_>
struct equal_operator_helper;

template<std::floating_point Type_>
struct equal_operator_helper<Type_> {
   using type_t = json::Float;

   template<class Type__>
   static constexpr long double convert(Type__&& type) {
      return std::forward<Type__>(type);
   }
};

template<std::integral Type_>
struct equal_operator_helper<Type_> {
   using type_t = json::Integer;

   template<class Type__>
   static constexpr int64_t convert(Type__&& type) {
      return std::forward<Type__>(type);
   }
};

template<std::convertible_to<std::string> Type_>
struct equal_operator_helper<Type_> {
   using type_t = json::String;

   template<class Type__>
   static constexpr Type__&& convert(Type__&& type) {
      return std::forward<Type__>(type);
   }
};

template<std::convertible_to<std::vector<std::byte>> Type_>
struct equal_operator_helper<Type_> {
   using type_t = json::Data;

   template<class Type__>
   static constexpr Type__&& convert(Type__&& type) {
      return std::forward<Type__>(type);
   }
};

template<class Type_>
ObjectPointer& ObjectPointer::operator=(Type_&& type) {
   using helper_t = equal_operator_helper<std::remove_const_t<std::remove_reference_t<Type_>>>;

   struct make_shared_enabler : public helper_t::type_t {
      constexpr make_shared_enabler(Type_&& type) :
          helper_t::type_t(helper_t::convert(std::forward<Type_>(type))) {}
   };
   this->m_ptr = std::make_shared<make_shared_enabler>(std::forward<Type_>(type));
   return *this;
}

template<class Type_>
ObjectPointer& ObjectPointer::operator+=(Type_&& type) {
   using helper_t = equal_operator_helper<std::remove_const_t<std::remove_reference_t<Type_>>>;

   struct make_shared_enabler : public helper_t::type_t {
      constexpr make_shared_enabler(Type_&& type) :
          helper_t::type_t(helper_t::convert(std::forward<Type_>(type))) {}
   };
   (*this->m_ptr) += json::ObjectPointer { std::make_shared<make_shared_enabler>(std::forward<Type_>(type)) };
   return *this;
}

} /* namespace json */

