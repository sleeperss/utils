/*
 * Object.h
 *
 *  Created on: Oct 12, 2019
 *      Author: sleeper
 */

#pragma once

#include <cstddef>
#include <string>

namespace json {

class ObjectPointer;

struct Formatter {
   std::string prefix;
   bool formatte;

   Formatter clone() const { return *this;}
};

class Object {
public:
   Object();
   virtual ~Object();

   virtual ObjectPointer& operator[](std::string const&);
   virtual ObjectPointer& operator[](std::size_t);
   virtual ObjectPointer& at(std::size_t);
   virtual ObjectPointer const& at(std::size_t) const;
   virtual ObjectPointer& insert();
   virtual ObjectPointer& operator+=(ObjectPointer&&);
   virtual ObjectPointer& back();

   std::string toString(bool formatte = true) const {
      Formatter formatter;
      formatter.formatte = formatte;
      return toString(formatter);
   }
   virtual std::string toString(Formatter&) const = 0;

   friend class ObjectPointer;
   friend class UndefinedObject;
};

} /* namespace json */
