/*
 * Object.cpp
 *
 *  Created on: Oct 12, 2019
 *      Author: sleeper
 */

#include "Object.h"

#include "exception/InvalidJsonAccess.h"
#include "ObjectPointer.h"

#include <cstddef>

#include <string>

namespace json {

Object::Object() {
}

Object::~Object() {
}
//todo segfault
ObjectPointer& Object::operator [](std::string const&) {
   throw InvalidJsonAccess();
}

ObjectPointer& Object::operator [](std::size_t) {
   throw InvalidJsonAccess();
}

ObjectPointer& Object::at(std::size_t) {
   throw InvalidJsonAccess();
}

ObjectPointer const& Object::at(std::size_t) const {
   throw InvalidJsonAccess();
}

ObjectPointer& Object::insert() {
   throw InvalidJsonAccess();
}

ObjectPointer& Object::operator+=(ObjectPointer&&) {
   throw InvalidJsonAccess();
}

ObjectPointer& Object::back() {
   throw InvalidJsonAccess();
}

} /* namespace json */
