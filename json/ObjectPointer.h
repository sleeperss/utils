/*
 * ContainerPtr.h
 *
 *  Created on: Oct 13, 2019
 *      Author: sleeper
 */

#pragma once

#include "Object.h"

#include <logger/logger.h>

#include <cstddef>
#include <memory>
#include <string>
#include <typeinfo>
#include <utility>
#include <vector>

namespace json {

class ObjectPointer {
protected:
   std::shared_ptr<Object> m_ptr;

   ObjectPointer(std::shared_ptr<Object> const&);

public:
   ObjectPointer(ObjectPointer const&) = default;
   ObjectPointer(ObjectPointer&&) = default;
   virtual ~ObjectPointer();

   template<class Type, typename... Args>
   static ObjectPointer create(Args&&... args) {
      struct make_shared_enabler : public Type {
         make_shared_enabler(Args&&... args) : Type(std::forward<Args>(args)...) {}
      };
      return ObjectPointer { std::make_shared<make_shared_enabler>(std::forward<Args>(args)...) };
   }

   ObjectPointer& operator[](const char*) const;
   ObjectPointer& operator[](std::string const&) const;
   ObjectPointer& operator[](std::size_t) const;

   std::string toString(bool formatte = true) const;
   std::string toString(Formatter&) const;

   ObjectPointer& insert();
   ObjectPointer& back() const;

   template<class Type>
   Type& cast() const;

   template<class Type>
   bool is() const;

   std::string value() const;
   std::vector<uint8_t> const& data() const;

   template<class Type_>
   Type_ value() const;

   ObjectPointer& replace(ObjectPointer const&);
   ObjectPointer& replace(ObjectPointer&);
   ObjectPointer& replace(ObjectPointer&&);

   ObjectPointer& operator=(ObjectPointer const&);
   ObjectPointer& operator=(ObjectPointer&);
   ObjectPointer& operator=(ObjectPointer&&);
   ObjectPointer& operator=(std::nullptr_t&&);
   template<class Type_>
   ObjectPointer& operator=(Type_&&);
   template<class Type_>
   ObjectPointer& operator+=(Type_&&);

   operator bool() const;
};

} /* namespace json */
