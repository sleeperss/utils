#pragma once

#include "Object.h"

#include <cstddef>
#include <string>

/*####################*/namespace json        {/*###################*/

class UndefinedObject : public Object {
public:
   UndefinedObject();
   virtual ~UndefinedObject();

   static ObjectPointer create();

   virtual std::string toString(Formatter&) const final override;
};

/*###################*/}// namespace json      /*###################*/
