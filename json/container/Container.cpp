/*
 * Container.cpp
 *
 *  Created on: Oct 13, 2019
 *      Author: sleeper
 */

#include "Container.h"

#include <cstddef>
#include <stdexcept>
#include <string>

#include "../exception/InvalidJsonAccess.h"
#include "../ObjectPointer.h"
#include "../value/Value.h"
#include "../UndefinedObject.h"

#include <utility>

namespace json {

Container::Container() {
}

Container::~Container() {
}

ObjectPointer& Container::operator [](std::string const& key) {
   try {
      return m_properties.at(key);
   } catch (std::out_of_range const&) {
   }
   return insert(key, json::ObjectPointer::create<json::UndefinedObject>()).second;
}

std::pair<bool, ObjectPointer&> Container::insert(std::string const& key, ObjectPointer const& object) {
   if (auto it = m_properties.find(key); it != m_properties.end())
      return { false, it->second };

   return { true, m_properties.emplace(key, object).first->second };
}

std::pair<bool, ObjectPointer&> Container::insert(std::string const& key, std::string const& value) {
   return insert(key, json::ObjectPointer::create<String>(value));
}

std::pair<bool, ObjectPointer&> Container::replace(std::string const& key, ObjectPointer const& object) {
   auto const res = m_properties.find(key);

   if (res != m_properties.end())
      m_properties.erase(key);

   return { res != m_properties.end(), m_properties.emplace(key, object).first->second };
}

std::pair<bool, ObjectPointer&> Container::replace(std::string const& key, std::string const& value) {
   return replace(key, json::ObjectPointer::create<String>(value));
}

decltype(Container::m_properties)& Container::properties() {
   return m_properties;
}

std::size_t Container::nbProperties() const {
   return m_properties.size();
}

ObjectPointer Container::create() {
   return ObjectPointer::create<json::Container>();
}

std::string Container::toString(Formatter& formatter) const {
   std::string result;
   for (auto const& property : m_properties) {
      auto subFormatter = formatter.clone();
      if (formatter.formatte)
         subFormatter.prefix = "\t" + subFormatter.prefix;
      result += std::string(result.size() ? ", " : "")
            + (formatter.formatte ? "\n" + subFormatter.prefix + "\"" + "\033[32;1m" : "\"") + property.first
            + (formatter.formatte ? "\033[0m" : "") + "\": "
            + property.second.toString(subFormatter);
   }

   return (formatter.formatte ? "\033[31;1m{\033[0m" : "{") + result
         + (formatter.formatte ? "\n" + formatter.prefix + "\033[31;1m}\033[0m" : "}");
}

}
