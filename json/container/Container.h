/*
 * Container.h
 *
 *  Created on: Oct 12, 2019
 *      Author: sleeper
 */

#pragma once

#include "../ObjectPointer.h"

#include <cstddef>
#include <string>
#include <unordered_map>
#include <utility>

namespace json {

namespace container {
class ValueParser;
}

class Container: public Object {
private:
   std::unordered_map<std::string, ObjectPointer> m_properties;

   Container();

public:
   virtual ~Container();

   virtual ObjectPointer& operator[](std::string const&) final override;

   std::pair<bool, ObjectPointer&> insert(std::string const&, ObjectPointer const&);
   std::pair<bool, ObjectPointer&> insert(std::string const&, std::string const&);
   std::pair<bool, ObjectPointer&> replace(std::string const&, ObjectPointer const&);
   std::pair<bool, ObjectPointer&> replace(std::string const&, std::string const&);

   decltype(m_properties)& properties();
   std::size_t nbProperties() const;

   static ObjectPointer create();

   std::string toString(Formatter&) const final override;

   friend class container::ValueParser;
   friend class Parser;
   friend class ObjectPointer;
};

} /* namespace json */
