#include "ContainerParser.h"
#include "Container.h"

#include "../ObjectPointer.inl"
#include "../Parser.inl"

#include "../exception/ParserError.h"
#include "../exception/InvalidJson.h"

#include <memory>
#include <utility>
#include <cstddef>
#include <string>

namespace json {
namespace container {

std::pair<std::size_t, ObjectPointer> BeginingParser::parseJson() {
   auto parser = this_ptr.lock();
   for (std::size_t i = 0; i < m_json.size();) {
         parser = parser->parseChar(i);

         if (!parser)
            return { 0, UndefinedObject::create() };

         if (parser->end())
            return { i, parser->object() };
   }

   return { 0, UndefinedObject::create() };
}

ObjectPointer& BeginingParser::object() {
   return m_object;
}

std::shared_ptr<Parser> BeginingParser::parseChar(std::size_t& index) {

   for (; index < m_json.size(); ++index) {
      auto const char_ = m_json.at(index);

      if (char_ == ' ' || char_ == '\t' || char_ == '\n' || char_ == '\r')
         continue;

      if (char_ == '{') {
         ++index;
         return Parser::create<KeyParser>(m_json, *this);
      }

      return nullptr;
   }

   return nullptr;
}

std::shared_ptr<Parser> BeginingParser::lock() const {
   return this_ptr.lock();
}

std::shared_ptr<Parser> KeyParser::parseChar(std::size_t& index) {

   for (; index < m_json.size(); ++index) {
      auto const char_ = m_json.at(index);

      if (char_ == ' ' || char_ == '\t' || char_ == '\n' || char_ == '\r')
         continue;

      if (char_ == '"') {
         ++index;
         return Parser::create<KeyParser2>(m_json, index, m_parent);
      }

      if (char_ == '}') {
         ++index;
         m_parent.m_end = true;
         return m_parent.lock();
      }

      throw ParserError("container: missing } or \"" + std::string(" (index: ") + std::to_string(index) + ")");
   }

   throw ParserError("container: missing } or \"" + std::string(" (index: ") + std::to_string(index) + ")");
}

std::shared_ptr<Parser> KeyParser2::parseChar(std::size_t& index) {
   for (; index < m_json.size(); ++index) {
      auto const char_ = m_json.at(index);

      if (char_ == '"') {
         ++index;
         return Parser::create<ValueParser>(m_json, m_json.substr(m_begging, index - 1 - m_begging), m_parent);
      }
   }

   throw ParserError("container: missing \"" + std::string(" (index: ") + std::to_string(index) + ")");
}

std::shared_ptr<Parser> ValueParser::parseChar(std::size_t& index) {

   for (; index < m_json.size(); ++index) {
      auto const char_ = m_json.at(index);

      if (char_ == ' ' || char_ == '\t' || char_ == '\n' || char_ == '\r')
         continue;

      if (char_ == ':') {
         ++index;
         auto const result = Parser::parseImpl(m_json.substr(index));

         if (!m_parent.m_object.cast<Container>().insert(m_key, result.second).first)
            throw ParserError("container: duplicate property " + m_key + " (index: " + std::to_string(index) + ")");

         index += result.first;
         return Parser::create<NextParser>(m_json, m_parent);
      }

      throw ParserError("container: missing \":\" (index: " + std::to_string(index) + ")");
   }

   throw ParserError("container: missing \":\" (index: " + std::to_string(index) + ")");
}

std::shared_ptr<Parser> NextParser::parseChar(std::size_t& index) {

   for (; index < m_json.size(); ++index) {
      auto const char_ = m_json.at(index);

      if (char_ == ' ' || char_ == '\t' || char_ == '\n' || char_ == '\r')
         continue;

      if (char_ == ',') {
         ++index;
         return Parser::create<KeyParser>(m_json, m_parent);
      } else if (char_ == '}') {
         ++index;
         m_parent.m_end = true;
         return m_parent.lock();
      }

      throw ParserError("container: missing , or }");
   }

   throw ParserError("container: missing , or }");
}

}
}
