#pragma once

#include "../Parser.h"
#include "Container.h"

#include <cstddef>
#include <memory>
#include <string>
#include <utility>

namespace json {

namespace container {

class BeginingParser: public Parser {
protected:
   ObjectPointer m_object { ObjectPointer::create<Container>() };

   BeginingParser(std::string const& json) :
         Parser(json) {
   }

   std::pair<std::size_t, ObjectPointer> parseJson() final override;
   virtual ObjectPointer& object() final override;

public:
   virtual ~BeginingParser() = default;
   std::shared_ptr<Parser> parseChar(std::size_t&) final override;
   std::shared_ptr<Parser> lock() const;

   friend class ValueParser;
   friend class KeyParser;
   friend class NextParser;
};

class KeyParser: public Parser {
protected:
   BeginingParser& m_parent;

   KeyParser(std::string const& json, BeginingParser& parent) :
         Parser(json), m_parent(parent) {
   }

public:
   virtual ~KeyParser() = default;
   std::shared_ptr<Parser> parseChar(std::size_t&) final override;

   friend class Parser;
};

class KeyParser2: public Parser {
protected:
   BeginingParser& m_parent;
   std::size_t m_begging;

   KeyParser2(std::string const& json, std::size_t begging, BeginingParser& parent) :
         Parser(json), m_parent(parent), m_begging(begging) {
   }

public:
   virtual ~KeyParser2() = default;
   std::shared_ptr<Parser> parseChar(std::size_t&) final override;

   friend class Parser;
};

class ValueParser: public Parser {
protected:
   BeginingParser& m_parent;
   const std::string m_key;

   ValueParser(std::string const& json, std::string const& key, BeginingParser& parent) :
         Parser(json), m_parent(parent), m_key(key) {
   }

public:
   virtual ~ValueParser() = default;
   std::shared_ptr<Parser> parseChar(std::size_t&) final override;

   friend class Parser;
};

class NextParser: public Parser {
protected:
   BeginingParser& m_parent;
   const std::string m_key;

   NextParser(std::string const& json, BeginingParser& parent) :
         Parser(json), m_parent(parent) {
   }

public:
   virtual ~NextParser() = default;
   std::shared_ptr<Parser> parseChar(std::size_t&) final override;

   friend class Parser;
};

}
}
