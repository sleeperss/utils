/*
 * Container.h
 *
 *  Created on: Oct 12, 2019
 *      Author: sleeper
 */

#pragma once

#include "../../json/Object.h"

#include <cassert>
#include <string>
#include <vector>

namespace json {

class Data: public Object {
protected:
   std::vector<uint8_t> m_data;

   Data(std::vector<uint8_t> const& data) :
      m_data { data } {
   }

public:

   virtual ~Data() {
   }

   static ObjectPointer create(std::vector<uint8_t> const& data);

   std::string toString(Formatter&) const final override;

   std::vector<uint8_t> const& getData() const;

   friend class ObjectPointer;
};
} /* namespace json */
