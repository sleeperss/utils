/*
 * Container.cpp
 *
 *  Created on: Oct 13, 2019
 *      Author: sleeper
 */

#include "Data.h"
#include "../ObjectPointer.h"

#include <algorithm>

#include <string>

#include <vector>

namespace json {

ObjectPointer Data::create(std::vector<uint8_t> const& data) {
   return ObjectPointer::create<Data>(data);
}

std::string Data::toString(Formatter& formatter) const {
   std::string result = "#" + std::to_string(m_data.size());
   if (formatter.formatte)
      return result;

   result += "#";
   auto const header_size = result.size();
   result.resize(header_size + m_data.size());

   std::copy(m_data.begin(), m_data.end(), result.data() + header_size);
   return result;
}

std::vector<uint8_t> const& Data::getData() const {
   return m_data;
}

}
