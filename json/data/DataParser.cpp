#include "DataParser.h"
#include "../Parser.inl"

#include "../exception/ParserError.h"

#include <algorithm>
#include <cstddef>
#include <memory>
#include <sstream>
#include <utility>
#include <vector>

namespace json {
namespace data {

std::pair<std::size_t, ObjectPointer> BeginingParser::parseJson() {
   auto parser = this_ptr.lock();
   for (std::size_t i = 0; i < m_json.size();) {
         parser = parser->parseChar(i);

         if (!parser)
            return { 0, UndefinedObject::create() };

         if (parser->end())
            return { i, parser->object() };
   }

   return { 0, UndefinedObject::create() };
}

std::shared_ptr<Parser> BeginingParser::parseChar(std::size_t& index) {

   for (; index < m_json.size(); ++index) {
      auto const char_ = m_json.at(index);

      if (char_ == ' ' || char_ == '\t' || char_ == '\n' || char_ == '\r')
         continue;

      if (char_ == '#') {
         ++index;
         return Parser::create<SizeParser>(m_json, index);
      }

      return nullptr;
   }

   return nullptr;
}

std::shared_ptr<Parser> SizeParser::parseChar(std::size_t& index) {

   for (; index < m_json.size(); ++index) {
      auto const char_ = m_json.at(index);

      if (char_ >= '0' && char_ <= '9')
         continue;

      if (char_ == '#') {
         std::stringstream ss { m_json.substr(m_beggining, index - m_beggining) };
         std::size_t size;
         ss >> size;
         ++index;

         if (m_json.size() < index + size)
            throw ParserError("data: json size smaller than data size");

         std::vector<uint8_t> data {};
         data.resize(size);
         std::copy(m_json.data() + index, m_json.data() + index + size, data.data());

         m_object.replace(json::Data::create(data));
         index += size;

         m_end = true;
         return this_ptr.lock();
      }

      throw ParserError("data: missing end of data size");
   }

   throw ParserError("data: missing end of data size");
}

}
}
