#pragma once

#include "../Parser.h"
#include "../Object.h"
#include "Data.h"

#include <string>
#include <assert.h>
#include <cstddef>
#include <memory>
#include <utility>

namespace json {

namespace data {

struct DataParser : public Parser {
   ObjectPointer m_object;

   DataParser(std::string const& json, ObjectPointer&& object) :
      Parser(json), m_object(std::move(object)) {
   }

   virtual ObjectPointer& object() final override {
      return m_object;
   }
};

class BeginingParser : public Parser {
protected:
   BeginingParser(std::string const& json) :
         Parser(json) {
   }

   std::pair<std::size_t, ObjectPointer> parseJson() final override;

public:
   virtual ~BeginingParser() = default;

   std::shared_ptr<Parser> parseChar(std::size_t&) final override;

   friend class Parser;
};

class SizeParser : public DataParser {
protected:
   std::size_t m_beggining;

   SizeParser(std::string const& json, std::size_t beggining) :
         DataParser(json, ObjectPointer::create<UndefinedObject>()),
         m_beggining(beggining) {
   }

public:
   virtual ~SizeParser() = default;

   std::shared_ptr<Parser> parseChar(std::size_t&) final override;

   friend class Parser;
};

}
}
