/*
 * Container.cpp
 *
 *  Created on: Oct 12, 2019
 *      Author: sleeper
 */

#include "Parser.h"
#include "Parser.inl"

#include "../json/array/ArrayParser.h"
#include "../json/container/ContainerParser.h"
#include "../json/exception/InvalidJson.h"
#include "../json/exception/ParserError.h"
#include "../json/value/ValueParser.h"
#include "../json/data/DataParser.h"

#include <cstddef>
#include <memory>
#include <string>
#include <utility>
#include <vector>

namespace json {

namespace container {
class BeginingParser;
}

ObjectPointer Parser::parse(std::string const& json) {
   auto result = parseImpl(json);
   if (result.first != json.size())
      throw std::runtime_error("parsing error...");
   return result.second;
}

bool Parser::end() const {
   return m_end;
}

std::pair<std::size_t, ObjectPointer> Parser::parseImpl(std::string const& json) {
   std::vector<std::shared_ptr<Parser>> parsers {
      Parser::create<container::BeginingParser>(json),
      Parser::create<array::BeginingParser>(json),
      Parser::create<value::BeginingParser>(json),
      Parser::create<data::BeginingParser>(json)
   };

   for (auto& parser : parsers)
      if (auto result = parser->parseJson(); result.second)
         return result;

   throw InvalidJson();
}

} /* namespace json */

std::ostream& operator<<(std::ostream& os, json::ObjectPointer const& json) {
   return (os << json.toString());
}
