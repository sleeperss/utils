/*
 * Container.h
 *
 *  Created on: Oct 12, 2019
 *      Author: sleeper
 */

#pragma once

#include "../../json/Object.h"

#include <cassert>
#include <sstream>
#include <string>

namespace json {

namespace value {
class BeginingParser;
class NumberParser;
class StringParser;
}

class Float: public Object {
protected:
   long double m_double {};

   Float(long double value) :
      m_double { value } {
   }

public:

   virtual ~Float() {
   }

   static ObjectPointer create(long double value);

   std::string toString(Formatter&) const final override;

   template<class Type_>
   Type_ getValue() const {
      std::stringstream ss;
      ss << m_double;

      Type_ res;
      ss >> res;
      return res;
   }

   std::string getValue() const;

   friend class value::BeginingParser;
   friend class value::NumberParser;
   friend class value::StringParser;
   friend class ObjectPointer;
};

class Integer: public Object {
protected:
   int64_t m_int {};

   Integer(int64_t value) :
      m_int { value } {
   }

public:

   virtual ~Integer() {
   }

   static ObjectPointer create(int64_t value);

   std::string toString(Formatter&) const final override;

   template<class Type_>
   Type_ getValue() const {
      std::stringstream ss;
      ss << m_int;

      Type_ res;
      ss >> res;
      return res;
   }

   std::string getValue() const;

   friend class value::BeginingParser;
   friend class value::NumberParser;
   friend class value::StringParser;
   friend class ObjectPointer;
};

class String: public Object {
protected:
   std::string m_value {};

   String(std::string const& value) :
      m_value { value } {
   }

public:

   virtual ~String() {
   }

   static ObjectPointer create(std::string const& value);

   std::string toString(Formatter&) const final override;

   template<class Type_>
   Type_ getValue() const {
      std::stringstream ss;
      ss << m_value;

      Type_ res;
      ss >> res;
      return res;
   }

   std::string getValue() const;

   friend class value::BeginingParser;
   friend class value::NumberParser;
   friend class value::StringParser;
   friend class ObjectPointer;
};
} /* namespace json */
