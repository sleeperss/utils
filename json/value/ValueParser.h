#pragma once

#include "../Parser.h"
#include "../Object.h"
#include "Value.h"

#include <string>
#include <assert.h>
#include <cstddef>
#include <memory>
#include <utility>

namespace json {

namespace value {

struct ValueParser : public Parser {
   ObjectPointer m_object;

   ValueParser(std::string const& json, ObjectPointer&& object) :
      Parser(json), m_object(std::move(object)) {
   }

   virtual ObjectPointer& object() final override {
      return m_object;
   }
};

class BeginingParser : public Parser {
protected:
   BeginingParser(std::string const& json) :
         Parser(json) {
   }

   std::pair<std::size_t, ObjectPointer> parseJson() final override;

public:
   virtual ~BeginingParser() = default;

   std::shared_ptr<Parser> parseChar(std::size_t&) final override;

   friend class Parser;
};

class NumberParser : public ValueParser {
protected:
   std::size_t m_beggining;
   bool m_float { false };
   bool m_begin { true };

   NumberParser(std::string const& json, std::size_t beggining) :
         ValueParser(json, ObjectPointer::create<UndefinedObject>()),
         m_beggining(beggining) {
   }

public:
   virtual ~NumberParser() = default;

   std::shared_ptr<Parser> parseChar(std::size_t&) final override;

   friend class Parser;
};

class StringParser : public ValueParser {
protected:
   std::size_t m_beggining;

   StringParser(std::string const& json, std::size_t beggining) :
         ValueParser(json, ObjectPointer::create<String>("null")),
         m_beggining(beggining) {
   }

public:
   virtual ~StringParser() = default;

   std::shared_ptr<Parser> parseChar(std::size_t&) final override;

   friend class Parser;
};


}
}
