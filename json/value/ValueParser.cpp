#include "ValueParser.h"
#include "../Parser.inl"
#include "../ObjectPointer.inl"

#include "../exception/ParserError.h"

#include <cstddef>
#include <memory>
#include <sstream>
#include <string>
#include <utility>

namespace json {
namespace value {

std::pair<std::size_t, ObjectPointer> BeginingParser::parseJson() {
   auto parser = this_ptr.lock();
   for (std::size_t i = 0; i < m_json.size();) {
         parser = parser->parseChar(i);

         if (!parser)
            return { 0, UndefinedObject::create() };

         if (parser->end())
            return { i, parser->object() };
   }

   return { 0, UndefinedObject::create() };
}

std::shared_ptr<Parser> BeginingParser::parseChar(std::size_t& index) {

   for (; index < m_json.size(); ++index) {
      auto const char_ = m_json.at(index);

      if (char_ == ' ' || char_ == '\t' || char_ == '\n' || char_ == '\r')
         continue;

      if ((char_ >= '0' && char_ <= '9') || char_ == '-' || char_ == '.')
         return Parser::create<NumberParser>(m_json, index);
      else if (char_ == '\"') {
         ++index;
         return Parser::create<StringParser>(m_json, index);
      }

      return nullptr;
   }

   return nullptr;
}

std::shared_ptr<Parser> NumberParser::parseChar(std::size_t& index) {

   for (; index < m_json.size(); ++index) {
      auto const char_ = m_json.at(index);

      if ((char_ >= '0' && char_ <= '9') || (char_ == '-' && m_begin) || (char_ == '.' && !m_float)) {
         m_float = char_ == '.';
         m_begin = false;
         continue;
      }

      std::stringstream ss { m_json.substr(m_beggining, index - m_beggining) };
      if (m_float) {
         long double value;
         ss >> value;

         m_object.replace(json::Float::create(value));
      } else {
         int64_t value;
         ss >> value;
         m_object.replace(json::Integer::create(value));
      }
      m_end = true;
      return this_ptr.lock();
   }

   throw ParserError("string: invalid number");
}

std::shared_ptr<Parser> StringParser::parseChar(std::size_t& index) {
   std::string result;

   for (; index < m_json.size(); ++index) {
      auto const char_ = m_json.at(index);

      if (char_ == '\\') {
         ++index;
         if (index >= m_json.size())
            throw ParserError("string: empty escape sequence");

         result += m_json.at(index);
         continue;
      }

      if (char_ == '"') {
         ++index;
         m_object.cast<String>() = result;
         m_end = true;
         return this_ptr.lock();
      }

      result += char_;
   }

   throw ParserError("string: missing end of string \'\"\'");
}

}
}
