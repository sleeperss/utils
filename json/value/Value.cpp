/*
 * Container.cpp
 *
 *  Created on: Oct 13, 2019
 *      Author: sleeper
 */

#include "Value.h"

#include "../ObjectPointer.inl"

#include <string>

namespace json {

ObjectPointer Float::create(long double const value) {
   return ObjectPointer::create<Float>(value);
}

std::string Float::toString(Formatter&) const {
   return getValue();
}

std::string Float::getValue() const {
   return std::to_string(m_double);
}

ObjectPointer Integer::create(int64_t const value) {
   return ObjectPointer::create<Integer>(value);
}

std::string Integer::toString(Formatter&) const {
   return getValue();
}

std::string Integer::getValue() const {
   return std::to_string(m_int);
}

ObjectPointer String::create(std::string const& value) {
   return ObjectPointer::create<String>(value);
}

std::string String::toString(Formatter&) const {
   return "\"" + getValue() + "\"";
}

std::string String::getValue() const {
   return m_value;
}

}
