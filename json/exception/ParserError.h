/*
 * InvalidJsonString.h
 *
 *  Created on: Oct 12, 2019
 *      Author: sleeper
 */

#pragma once

#include "exception.h"

#include <string>

namespace json {

class ParserError: public exception {
public:
   ParserError(std::string const& message);
   virtual ~ParserError();
};

} /* namespace json */
