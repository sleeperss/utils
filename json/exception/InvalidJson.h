/*
 * InvalidJsonString.h
 *
 *  Created on: Oct 12, 2019
 *      Author: sleeper
 */

#pragma once

#include "exception.h"

namespace json {

class InvalidJson: public exception {
public:
   InvalidJson();
   virtual ~InvalidJson();
};

} /* namespace json */
