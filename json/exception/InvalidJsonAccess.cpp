/*
 * InvalidJonAccess.cpp
 *
 *  Created on: Oct 14, 2019
 *      Author: sleeper
 */

#include "InvalidJsonAccess.h"

/*####################*/namespace json       {/*###################*/

InvalidJsonAccess::InvalidJsonAccess() {
   m_message = "invalid json access";
}

InvalidJsonAccess::~InvalidJsonAccess() {
}

/*###################*/}// namespace json     /*###################*/

