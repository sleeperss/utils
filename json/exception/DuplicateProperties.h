/*
 * DuplicateProperties.h
 *
 *  Created on: Oct 13, 2019
 *      Author: sleeper
 */

#pragma once

#include "exception.h"

namespace json {

class DuplicateProperties: public exception {
public:
   DuplicateProperties();
   virtual ~DuplicateProperties();
};

} /* namespace json */
