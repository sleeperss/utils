/*
 * InvalidEscapeSequence.h
 *
 *  Created on: Oct 13, 2019
 *      Author: sleeper
 */

#pragma once

#include "exception.h"

namespace json {

class InvalidEscapeSequence: public exception {
public:
   InvalidEscapeSequence();
   virtual ~InvalidEscapeSequence();
};

} /* namespace json */
