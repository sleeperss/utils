/*
 * InvalidJsonString.cpp
 *
 *  Created on: Oct 12, 2019
 *      Author: sleeper
 */

#include "../../json/exception/ParserError.h"

#include <string>

namespace json {

ParserError::ParserError(std::string const& message) {
   m_message = message;
}

ParserError::~ParserError() {
}

} /* namespace json */
