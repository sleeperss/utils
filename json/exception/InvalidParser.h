/*
 * InvalidJsonString.h
 *
 *  Created on: Oct 12, 2019
 *      Author: sleeper
 */

#pragma once

#include "exception.h"

#include <string>

namespace json {

class InvalidParser: public exception {
private:
   std::string m_message;

public:
   InvalidParser(std::string const& message);
   virtual ~InvalidParser();

   virtual const char* what() const noexcept final override;
};

} /* namespace json */
