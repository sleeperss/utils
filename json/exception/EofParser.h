/*
 * EofParser.h
 *
 *  Created on: Oct 13, 2019
 *      Author: sleeper
 */

#pragma once

#include "exception.h"

namespace json {

class EofParser: public exception {
public:
   EofParser();
   virtual ~EofParser();
};

} /* namespace json */
