#include "exception.h"

/*####################*/namespace json       {/*###################*/

exception::exception() : m_message("json exception") {
}

exception::~exception() {
}

const char* exception::what() const noexcept {
   return m_message.c_str();
}

/*###################*/}// namespace json     /*###################*/

