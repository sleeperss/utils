#pragma once

#include <exception>
#include <string>

/*####################*/namespace json           {/*###################*/

class exception : public std::exception {
protected:
   std::string m_message;

public:
   exception();
   virtual ~exception();

   virtual const char* what() const noexcept override;
};

/*###################*/}// namespace json         /*###################*/

