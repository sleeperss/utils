/*
 * InvalidJsonString.cpp
 *
 *  Created on: Oct 12, 2019
 *      Author: sleeper
 */

#include "../../json/exception/InvalidParser.h"

namespace json {

InvalidParser::InvalidParser(std::string const& message) :
      m_message(message) {
}

InvalidParser::~InvalidParser() {
}

const char* InvalidParser::what() const noexcept {
   return m_message.c_str();
}

} /* namespace json */
