/*
 * InvalidJsonString.cpp
 *
 *  Created on: Oct 12, 2019
 *      Author: sleeper
 */

#include "../../json/exception/InvalidJson.h"

namespace json {

InvalidJson::InvalidJson() {
   m_message = "invalid json";
}

InvalidJson::~InvalidJson() {
}

} /* namespace json */
