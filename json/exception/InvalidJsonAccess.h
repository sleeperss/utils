/*
 * InvalidJonAccess.h
 *
 *  Created on: Oct 14, 2019
 *      Author: sleeper
 */

#pragma once

#include "exception.h"

/*####################*/namespace json       {/*###################*/

class InvalidJsonAccess: public exception {
public:
   InvalidJsonAccess();
   virtual ~InvalidJsonAccess();
};

/*###################*/}// namespace json     /*###################*/

