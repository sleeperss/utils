/*
 * Parser.h
 *
 *  Created on: Oct 13, 2019
 *      Author: sleeper
 */

#pragma once

#include "ObjectPointer.h"
#include "UndefinedObject.h"

#include <cstddef>

#include <memory>
#include <stdexcept>
#include <string>
#include <utility>

namespace json {

namespace container {
class BeginingParser;
}
namespace array {
class BeginingParser;
}
namespace value {
class BeginingParser;
}
namespace data {
class BeginingParser;
}

class Parser {
protected:
   const std::string& m_json;
   const std::weak_ptr<Parser> this_ptr;
   bool m_end = false;

   Parser(std::string const& json) :
         m_json(json) {
   }
   virtual std::shared_ptr<Parser> parseChar(std::size_t&) = 0;
   virtual std::pair<std::size_t, ObjectPointer> parseJson() {
      throw std::runtime_error("hum, we shouldn't be there... " + std::to_string(__LINE__));
   }

   template<class Type, typename ... Args>
   static std::shared_ptr<Parser> create(std::string const& json, Args&& ... args);
   static std::pair<std::size_t, ObjectPointer> parseImpl(std::string const&);
   virtual ObjectPointer& object() {
      throw std::runtime_error(__FILE__ + std::to_string(__LINE__));
   }

public:
   virtual ~Parser() = default;
   static ObjectPointer parse(std::string const&);

   bool end() const;

   friend class container::BeginingParser;
   friend class array::BeginingParser;
   friend class value::BeginingParser;
   friend class data::BeginingParser;
};

}

std::ostream& operator<<(std::ostream& os, json::ObjectPointer const& json);
