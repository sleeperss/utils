#pragma once

#include "Parser.h"

#include <memory>
#include <string>
#include <utility>

namespace json {

template<class Type, typename ... Args>
std::shared_ptr<Parser> Parser::create(std::string const& json, Args&& ... args) {
   struct make_shared_enabler: public Type {
      make_shared_enabler(std::string const& json, Args&& ... args) :
            Type(json, args...) {
      }
   };
   auto ptr = std::make_shared<make_shared_enabler>(json, std::forward<Args>(args)...);
   const_cast<std::weak_ptr<Parser>&>(ptr->this_ptr) = ptr;
   return ptr;
}

}
