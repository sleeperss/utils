#pragma once

#include "../Parser.h"
#include "Array.h"

#include <cstddef>
#include <memory>
#include <string>
#include <utility>

namespace json {

namespace array {

class BeginingParser: public Parser {
protected:
   ObjectPointer m_object { ObjectPointer::create<Array>() };

   BeginingParser(std::string const& json) :
         Parser(json) {
   }

   std::pair<std::size_t, ObjectPointer> parseJson() final override;
   virtual ObjectPointer& object() final override;

public:
   virtual ~BeginingParser() = default;
   std::shared_ptr<Parser> parseChar(std::size_t&) final override;
   std::shared_ptr<Parser> lock() const;

   friend class ValueParser;
   friend class NextParser;
};

class ValueParser: public Parser {
protected:
   BeginingParser& m_parent;

   ValueParser(std::string const& json, BeginingParser& parent) :
         Parser(json), m_parent(parent) {
   }

public:
   virtual ~ValueParser() = default;
   std::shared_ptr<Parser> parseChar(std::size_t&) final override;

   friend class Parser;
};

class NextParser: public Parser {
protected:
   BeginingParser& m_parent;

   const std::string m_key;

   NextParser(std::string const& json, BeginingParser& parent) :
         Parser(json), m_parent(parent) {
   }

public:
   virtual ~NextParser() = default;
   std::shared_ptr<Parser> parseChar(std::size_t&) final override;

   friend class Parser;
};

}
}
