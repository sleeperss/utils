/*
 * Container.cpp
 *
 *  Created on: Oct 13, 2019
 *      Author: sleeper
 */

#include "Array.h"

#include <cstddef>
#include <stdexcept>
#include <string>

#include "../exception/InvalidJsonAccess.h"
#include "../ObjectPointer.h"
#include "../UndefinedObject.h"

namespace json {

Array::Array() {
}

Array::~Array() {
}

ObjectPointer& Array::operator [](std::size_t const key) {
   try {
      return m_objects.at(key);
   } catch (std::out_of_range const&) {
      return m_objects[key] = UndefinedObject::create();
   }
}

ObjectPointer& Array::at(std::size_t const key) {
   return m_objects.at(key);
}

ObjectPointer const& Array::at(std::size_t const key) const {
   return m_objects.at(key);
}

void Array::push_back(ObjectPointer const& object) {
   m_objects.push_back(object);
}

ObjectPointer& Array::insert() {
   return m_objects.emplace_back(json::ObjectPointer::create<json::UndefinedObject>());
}

ObjectPointer& Array::operator+=(ObjectPointer&& elem) {
   return m_objects.emplace_back(std::move(elem));
}

ObjectPointer& Array::back() {
   return m_objects.back();
}

ObjectPointer Array::create() {
   return ObjectPointer::create<json::Array>();
}

std::string Array::toString(Formatter& formatter) const {
   std::string result;

   for (auto const& object : m_objects) {
      auto subFormatter = formatter.clone();
      if (formatter.formatte)
         subFormatter.prefix = "\t" + subFormatter.prefix;
      result += std::string(result.size() ? ", " : "") + (formatter.formatte ? "\n" + subFormatter.prefix : "")
            + object.toString(subFormatter);
   }

   return (formatter.formatte ? "\033[33;1m[\033[0m" : "[") + result
         + (formatter.formatte ? "\n" + formatter.prefix + "\033[33;1m]\033[0m\n" : "]");
}

std::vector<ObjectPointer>::iterator Array::begin() {
   return m_objects.begin();
}

std::vector<ObjectPointer>::iterator Array::end() {
   return m_objects.end();
}

std::vector<ObjectPointer>::const_iterator Array::begin() const {
   return m_objects.begin();
}

std::vector<ObjectPointer>::const_iterator Array::end() const {
   return m_objects.end();
}

std::size_t Array::size() const {
   return m_objects.size();
}

}
