/*
 * Container.h
 *
 *  Created on: Oct 12, 2019
 *      Author: sleeper
 */

#pragma once

#include <vector>

#include "../Object.h"

#include <cstddef>
#include <string>

namespace json {

namespace array {
class ValueParser;
}

class Array: public Object {
private:
   std::vector<ObjectPointer> m_objects;

   Array();

public:
   virtual ~Array();

   ObjectPointer& operator[](std::size_t) final;
   ObjectPointer& at(std::size_t) final;
   ObjectPointer const& at(std::size_t) const final;
   ObjectPointer& insert() final;
   ObjectPointer& back() final;

   ObjectPointer& operator+=(ObjectPointer&&) final;

   void push_back(ObjectPointer const&);

   static ObjectPointer create();

   std::string toString(Formatter&) const final override;

   std::vector<ObjectPointer>::iterator begin();
   std::vector<ObjectPointer>::iterator end();

   std::vector<ObjectPointer>::const_iterator begin() const;
   std::vector<ObjectPointer>::const_iterator end() const;

   std::size_t size() const;

   friend class array::ValueParser;
   friend class Parser;
   friend class ObjectPointer;
};

} /* namespace json */
