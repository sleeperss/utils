#include "UndefinedObject.h"
#include "ObjectPointer.inl"

#include "exception/InvalidJsonAccess.h"

#include <string>

/*####################*/namespace json       {/*###################*/

UndefinedObject::UndefinedObject() {
}

UndefinedObject::~UndefinedObject() {
}

ObjectPointer UndefinedObject::create() {
   return ObjectPointer::create<UndefinedObject>();
}

std::string UndefinedObject::toString(Formatter&) const {
   throw json::InvalidJsonAccess();
}

/*###################*/}// namespace json     /*###################*/
