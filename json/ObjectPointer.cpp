/*
 * ContainerPtr.cpp
 *
 *  Created on: Oct 13, 2019
 *      Author: sleeper
 */

#include "ObjectPointer.h"
#include "ObjectPointer.inl"
#include "UndefinedObject.h"
#include "exception/InvalidJsonAccess.h"

#include <cstddef>
#include <memory>
#include <stdexcept>

#include <string>
#include <utility>
#include <vector>

namespace json {

ObjectPointer::ObjectPointer(std::shared_ptr<Object> const& ptr) :
      m_ptr(ptr) {
}

ObjectPointer::~ObjectPointer() {
}

ObjectPointer& ObjectPointer::operator [](const char* key) const {
   if (!m_ptr)
      throw std::runtime_error("nullptr..." + std::to_string(__LINE__));
   return m_ptr->operator [](std::string(key));
}

ObjectPointer& ObjectPointer::operator [](std::string const& key) const {
   if (!m_ptr)
      throw std::runtime_error("nullptr..." + std::to_string(__LINE__));
   return m_ptr->operator [](key);
}

ObjectPointer& ObjectPointer::operator [](std::size_t index) const {
   if (!m_ptr)
      throw std::runtime_error("nullptr..." + std::to_string(__LINE__));
   return m_ptr->operator [](index);
}

ObjectPointer& ObjectPointer::insert() {
   if (!m_ptr)
      throw std::runtime_error("nullptr..." + std::to_string(__LINE__));
   return m_ptr->insert();
}

ObjectPointer& ObjectPointer::back() const {
   if (!m_ptr)
      throw std::runtime_error("nullptr..." + std::to_string(__LINE__));
   return m_ptr->back();
}

std::string ObjectPointer::toString(bool formatte) const {
   if (!m_ptr)
      throw std::runtime_error("nullptr..." + std::to_string(__LINE__));
   return m_ptr->toString(formatte);
}

std::string ObjectPointer::toString(Formatter& formatter) const {
   if (!m_ptr)
      throw std::runtime_error("nullptr..." + std::to_string(__LINE__));
   return m_ptr->toString(formatter);
}

std::string ObjectPointer::value() const {
   return this->cast<json::String>().getValue();
}

std::vector<uint8_t> const& ObjectPointer::data() const {
   return this->cast<json::Data>().getData();
}

ObjectPointer& ObjectPointer::operator =(ObjectPointer const& ptr) {
   if (!m_ptr)
      throw std::runtime_error("nullptr..." + std::to_string(__LINE__));
   if (!*this)
      return this->m_ptr = ptr.m_ptr, *this;
   throw json::InvalidJsonAccess();
}

ObjectPointer& ObjectPointer::operator =(ObjectPointer& ptr) {
   if (!m_ptr)
      throw std::runtime_error("nullptr..." + std::to_string(__LINE__));
   if (!*this)
      return this->m_ptr = ptr.m_ptr, *this;
   throw json::InvalidJsonAccess();
}

ObjectPointer& ObjectPointer::operator =(ObjectPointer&& ptr) {
   if (!m_ptr)
      throw std::runtime_error("nullptr..." + std::to_string(__LINE__));
   if (!*this)
      return this->m_ptr.swap(ptr.m_ptr), *this;
   throw json::InvalidJsonAccess();
}

ObjectPointer& ObjectPointer::operator =(std::nullptr_t&&) {
   return this->m_ptr = json::UndefinedObject::create().m_ptr, *this;
}

ObjectPointer& ObjectPointer::replace(ObjectPointer const& ptr) {
   return this->m_ptr = ptr.m_ptr, *this;
}

ObjectPointer& ObjectPointer::replace(ObjectPointer& ptr) {
   return this->m_ptr = ptr.m_ptr, *this;
}

ObjectPointer& ObjectPointer::replace(ObjectPointer&& ptr) {
   return this->m_ptr.swap(ptr.m_ptr), *this;//fixme
}

ObjectPointer::operator bool() const {
   return !std::dynamic_pointer_cast<UndefinedObject>(m_ptr);
}

} /* namespace json */
