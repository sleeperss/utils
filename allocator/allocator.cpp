#include "allocator.h"

#include <logger/logger.h>
#include <utils/utils.inl>

#include <assert.h>
#include <atomic>
#include <cerrno>
#include <cstddef>
#include <cstring>
#include <ios>
#include <iterator>
#include <memory>
#include <new>
#include <ostream>
#include <string>
#include <sys/mman.h>
#include <unistd.h>
#include <utility>

allocator_t::allocator_t() {
   m_id = ++next_id;
}

allocator_t::~allocator_t() {
   if (m_used_memory.size()) {
      std::size_t used = 0;

      for (const auto& mem : m_used_memory)
         used += mem.second;

      llog::tagger_t const tag {"alloc_" + sid(), llog::Cyan };
      llog::error << "memory leak detected: " << used << " bytes in " << m_used_memory.size() << " blocks !" << std::endl;
   }
}

allocator_t::free_memory_t::free_memory_t () :
      begin { mmap(nullptr, thread_memory_size, PROT_NONE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0) } {
   if (begin == MAP_FAILED) { // @suppress("C-Style cast instead of C++ cast")
      llog::tagger_t const tag {"alloc_" + sid(), llog::Cyan };
      llog::error << "unable to reserve " << thread_memory_size << " size of memory (" << errno << "): " << strerror(errno) << std::endl;
      throw std::bad_alloc();
   }

   by_offset.emplace(begin, thread_memory_size);
   by_size.emplace(thread_memory_size, begin);
}

allocator_t::free_memory_t::~free_memory_t() {
   if (begin != MAP_FAILED) // @suppress("C-Style cast instead of C++ cast")
      munmap(begin, thread_memory_size);
}

const std::size_t allocator_t::page_size = sysconf(_SC_PAGESIZE);

void* allocator_t::lextend(void* ptr, std::size_t const new_size) {
   llog::tagger_t const tag {"alloc_" + sid(), llog::Cyan };
   auto const used = [&]() {
      std::shared_lock<std::shared_mutex> const lock { m_mutex };
      auto const used = m_used_memory.find(ptr);

      if (used == m_used_memory.end()) {
         llog::error << "Invalid access at 0x" << std::hex << ptr << " !" << std::endl;
         std::terminate();
      }

      return used;
   }();

   if (used->second < new_size) {
      auto const lower = [&]() {
         std::shared_lock<std::shared_mutex> const lock { m_free_memory.mutex };
         if (!m_free_memory.by_offset.size())
            throw std::bad_alloc();

         auto lower = m_free_memory.by_offset.upper_bound(ptr);

         if (m_free_memory.by_offset.begin() == lower)
            throw std::bad_alloc();

         return --lower;
      }();

      ptr = m_free_memory.use_right(lower->first, new_size - used->second);

      std::lock_guard<std::shared_mutex> const lock { m_mutex };
      m_used_memory.erase(used);
      m_used_memory.emplace(ptr, new_size);
   }

   return ptr;
}

void* allocator_t::rextend(void* const ptr, std::size_t const new_size) {
   llog::tagger_t const tag {"alloc_" + sid(), llog::Cyan };
   auto const used = [&]() {
      std::shared_lock<std::shared_mutex> const lock { m_mutex };
      auto const used = m_used_memory.find(ptr);

      if (used == m_used_memory.end()) {
         llog::error << "Invalid access at 0x" << std::hex << ptr << " !" << std::endl;
         std::terminate();
      }

      return used;
   }();

   if (used->second < new_size) {
      {
         std::shared_lock<std::shared_mutex> const lock { m_free_memory.mutex };
         if (m_free_memory.by_offset.find(byte(ptr) + used->second) == m_free_memory.by_offset.end())
            throw std::bad_alloc();
      }

      m_free_memory.use_left(byte(ptr) + used->second, new_size - used->second);
      used->second = new_size;
   }

   return ptr;
}

alloc_initializer_t::alloc_initializer_t() {
   llog::init();
   std::shared_ptr<allocator_t>::operator=(make_shared_enabler<allocator_t>());
}


alloc_initializer_t::alloc_initializer_t(std::nullptr_t const&) :
      std::shared_ptr<allocator_t>(nullptr) {
}

alloc_initializer_t::alloc_initializer_t(alloc_initializer_t const& alloc) :
      std::shared_ptr<allocator_t>(alloc) {
   *this = alloc;
}

alloc_initializer_t::alloc_initializer_t(alloc_initializer_t&& alloc) :
      std::shared_ptr<allocator_t>() {
   *this = std::move(alloc);
}

alloc_initializer_t& alloc_initializer_t::operator=(alloc_initializer_t const& alloc) {
   std::shared_ptr<allocator_t>::operator=(alloc);
   return *this;
}

alloc_initializer_t& alloc_initializer_t::operator=(alloc_initializer_t&& alloc) {
   std::shared_ptr<allocator_t>::operator=(std::move(alloc));
   return *this;
}

void allocator_t::set_instance(alloc_initializer_t const& allocator) {
   instance = allocator;
}

alloc_initializer_t const& allocator_t::get_instance() {
   if (instance)
      return instance;
   else
      return instance = alloc_initializer_t{};
}

std::size_t allocator_t::id() {
   return get_instance()->m_id;
}

std::string allocator_t::sid() {
   return std::to_string(id());
}

void allocator_t::free(void* const ptr) {//fixme llog inst order
   auto const used = [&]() {
      std::shared_lock<std::shared_mutex> const lock { m_mutex };
      auto const used = m_used_memory.find(ptr);

      if (used == m_used_memory.end()) {
         llog::tagger_t const tag {"alloc_" + sid(), llog::Cyan };
         llog::error << "Invalid free at 0x" << std::hex << ptr << " !" << std::endl;
         std::terminate();
      }

      return used;
   }();

   m_free_memory.release(used->first, used->second);

   std::lock_guard<std::shared_mutex> const lock { m_mutex };
   m_used_memory.erase(used);
}

void* allocator_t::alloc(std::size_t const size) {
   llog::tagger_t const tag {"alloc_" + sid(), llog::Cyan };
   auto const freespace = [&]() {
      std::shared_lock<std::shared_mutex> const lock { m_free_memory.mutex };

      auto const freespace = m_free_memory.by_size.begin();

      if (freespace == m_free_memory.by_size.end() || freespace->first < size) {
         llog::warning << "Can't allocate block of size " << size << ", no more space available!" << std::endl;
         throw std::bad_alloc();
      }

      return freespace;
   }();

   auto ptr = m_free_memory.use_center(freespace->second, size);

   std::lock_guard<std::shared_mutex> const lock { m_mutex };
   m_used_memory.emplace(ptr, size);
   return ptr;
}

// -----------------------------------------------------------------

void* allocator_t::free_memory_t::use_left(void* const ptr, std::size_t const size) {
   llog::tagger_t const tag {"alloc_" + sid(), llog::Cyan };
   std::lock_guard<std::shared_mutex> const lock { mutex };

   auto const free_mem = by_offset.find(ptr);

#ifdef DEBUG
   if (ptr < begin || free_mem == by_offset.end()) {
      llog::error << llog::tag_t {"alloc_" + sid(), llog::Cyan } << "Invalid address " << ptr << "!" << std::endl;
      std::terminate();
   }
#endif

   auto free_mem_ptr = free_mem->first;
   auto free_mem_size = free_mem->second;

   if (free_mem_size == size)
      return use(free_mem_ptr);

#ifdef DEBUG
   if (free_mem_size < size) {
      llog::error << "Invalid block size!" << std::endl;
      std::terminate();
   }
#endif

   for (auto it = by_size.equal_range(free_mem_size); it.first != it.second; ++it.first) {
      if (it.first->second == free_mem_ptr) {
         block_alloc(ptr, size);

         by_size.erase(it.first);
         by_offset.erase(free_mem);

         free_mem_size -= size;
         free_mem_ptr = byte(free_mem_ptr) + size;

         by_size.emplace(free_mem_size, free_mem_ptr);
         by_offset.emplace(free_mem_ptr, free_mem_size);

         return ptr;
      }
   }

   llog::error << "hum, we shouldn't be there... " << __FILE__ << ":" << __LINE__ << std::endl;
   std::terminate();
}

void* allocator_t::free_memory_t::use_right(void* ptr, std::size_t const size) {
   llog::tagger_t const tag {"alloc_" + sid(), llog::Cyan };
   std::lock_guard<std::shared_mutex> const lock { mutex };

   auto const free_mem = by_offset.find(ptr);

#ifdef DEBUG
   if (ptr < begin || free_mem == by_offset.end()) {
      llog::error << "Invalid address " << ptr << "!" << std::endl;
      std::terminate();
   }
#endif

   auto& [free_mem_ptr, free_mem_size] = *free_mem;

   if (free_mem_size == size)
      return use(free_mem_ptr);

#ifdef DEBUG
   if (free_mem_size < size) {
      llog::error << "Invalid block size!" << std::endl;
      std::terminate();
   }
#endif

   for (auto it = by_size.equal_range(free_mem_size); it.first != it.second; ++it.first) {
      if (it.first->second == free_mem_ptr) {
         ptr = byte(free_mem_ptr) + free_mem_size - size;
         block_alloc(ptr, size);

         by_size.erase(it.first);
         free_mem_size -= size;
         by_size.emplace(free_mem_size, free_mem_ptr);

         return ptr;
      }
   }

   llog::error << "hum, we shouldn't be there... " << __FILE__ << ":" << __LINE__ << std::endl;
   std::terminate();
}

void* allocator_t::free_memory_t::use_center(void* ptr, std::size_t const size) {
   llog::tagger_t const tag {"alloc_" + sid(), llog::Cyan };
   std::lock_guard<std::shared_mutex> const lock { mutex };

   auto const free_mem = by_offset.find(ptr);

#ifdef DEBUG
   if (ptr < begin || free_mem == by_offset.end()) {
      llog::error << "Invalid address " << ptr << "!" << std::endl;
      std::terminate();
   }
#endif

   auto free_mem_ptr = free_mem->first;
   auto free_mem_size = free_mem->second;

   if (free_mem_size == size)
      return use(free_mem_ptr);

#ifdef DEBUG
   if (free_mem_size < size) {
      llog::error << "Invalid block size!" << std::endl;
      std::terminate();
   }
#endif

   for (auto it = by_size.equal_range(free_mem_size); it.first != it.second; ++it.first) {
      if (it.first->second == free_mem_ptr) {
         free_mem_size -= size;
         bool const odd = free_mem_size & 0b1;
         free_mem_size >>= 1;

         ptr = byte(free_mem_ptr) + free_mem_size;
         block_alloc(ptr, size);

         by_size.erase(it.first);
         by_offset.erase(free_mem_ptr);

         if (free_mem_size) {
            by_size.emplace(free_mem_size, free_mem_ptr);
            by_offset.emplace(free_mem_ptr, free_mem_size);
         }

         free_mem_ptr = byte(ptr) + size;

         if (odd)
            ++free_mem_size;

         by_size.emplace(free_mem_size, free_mem_ptr);
         by_offset.emplace(free_mem_ptr, free_mem_size);

         return ptr;
      }
   }

   llog::error << "hum, we shouldn't be there... " << __FILE__ << ":" << __LINE__ << std::endl;
   std::terminate();
}

void* allocator_t::free_memory_t::use(void* const ptr) {
   llog::tagger_t const tag {"alloc_" + sid(), llog::Cyan };
   std::lock_guard<std::shared_mutex> const lock { mutex };

   auto const free_mem = by_offset.find(ptr);

#ifdef DEBUG
   if (ptr < begin || free_mem == by_offset.end()) {
      llog::error << "Invalid address " << ptr << "!" << std::endl;
      std::terminate();
   }
#endif

   auto const free_mem_size = free_mem->second;

   for (auto it = by_size.equal_range(free_mem_size); it.first != it.second; ++it.first) {
      if (it.first->second == ptr) {
         block_alloc(ptr, free_mem_size);

         by_size.erase(it.first);
         by_offset.erase(ptr);

         return ptr;
      }
   }

   llog::error << "hum, we shouldn't be there... " << __FILE__ << ":" << __LINE__ << std::endl;
   std::terminate();
}

void allocator_t::free_memory_t::release(void* ptr, std::size_t size) {
   decrement_inuse(size);

   std::lock_guard<std::shared_mutex> const lock { mutex };

   // merge right
   if (auto const free_right = by_offset.find(byte(ptr) + size); free_right != by_offset.end()) {
      for (auto it = by_size.equal_range(free_right->second);;) {
         if (it.first->second == free_right->first) {
            by_size.erase(it.first);
            break;
         }

         if (++it.first == it.second) {
            llog::tagger_t const tag {"alloc_" + sid(), llog::Cyan };
            llog::error << "hum, we shouldn't be there... " << __FILE__ << ":" << __LINE__ << std::endl;
            std::terminate();
         }
      }

      size += free_right->second;
      by_offset.erase(free_right);
   }

   // merge left
   if (auto it = by_offset.upper_bound(ptr);
      (it != by_offset.begin()) && by_offset.size()) {
      auto const free_left = std::prev(it);

      if (byte(free_left->first) + free_left->second == ptr) {
         for (auto it = by_size.equal_range(free_left->second);;) {
            if (it.first->second == free_left->first) {
               by_size.erase(it.first);
               break;
            }

            if (++it.first == it.second) {
               llog::tagger_t const tag {"alloc_" + sid(), llog::Cyan };
               llog::error << "hum, we shouldn't be there... " << __FILE__ << ":" << __LINE__ << std::endl;
               std::terminate();
            }
         }

         ptr = byte(ptr) - free_left->second;
         size += free_left->second;
         by_offset.erase(free_left->first);
      }
   }

   by_offset.emplace(ptr, size);
   by_size.emplace(size, ptr);

   auto page_aligned = byte(((reinterpret_cast<std::size_t>(ptr) + page_size - 1) / page_size) * page_size);
   assert(page_aligned >= ptr);
   mprotect(page_aligned, size - (page_aligned - byte(ptr)), PROT_NONE);
}

void* allocator_t::free_memory_t::block_alloc(void* const ptr, std::size_t const size) const {
   llog::tagger_t const tag {"alloc_" + sid(), llog::Cyan };
   auto const page = byte((reinterpret_cast<std::size_t>(ptr) / page_size) * page_size);
   if (mprotect(page, size + (byte(ptr) - page), PROT_READ | PROT_WRITE)) {
      llog::error << "Allocation at 0x" << std::hex << reinterpret_cast<std::size_t>(page) << " of " << std::dec << reinterpret_cast<std::size_t>(size + (byte(ptr) - page)) << " bytes failed (" << errno << ": " << strerror(errno) << ")!" << std::endl;
      throw std::bad_alloc();//fixme huge chunk ...
   }
   increment_inuse(size);

#ifdef ADDRESS_SANITIZER
      [&]() __attribute__((no_sanitize_address)) {
         auto* const sanitize_begin = reinterpret_cast<uint8_t*>(byte(reinterpret_cast<std::size_t>(ptr) >> 0x3) + SHADOW_OFFSET);
         auto const* const sanitize_end = byte(reinterpret_cast<std::size_t>(byte(ptr) + size) >> 0x3) + SHADOW_OFFSET;

         auto* const sanitize_begin_64aligned = reinterpret_cast<uint64_t*>(reinterpret_cast<std::size_t>(sanitize_begin + 7) & ~0b1111ul);
         auto* const sanitize_end_64aligned = reinterpret_cast<uint64_t*>(reinterpret_cast<std::size_t>(sanitize_end) & ~0b1111ul);

         for (uint8_t* it = sanitize_begin; it != byte(sanitize_begin_64aligned); ++it)
            it[0] = 0;

         for (uint64_t* it = sanitize_begin_64aligned; it != sanitize_end_64aligned; ++it)
            it[0] = 0;

         for (uint8_t* it = byte(sanitize_end_64aligned); it != sanitize_end; ++it)
            it[0] = 0;
      }();
#endif

   return page;
}

void allocator_t::free_memory_t::increment_inuse(std::size_t const size) const {
   llog::tagger_t const tag {"alloc_" + sid(), llog::Cyan };
   m_inuse += size;
   std::size_t max_use = m_max_inuse;
   bool exchanged = false;
   while (m_inuse > max_use && !(exchanged = m_max_inuse.compare_exchange_strong(max_use, m_inuse)));

   max_use = m_max_inuse;

   if (exchanged) {
      std::string max_use_str;

      if (auto const value = max_use >> 40)
         max_use_str = std::to_string(value) + "TB";
      else if (auto const value = max_use >> 30)
         max_use_str = std::to_string(value) + "GB";
      else if (auto const value = max_use >> 20)
         max_use_str = std::to_string(value) + "MB";
      else if (auto const value = max_use >> 10)
         max_use_str = std::to_string(value) + "KB";
      else
         max_use_str = std::to_string(value) + "B";

      llog::info << "Peak allocated memory reached (" << max_use_str << ")!" << std::endl;
   }
}

void allocator_t::free_memory_t::decrement_inuse(std::size_t const size) const {
   assert(m_inuse >= size);
   m_inuse -= size;
}

// -----------------------------------------------------------------

std::atomic<std::size_t> allocator_t::next_id { 0 };
alloc_initializer_t thread_local allocator_t::instance { };

std::atomic<std::size_t> allocator_t::m_inuse { 0 };
std::atomic<std::size_t> allocator_t::m_max_inuse { 0 };
