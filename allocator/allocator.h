#pragma once

#include <atomic>
#include <cstddef>
#include <functional>
#include <map>
#include <memory>
#include <shared_mutex>
#include <string>
#include <sys/mman.h>

class allocator_t;

struct alloc_initializer_t : public std::shared_ptr<allocator_t> {
private:
   alloc_initializer_t();

public:
   alloc_initializer_t(std::nullptr_t const&);
   alloc_initializer_t(alloc_initializer_t const&);
   alloc_initializer_t(alloc_initializer_t &&);

   alloc_initializer_t& operator=(alloc_initializer_t const&);
   alloc_initializer_t& operator=(alloc_initializer_t &&);

   friend class allocator_t;
};

class allocator_t {
private:
   struct free_memory_t {
      void* const begin;

      std::multimap<std::size_t, void*, std::greater<std::size_t>> by_size {};
      std::map<void*, std::size_t> by_offset {};
      std::shared_mutex mutex {};

      void* use(void*);
      void* use_left(void*, std::size_t);
      void* use_right(void*, std::size_t);
      void* use_center(void*, std::size_t);
      void release(void*, std::size_t);

      free_memory_t();
      virtual ~free_memory_t();

   private:
      void* block_alloc(void*, std::size_t) const;
      void increment_inuse(std::size_t) const;
      void decrement_inuse(std::size_t) const;
   } m_free_memory;

   std::map<void*, std::size_t> m_used_memory {};
   std::shared_mutex m_mutex {};
   std::size_t m_id;

   static std::atomic<std::size_t> m_inuse;
   static std::atomic<std::size_t> m_max_inuse;

   static const std::size_t page_size;
   static constexpr std::size_t thread_memory_size { 16ul << 30 };
   static std::atomic<std::size_t> next_id;
   static thread_local alloc_initializer_t instance;

protected:
   allocator_t();

public:
   virtual ~allocator_t();

   void* alloc(std::size_t);
   void free(void*);

   void* rextend(void*, std::size_t);
   void* lextend(void*, std::size_t);

   static void set_instance(alloc_initializer_t const& allocator);
   static alloc_initializer_t const& get_instance();

   static std::size_t id();
   static std::string sid();
};

