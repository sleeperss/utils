#include "Scheduler.h"
#include "Job.h"

#include <utils/utils.inl>
#include <logger/logger.h>

#include <atomic>
#include <chrono>
#include <cstddef>
#include <functional>
#include <memory>
#include <ostream>
#include <stdexcept>
#include <string>
#include <unordered_map>
#include <utility>


/*####################*/namespace job        {/*###################*/

Scheduler::Scheduler() {
}

Scheduler::~Scheduler() {
}

Scheduler::lock_t Scheduler::init(std::unordered_map<type_t, std::unique_ptr<WorkerPoll>>&& worker_polls) {
   if(m_worker_polls.size())
      throw std::runtime_error("double scheduler initialization");

   m_worker_polls = std::move(worker_polls);

   return {};
}

std::string Scheduler::current_job_name() {
   auto const current_job = get().m_current_job.lock();
   return (current_job ? current_job->m_name : "main");
}

std::string Scheduler::worker_name() {
   auto const current_job = get().m_current_job.lock();
   return (current_job ? current_job->m_worker_name : "t0");
}

Scheduler& Scheduler::get() {
   if (!instance)
      instance = create();

   return *instance;
}

std::shared_ptr<Scheduler> Scheduler::create() {
   return make_shared_enabler<Scheduler>();
}

Scheduler::lock_t::~lock_t() {
   for (auto const& poll : m_worker_polls)
      poll.second->stop();

   m_worker_polls.clear();
}

Scheduler& Scheduler::operator <<(operation_t const& operation) {
   return (*operation)(*this);
}

Scheduler& Scheduler::operator <<(op::operation_t const& operation) {
   return operation(*this);
}

Scheduler& Scheduler::reset() {
   assert(!m_attached_jobs.size());
   assert(!m_detached_jobs.size());
   assert(!m_resume_jobs.size());

   m_attached = false;

   m_attached_jobs = { };
   m_detached_jobs = { };
   m_resume_jobs = { };

   m_current_type = { };
   m_current_priority = { };
   m_current_stack_size = { 25 << 10 };
   m_job_name = { };

   return *this;
}

Scheduler& Scheduler::candrop() {
   m_current_priority.m_timeout = std::chrono::seconds { 1 };
   return *this;
}

Scheduler& Scheduler::nodrop() {
   m_current_priority.m_timeout = { };
   return *this;
}

Scheduler& Scheduler::flush() {
   auto const job_poll = std::make_shared<job_poll_t>(m_attached_jobs.size() + 1);

   for (auto& [type, job] : m_detached_jobs) {
      auto const worker_poll = m_worker_polls.find(type);
      if (worker_poll == m_worker_polls.end())
         throw std::runtime_error("missing job type poll");

      worker_poll->second->addJob(Job::create(std::move(job.function), job.priority, type, job.stack_size,
         job.name.size() ? job.name : "dt_" + std::to_string(Job::uid())));
   }
   m_detached_jobs.clear();

   for (const auto& job : m_resume_jobs) {
      if (auto currentJob = m_current_job.lock(); currentJob && job == currentJob) {
         currentJob->m_do_resume = true;
         currentJob->detach();
      } else {

         auto const worker_poll = m_worker_polls.find(job->type());
         if (worker_poll == m_worker_polls.end())
            throw std::runtime_error("missing job poll type");

         worker_poll->second->addJob(job);
      }
   }
   m_resume_jobs.clear();

   if (m_attached_jobs.size()) {
      for (auto const& [type, job] : m_attached_jobs) {
         auto const worker_poll = m_worker_polls.find(type);
         if (worker_poll == m_worker_polls.end())
            throw std::runtime_error("missing job type poll");

         worker_poll->second->addJob(Job::create(
            [run = job.function, job_poll, main_job = m_current_job.lock()]() {
               bool finished { false };
               SCOPE_EXIT([&]() {
                  if (!finished)
                     ++job_poll->unterminated_number;

                  if (--job_poll->size == 0) {
                     assert(main_job);
                     assert(!main_job->terminated());
                     Scheduler::get() << op::resume_t { main_job } << job::flush;
                  } else if (!main_job) {
                     std::lock_guard<std::mutex> const lock { job_poll->mutex };
                     job_poll->cond_var.notify_one();
                  }
               });

               run();
               finished = true;
            }, job.priority, type, job.stack_size,
            job.name.size() ? job.name : "at_" + std::to_string(Job::uid())));
      }

      m_attached_jobs.clear();
      if (auto currentJob = m_current_job.lock(); currentJob) {
         currentJob->m_attached_job_poll = job_poll;
         currentJob->detach();
      } else {
         std::unique_lock<std::mutex> lock { job_poll->mutex };
         while (job_poll->size != 1)
            job_poll->cond_var.wait(lock);
      }
   }

   return reset();
}

Scheduler& Scheduler::addJob(std::function<void()> const& job) {
   if (m_attached)
      m_attached_jobs.emplace(m_current_type, new_jobs__t { job, m_job_name, m_current_priority, m_current_stack_size });
   else
      m_detached_jobs.emplace(m_current_type, new_jobs__t { job, m_job_name, m_current_priority, m_current_stack_size });

   return *this;
}

Scheduler& Scheduler::resumeJob(std::shared_ptr<Job> const& job) {
   assert(!job->terminated());

   if (!job)
      throw std::runtime_error("job nullptr...");

   if (m_current_priority)
      job->m_priority = m_current_priority;

   m_resume_jobs.push_back(job);
   return *this;
}

std::shared_ptr<Job> Scheduler::this_job() {
   return Scheduler::get().m_current_job.lock();
}

Scheduler& Scheduler::attached() {
   m_attached = true;
   return *this;
}

Scheduler& Scheduler::detached() {
   m_attached = false;
   return *this;
}

Scheduler& Scheduler::type(type_t const& type) {
   m_current_type = type;
   return *this;
}

Scheduler& Scheduler::priority(priority_t const& priority) {
   m_current_priority = priority;
   return *this;
}

Scheduler& Scheduler::stack_size(std::size_t const stack_size) {
   m_current_stack_size = stack_size;
   return *this;
}

Scheduler& Scheduler::name(std::string const& name) {
   m_job_name = name;
   return *this;
}

std::unordered_map<type_t, std::unique_ptr<WorkerPoll>> Scheduler::m_worker_polls {};

thread_local std::shared_ptr<Scheduler> Scheduler::instance { nullptr };

/*###################*/} // namespace job      /*###################*/
