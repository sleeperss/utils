#pragma once

#include "jobs.h"
#include "WorkerPoll.h"

#include <atomic>
#include <cstddef>

#include <functional>
#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

/*####################*/namespace job {/*###################*/

struct job_poll_t {
   std::atomic<std::size_t> size;
   std::atomic<std::size_t> unterminated_number { 0 };
   std::condition_variable cond_var;
   std::mutex mutex;
   job_poll_t(std::size_t const size) : size(size) {};
};

class Scheduler {
public:
   struct lock_t {
      lock_t() = default;
      virtual ~lock_t();
   };

   using worker_polls_t = std::unordered_map<type_t, std::unique_ptr<WorkerPoll>>;

   static lock_t init(worker_polls_t&& worker_polls);

   static Scheduler& get();
   static std::string current_job_name();
   static std::string worker_name();

private:
   static thread_local std::shared_ptr<Scheduler> instance;
   static std::shared_ptr<Scheduler> create();

   static worker_polls_t m_worker_polls;
   std::weak_ptr<Job> m_current_job {};

   bool m_attached { false };
   struct new_jobs__t {
      std::function<void()> function;
      std::string name;
      priority_t priority;
      std::size_t stack_size;
   };
   using new_jobs_t = std::unordered_multimap<type_t, new_jobs__t>;
   using jobs_t = std::vector<std::shared_ptr<Job>>;

   new_jobs_t m_attached_jobs { };
   new_jobs_t m_detached_jobs { };
   jobs_t m_resume_jobs { };

   type_t m_current_type { };
   priority_t m_current_priority { };
   std::size_t m_current_stack_size { 25 << 10 };
   std::string m_job_name { };

protected:
   Scheduler();
   Scheduler(Scheduler const&) = delete;
   Scheduler& operator=(Scheduler const&) = delete;

public:
   ~Scheduler();

   Scheduler& operator<<(operation_t const&);
   Scheduler& operator<<(op::operation_t const&);

   Scheduler& flush();

   Scheduler& candrop();
   Scheduler& nodrop();

   Scheduler& reset();
   Scheduler& attached();
   Scheduler& detached();

   Scheduler& type(type_t const&);
   Scheduler& priority(priority_t const&);
   Scheduler& stack_size(std::size_t);
   Scheduler& name(std::string const&);

   Scheduler& addJob(std::function<void()> const& job);
   Scheduler& resumeJob(std::shared_ptr<Job> const& job);

   static std::shared_ptr<Job> this_job();

   friend class Worker;
   friend class lock_t;
   friend class Job;
};


/*###################*/} // namespace job      /*###################*/
