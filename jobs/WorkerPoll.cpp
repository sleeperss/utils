#include "WorkerPoll.h"
#include "Scheduler.h"

#include <algorithm>

#include <sighandler/signalhandler.h>
#include <logger/logger.h>

#include <assert.h>
#include <atomic>
#include <chrono>
#include <cstddef>
#include <memory>
#include <ostream>
#include <string>
#include <string.h>
#include <thread>
#include <vector>


/*####################*/namespace job        {/*###################*/

WorkerPoll::WorkerPoll(std::size_t const nb_jobs, std::size_t const sleep_duration) :
   m_sleep_duration(sleep_duration),
   m_workers([&]() {
      std::vector<std::unique_ptr<Worker>> workers {};

      for (unsigned int i = 0; i < nb_jobs; ++i)
         workers.push_back(std::make_unique<Worker>(*this));

      return workers;
   } ()) {
}

WorkerPoll::~WorkerPoll() {
   assert(m_status == DONE);

   assert(!waiting.jobs.size());
   assert(!future.jobs.size());
   assert(!scheduled.jobs.size());
}

void WorkerPoll::stop() {
   assert(m_status == RUNNING);
   m_status = KILLING;

   for (auto const& worker : m_workers)
      worker->wait_stop();

   llog::tagger_t const tag { "~WorkerPoll", llog::LightRed };
   if (scheduled.unterminated)
      llog::warning << "Deleting poll with " << scheduled.unterminated << " remaining jobs" << std::endl;

   assert(m_status == DONE);
}

void WorkerPoll::addJob(const std::shared_ptr<Job>& job) {
   assert(m_status == RUNNING || (m_status == KILLING && !job->is_new()));

   assert(!job->terminated());
   job->set();

   if (job->priority().m_wanted_at > std::chrono::system_clock::now()) {
      std::lock_guard<std::shared_mutex> const lock { future.mutex };
      future.jobs.emplace(job->priority(), job);
   } else {
      std::lock_guard<std::shared_mutex> const lock { waiting.mutex };
      waiting.jobs.emplace(job->priority(), job);
   }

   std::lock_guard<std::mutex> const lock { scheduled.cv.mutex };
   scheduled.cv.cv.notify_one();
}

auto WorkerPoll::update() {
   std::shared_lock<std::shared_mutex> lock { scheduled.mutex };
   if (scheduled.index >= scheduled.jobs.size()) {
      lock.unlock();
      std::unique_lock<std::shared_mutex> lock { scheduled.mutex };

      if (scheduled.index < scheduled.jobs.size())
         return m_status;

      scheduled.jobs.clear();
      scheduled.index = 0;

      {
         std::shared_lock<std::shared_mutex> const lock_future { future.mutex };
         std::shared_lock<std::shared_mutex> const lock_waiting { waiting.mutex };
         if (m_status != RUNNING &&
           !(future.jobs.size() || waiting.jobs.size()) &&
           !scheduled.processing)
            return m_status = DONE;
      }

      add_future();
      add_waiting();

      if (!scheduled.jobs.size()) {
         std::shared_lock<std::shared_mutex> lock_waiting { waiting.mutex };

         if (!waiting.jobs.size()) {
            if (future.jobs.size()) {
               std::unique_lock<std::mutex> lock_cv { scheduled.cv.mutex };

               auto const now = std::chrono::system_clock::now();
               auto const duration = future.jobs.size() ? future.jobs.begin()->first.m_wanted_at - now : std::chrono::microseconds(m_sleep_duration);

               if (duration.count() > 0) {
                  lock_waiting.unlock();
                  lock.unlock();
                  scheduled.cv.cv.wait_for(lock_cv, duration);
               }
            } else {
               std::unique_lock<std::mutex> lock_cv { scheduled.cv.mutex };

               lock_waiting.unlock();
               lock.unlock();
               scheduled.cv.cv.wait_for(lock_cv, std::chrono::microseconds(m_sleep_duration));
            }
         }
      } else {
         lock.unlock();
         std::lock_guard<std::mutex> const lock { scheduled.cv.mutex };
         scheduled.cv.cv.notify_all();
      }
   }

   return m_status;
}

void WorkerPoll::add_future() {
   std::lock_guard<std::shared_mutex> const lock_future { future.mutex };
   if (future.jobs.size()) {
      auto const upper_bound = (m_status == KILLING ? future.jobs.end() : future.jobs.upper_bound(std::chrono::system_clock::now()));

      {
         std::lock_guard<std::shared_mutex> const lock_waiting { waiting.mutex };
         waiting.jobs.insert(future.jobs.begin(), upper_bound);
      }

      future.jobs.erase(future.jobs.begin(), upper_bound);
   }
}

void WorkerPoll::add_waiting() {
   assert(!scheduled.mutex.try_lock());
   std::lock_guard<std::shared_mutex> const lock_waiting { waiting.mutex };

   auto it = waiting.jobs.begin();
   for (;(it != waiting.jobs.end()) &&
         ((m_status == KILLING) || (scheduled.jobs.size() < m_workers.size()));
        ++it)
      scheduled.jobs.emplace_back(it->second);

   waiting.jobs.erase(waiting.jobs.begin(), it);
}

void Worker::run() const {
   SignalHandler::get().setAltStack();
   SignalHandler::get().local.insert(SignalHandler::SigSegv, [](siginfo_t const& siginfo, ucontext_t& ucontext) {
         if (context_t::current_context()) {
            context_t::current_context()->panic_mode(siginfo.si_addr, ucontext);
         } else {
            std::string thread_name;
            thread_name.reserve(16);
            if (int const ret = pthread_getname_np(pthread_self(), thread_name.data(), 16); ret) {
               llog::error << "error retrieving thread name: " << strerror(ret) << std::endl;
            }
            llog::error << "SEGFAULT on worker [" << thread_name << + "] !!!!" << std::endl;
            std::terminate();
         }
      });

   while (m_worker_poll.update() != WorkerPoll::DONE) {
      std::shared_ptr<Job> job = { nullptr };
      std::size_t index {};
      {
         std::shared_lock<std::shared_mutex> const lock { m_worker_poll.scheduled.mutex };

         ++m_worker_poll.scheduled.processing;
         index = m_worker_poll.scheduled.index++;
         if (index < m_worker_poll.scheduled.jobs.size())
            m_worker_poll.scheduled.jobs.at(index).swap(job);
         else {
            --m_worker_poll.scheduled.processing;
            continue;
         }

         assert(m_worker_poll.m_status != WorkerPoll::DONE);
      }

      assert(job);
      assert(!job->terminated());

      auto const thread_name = "job ";
      pthread_setname_np(pthread_self(), thread_name);

      auto& scheduler = Scheduler::get();
      auto const now = std::chrono::system_clock::now();

      {
         SCOPE_EXIT([&]() {
            if (job->m_attached_job_poll && --job->m_attached_job_poll->size == 0)
               scheduler << op::resume_t { job } << flush;

            job = nullptr;
            --m_worker_poll.scheduled.processing;
         });

         if (m_worker_poll.m_status == WorkerPoll::KILLING) {
            ++m_worker_poll.scheduled.unterminated;
            job->terminate();
            continue;
         }

         auto const deadline = job->priority().m_wanted_before + (job->priority().m_wanted_before == job->priority().m_wanted_at ? job->priority().m_timeout : duration_t {});
         if (job->is_new() && deadline != job->priority().m_wanted_at && deadline < now) {
            bool const drop = job->priority().m_timeout != duration_t {} &&
                  (job->priority().m_timeout + job->priority().m_wanted_before < now);

            auto const tag = drop ? llog::tagger_t { "drop", llog::Red } : llog::tagger_t { "run", llog::Yellow };
            (drop ? llog::warning << llog::fgd_red :
                    llog::warning << llog::fgd_yellow) <<
                  "Late job ! [" << job->name() << "]"
                  "(" << std::chrono::duration_cast<std::chrono::milliseconds>(now - job->priority().m_wanted_at).count() << "ms - " << std::chrono::duration_cast<std::chrono::milliseconds>(now - job->priority().m_wanted_before).count() << "ms)" << std::endl;

            if (drop) {
               job->terminate();
               continue;
            }
         }

         job->m_worker_name = m_name;
         assert(!job->terminated());
         job->run();

         if (job->m_do_resume) {
            assert(!job->terminated());
            m_worker_poll.addJob(job);
         }
      }
   }

   assert(!m_worker_poll.scheduled.jobs.size());
   assert(!m_worker_poll.future.jobs.size());
   assert(!m_worker_poll.waiting.jobs.size());
   assert(!m_worker_poll.scheduled.processing);
}

Worker::Worker(WorkerPoll& poll) :
   m_worker_poll(poll),
   m_thread([&](){
      run();
   }) {
}

std::string const& Worker::name() {
   return m_name;
}

Worker::~Worker() {
   wait_stop();
}

void Worker::wait_stop() const {
   if (m_thread.joinable())
      const_cast<std::thread&>(m_thread).join();
}

std::atomic<std::size_t> Worker::m_next_id { 0 };

/*###################*/} // namespace job      /*###################*/
