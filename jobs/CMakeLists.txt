file(GLOB_RECURSE SRCS *.cpp)
add_library(jobs STATIC ${SRCS})

target_link_libraries(jobs 
							sighandler 
							context 
							utils 
							-lpthread)

target_compile_options(jobs PUBLIC "$<$<CONFIG:DEBUG>:-DDEBUG>" -std=c++2a
		-export-dynamic
		-fconcepts 
		-Wall -Wextra -Wcast-align -Waddress-of-packed-member
		-ftemplate-backtrace-limit=0
		-ggdb3 -pg
		"$<$<CONFIG:Release>:-O3>"
		"$<$<CONFIG:Debug>:-O0>"
		-D_GNU_SOURCE
		)
		
		
if(DEFINED SANITIZE)
	target_compile_options(jobs PUBLIC 
	        -fsanitize=${SANITIZE}
		    )
endif(DEFINED SANITIZE)
if(DEFINED ADDRESS_SANITIZER)
	target_compile_options(jobs PUBLIC 
		    "-DADDRESS_SANITIZER"
		    -fsanitize-recover=address
		    )
endif(DEFINED ADDRESS_SANITIZER)
