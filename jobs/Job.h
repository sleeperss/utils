#pragma once

#include "jobs.h"
#include "Scheduler.h"

#include <context/context.hpp>

#include <atomic>
#include <cstddef>
#include <exception>
#include <functional>
#include <memory>
#include <string>
#include <thread>

/*####################*/namespace job        {/*###################*/

struct exception {
};

struct sigkill : exception {
};

struct sigdrop : exception {
   std::size_t m_nb;

   sigdrop(std::size_t const nb);
};

class Job : private sharable<Job> {
private:
   priority_t m_priority;
   type_t m_type;
   std::string m_name;

   bool m_do_resume { false };
   bool m_new { true };

   static std::atomic<std::size_t> m_next_id;

   std::shared_ptr<job_poll_t> m_attached_job_poll { nullptr };
   Scheduler* m_scheduler { nullptr };
   std::string m_worker_name;
   std::shared_ptr<context_t> m_context;
   std::function<void(context_t&)> const m_run;

private:
   Job(std::function<void()> const&& job, priority_t const& priority, type_t const& type, std::size_t stack_size = 35 << 10,
         std::string const& name = "any_" + std::to_string(++m_next_id));

   Job(Job const&) = delete;
   Job& operator=(Job const&) = delete;

   using sharable_type_t::create;

public:
   virtual ~Job();

   std::string const& name() const;
   type_t const& type() const;
   priority_t const& priority() const;
   bool is_new() const;
   bool terminated() const;

   void run();
   void terminate();
   void detach();
   void set();

   static std::size_t uid();

private:
   void perform(const std::function<void()>&);

   friend class Scheduler;
   friend class Worker;
   friend sharable_type_t;
};

/*###################*/} // namespace job      /*###################*/
