#pragma once

#include <context/context.hpp>

#include <chrono>
#include <cstddef>
#include <functional>
#include <memory>
#include <mutex>
#include <shared_mutex>
#include <string>

/*####################*/namespace job {/*###################*/

using time_point_t = std::chrono::system_clock::time_point;
using duration_t = std::chrono::system_clock::duration;
using worker_hash_t = std::size_t;

using type_t = std::size_t;

class WorkerPoll;
class Scheduler;
class Job;
struct job_poll_t;

/*####################*/namespace op         {/*###################*/

class operation_t {
public:
   operation_t() = default;
   virtual Scheduler& operator() (Scheduler&) const = 0;

   virtual ~operation_t() = default;

   friend class flush_t;
};

/*###################*/}// namespace op       /*###################*/

using operation_t = std::unique_ptr<op::operation_t>;

extern operation_t const reset;
extern operation_t const flush;

extern operation_t const candrop;
extern operation_t const nodrop;

extern operation_t const attached;
extern operation_t const detached;

extern operation_t const urgent_priority;
extern operation_t const default_priority;

// -----------------------------------------------------------------

struct priority_t {
   time_point_t m_wanted_at;
   time_point_t m_wanted_before;
   duration_t m_timeout;

   static constexpr std::chrono::time_point<std::chrono::system_clock> nulltime {};

   priority_t(time_point_t const& wanted_at = nulltime,
         time_point_t const& wanted_before = nulltime,
         duration_t const& timeout = std::chrono::seconds { 1 });

   struct comparaison {
      struct future {
         constexpr bool operator()(const priority_t& is_lower, const priority_t& is_greater) const {
            return is_lower.m_wanted_at < is_greater.m_wanted_at;
         }
      };

      constexpr bool operator()(const priority_t& is_lower, const priority_t& is_greater) const {
         return is_lower.m_wanted_before < is_greater.m_wanted_before;
      }
   };

   operator bool() const;
   void set();
};

// -----------------------------------------------------------------

/*####################*/namespace op         {/*###################*/

class type_t : public operation_t {
private:
   job::type_t const m_type;

public:
   type_t(job::type_t const);
   virtual Scheduler& operator() (Scheduler&) const final override;

   virtual ~type_t() = default;
};

class priority_t : public operation_t {
private:
   job::priority_t const m_priority;

public:
   priority_t(job::priority_t const&&);
   virtual Scheduler& operator() (Scheduler&) const final override;

   virtual ~priority_t() = default;
};

class stack_size_t : public operation_t {
private:
   std::size_t const m_stack_size;

public:
   stack_size_t(std::size_t);
   virtual Scheduler& operator() (Scheduler&) const final override;

   virtual ~stack_size_t() = default;
};

class job_t : public operation_t {
private:
   std::function<void()> const m_job;

public:
   job_t(std::function<void()> const&&);
   job_t(std::function<void()> const&);
   virtual Scheduler& operator() (Scheduler&) const final override;

   virtual ~job_t() = default;
};

class resume_t : public operation_t {
private:
   std::shared_ptr<Job> const m_job;

public:
   resume_t(std::shared_ptr<Job> const&);
   virtual Scheduler& operator() (Scheduler&) const final override;

   virtual ~resume_t() = default;
};

class name_t : public operation_t {
private:
   std::string const m_name;

public:
   name_t(std::string const&);
   virtual Scheduler& operator() (Scheduler&) const final override;

   virtual ~name_t() = default;
};

/*###################*/}// namespace op       /*###################*/
/*###################*/} // namespace job      /*###################*/

