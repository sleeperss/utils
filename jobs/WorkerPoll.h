#pragma once

#include "jobs.h"
#include "Job.h"

#include <atomic>
#include <cstddef>
#include <map>
#include <memory>
#include <mutex>
#include <shared_mutex>
#include <string>
#include <thread>
#include <vector>

/*####################*/namespace job {/*###################*/

class WorkerPoll;

class Worker {
private:
   WorkerPoll& m_worker_poll;
   std::thread m_thread;
   static std::atomic<std::size_t> m_next_id;
   std::string m_name { "t" + std::to_string(++Worker::m_next_id) };
   
   std::string const& name();
   void run() const;

public:
   Worker(WorkerPoll& poll);
   virtual ~Worker();

   void wait_stop() const;
};

class WorkerPoll {
private:
   static WorkerPoll instance;
   enum { RUNNING, KILLING, DONE } m_status { RUNNING };
   std::size_t const m_sleep_duration;

   struct {
      using jobs_t = std::multimap<priority_t, std::shared_ptr<Job>, priority_t::comparaison::future>;
      std::shared_mutex mutex;
      jobs_t jobs { };
   } future { };

   struct {
      using jobs_t = std::multimap<priority_t, std::shared_ptr<Job>, priority_t::comparaison>;
      std::shared_mutex mutex;
      jobs_t jobs { };
   } waiting { };

   struct {
      using jobs_t = std::vector<std::shared_ptr<Job>>;

      std::shared_mutex mutex { };
      jobs_t jobs { };

      std::atomic<std::size_t> index { 0 };
      std::atomic<std::size_t> processing { 0 };
      struct {
         std::mutex mutex {};
         std::condition_variable cv {};
      } cv;
      std::atomic<std::size_t> unterminated { 0 };
   } scheduled { };

   std::vector<std::unique_ptr<Worker>> const m_workers;

   void addJob(const std::shared_ptr<Job>&);
   auto update();
   void add_future();
   void add_waiting();

public:
   WorkerPoll(std::size_t const nb_jobs = 1, std::size_t const sleep_duration = 100'000);
   ~WorkerPoll();

   void stop();

   friend class Scheduler;
   friend class Worker;
};

/*###################*/} // namespace job      /*###################*/
