#include "Job.h"
#include "Scheduler.h"

#include <context/context.inl>
#include <logger/logger.h>

#include <utils/utils.inl>
#include <atomic>
#include <cstddef>
#include <functional>
#include <ostream>
#include <string.h>
#include <string>
#include <sstream>
#include <thread>
#include <utility>


/*####################*/namespace job        {/*###################*/

Job::Job(std::function<void()> const&& job, priority_t const& priority, type_t const& type, std::size_t const stack_size, std::string const& name) :
   m_priority(priority),
   m_type(type),
   m_name(name),
   m_context { context_t::create(
         (stack_size
#ifdef ADDRESS_SANITIZER
         * 4 + 2) / 3
#else
         )
#endif
         , [](context_t&){ }) },
   m_run { [&, run = std::move(job)] (context_t&) -> void {
      try {
         m_scheduler = &Scheduler::get();
         m_scheduler->m_current_job = weak();
         m_scheduler->m_job_name = m_name;

         run();
      } catch (const sigkill&) {
      } catch (const sigdrop&) {
      }
   }} {
}

Job::~Job() {
}

std::string const& Job::name() const {
   return m_name;
}

type_t const& Job::type() const {
   return m_type;
}

priority_t const& Job::priority() const {
   return m_priority;
}

bool Job::is_new() const {
   return m_new;
}

bool Job::terminated() const {
   return !m_new && m_context->terminated();
}

void Job::perform(const std::function<void()>& action) {
#ifdef DEBUG
   std::string thread_name;
   thread_name.reserve(16);
   if (int const ret = pthread_getname_np(pthread_self(), thread_name.data(), 16); ret) {
      llog::error << "error retrieving thread name: " << strerror(ret) << std::endl;
      std::terminate();
   }

   thread_name = std::string(thread_name.c_str());
   thread_name += m_name;
   assert(thread_name.size() < 16);
   if (int const ret = pthread_setname_np(pthread_self(), thread_name.c_str()); ret) {
      llog::error << "error setting thread name \"" << thread_name << "\": " << strerror(ret) << std::endl;
      std::terminate();
   }

   SCOPE_EXIT([&]() {
      thread_name.resize(thread_name.size() - m_name.size());
      if (int const ret = pthread_setname_np(pthread_self(), thread_name.c_str()); ret) {
         llog::error << "error setting thread name \"" << thread_name << "\": " << strerror(ret) << std::endl;
         std::terminate();
      }
   });
#endif

   std::function<void()> const preform_ = [&]() {
      try {
            assert(!terminated());
            action();
         } catch (const stack_overflow& e) {
            std::stringstream ss {};

            auto const bp = (byte(e.context.getRbp()) - byte(e.context.getRsp()));
            auto const top = (byte(e.context.getTop()) - byte(e.context.getRsp()));
            auto const btm = (byte(e.context.getBottom()) - byte(e.context.getRsp()));
            auto const flt = (byte(e.faulty) - byte(e.context.getRsp()));

            llog::info
            << llog::tag_t { "stackoverflow", llog::LightCyan }
            << llog::tag_t { "sp:" + (ss << e.context.getRsp(), ss.str()), llog::Cyan }
            << llog::tag_t { "bp:" + (ss.str(""), ss << (bp < 0 ? "-" : "+") << std::hex << std::abs(bp), ss.str()), llog::Cyan } << llog::stl_rst
            << llog::tag_t { "top:" + (ss.str(""), ss << (top < 0 ? "-" : "+") << std::hex << std::abs(top), ss.str()), llog::Cyan } << llog::stl_rst
            << llog::tag_t { "btm:" + (ss.str(""), ss << (btm < 0 ? "-" : "+") << std::hex << std::abs(btm), ss.str()), llog::Cyan } << llog::stl_rst
            << llog::tag_t { "flt:" + (ss.str(""), ss << (flt < 0 ? "-" : "+") << std::hex << std::abs(flt), ss.str()), llog::Cyan } << llog::stl_rst
            <<     "extending from " << byte_str(e.context.get_old_size()) << " to " << llog::fgd_light_yellow << byte_str(e.context.get_size()) << std::endl;
            preform_();
         }
   };
   preform_();
}

void Job::terminate() {
   if (m_new) {
      m_new = false;
      assert(terminated());
      return;
   }
   perform([&]() {
      m_context->throw_inside(sigkill {});
      assert(terminated());
   });
}

void Job::run() {
   m_do_resume = false;
   perform([&]() {
         if (m_attached_job_poll && m_attached_job_poll->unterminated_number) {
            m_context->throw_inside(sigdrop { m_attached_job_poll->unterminated_number });
            assert(terminated());
         } else if (m_new) {
            m_new = false;
            m_context->start(std::move(m_run));
         } else
            m_context->attach();
   });
}

void Job::set() {
   m_priority.set();
}

void Job::detach() {
   m_context->detach();
}

std::size_t Job::uid() {
   return ++m_next_id;
}

sigdrop::sigdrop(std::size_t const nb) :
      m_nb(nb) {
}

std::atomic<std::size_t> Job::m_next_id {};

/*###################*/} // namespace job      /*###################*/
