#include "jobs.h"
#include "Scheduler.h"

#include <chrono>
#include <cstddef>

#include <functional>

#include <memory>
#include <string>
#include <utility>

/*####################*/namespace job        {/*###################*/
/*####################*/namespace op         {/*###################*/

#define OPERATION_PROTOTYPE(TYPE)\
class TYPE##_t : public operation_t {\
public:\
   TYPE##_t() = default;\
   virtual Scheduler& operator() (Scheduler&) const final override;\
\
   virtual ~TYPE##_t() = default;\
}

#define OPERATION_IMPL(TYPE)\
Scheduler& TYPE##_t::operator() (Scheduler& scheduler) const {\
   return scheduler.TYPE();\
}

#define OPERATION(TYPE) \
OPERATION_PROTOTYPE(TYPE);\
OPERATION_IMPL(TYPE);//

OPERATION(flush);

OPERATION(candrop);
OPERATION(nodrop);

OPERATION(reset);
OPERATION(attached);
OPERATION(detached);

OPERATION_PROTOTYPE(urgent_priority);
OPERATION_PROTOTYPE(default_priority);

// -----------------------------------------------------------------

type_t::type_t(job::type_t const type) :
   m_type(type) {
}

Scheduler& type_t::operator() (Scheduler& scheduler) const {
   return scheduler.type(m_type);
}

priority_t::priority_t(job::priority_t const&& priority) :
   m_priority(std::move(priority)) {
}

Scheduler& priority_t::operator() (Scheduler& scheduler) const {
   return scheduler.priority(m_priority);
}

stack_size_t::stack_size_t(std::size_t const stack_size) :
   m_stack_size(stack_size) {
}

Scheduler& stack_size_t::operator() (Scheduler& scheduler) const {
   return scheduler.stack_size(m_stack_size);
}

job_t::job_t(std::function<void()> const&& job) :
   m_job(std::move(job)) {
}

job_t::job_t(std::function<void()> const& job) :
   m_job(job) {
}

Scheduler& job_t::operator() (Scheduler& scheduler) const {
   return scheduler.addJob(m_job);
}

resume_t::resume_t(std::shared_ptr<Job> const& job) :
   m_job(job) {
}

Scheduler& resume_t::operator() (Scheduler& scheduler) const {
   return scheduler.resumeJob(m_job);
}

name_t::name_t(std::string const& name) :
   m_name(name) {
}

Scheduler& name_t::operator() (Scheduler& scheduler) const {
   return scheduler.name(m_name);
}

Scheduler& urgent_priority_t::operator() (Scheduler& scheduler) const {
   return scheduler.priority(job::priority_t { std::chrono::system_clock::now(), std::chrono::system_clock::now() });
};

Scheduler& default_priority_t::operator() (Scheduler& scheduler) const {
   return scheduler.priority(job::priority_t { });
};

// -----------------------------------------------------------------

/*###################*/}// namespace op       /*###################*/

// -----------------------------------------------------------------

operation_t const reset = std::make_unique<op::reset_t>();
operation_t const flush = std::make_unique<op::flush_t>();

operation_t const candrop = std::make_unique<op::candrop_t>();
operation_t const nodrop = std::make_unique<op::nodrop_t>();

operation_t const attached = std::make_unique<op::attached_t>();
operation_t const detached = std::make_unique<op::detached_t>();
operation_t const urgent_priority = std::make_unique<op::urgent_priority_t>();
operation_t const default_priority = std::make_unique<op::default_priority_t>();

// -----------------------------------------------------------------

priority_t::priority_t(time_point_t const& wanted_at, time_point_t const& wanted_before, duration_t const& timeout) :
      m_wanted_at(wanted_at),
      m_wanted_before(wanted_before),
      m_timeout(timeout) {
}

void priority_t::set() {
   if (m_wanted_at == nulltime)
      m_wanted_at = std::chrono::system_clock::now();
   
   if (m_wanted_before == nulltime)
      m_wanted_before = m_wanted_at + std::chrono::seconds(1);
}

priority_t::operator bool() const {
   return m_wanted_at != nulltime || m_wanted_before != nulltime;
}

// -----------------------------------------------------------------

/*###################*/}// namespace job      /*###################*/
