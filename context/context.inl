#pragma once

#include "context.hpp"
#include "utils/utils.hpp"

#include <cassert>
#include <cstddef>
#include <cstdlib>
#include <functional>
#include <memory>
#include <string.h>
#include <utility>

template<class ... Args_>
void context_t::start(const std::function<void(context_t&, Args_&...)>& runner, Args_ const&... args) {
   init(&runner, &const_cast<Args_&>(args)...);//const_cast => we will manipulate a copy in the new stack
}

template<class ... Args_>
__attribute__((no_sanitize_address, optimize("O0")))
void context_t::init(const std::function<void(context_t&, Args_&...)>* runner, Args_*... args) {
   assert(terminated());

   label_addr(create___attach_return_addr);
   auto* this_ = this;

   if (callstack_t::m_cache.size()) {
      m_callstack = std::move(callstack_t::m_cache.back());

      /* Allocate the DTV for this thread.  */
      if (_dl_allocate_tls(TLS_TPADJ(m_callstack->m_thread_ptr)) == nullptr) {
         llog::error << "unable to allocate tls (" << strerror(errno) << ")!" << std::endl;
         std::abort();
      }

      callstack_t::m_cache.pop_back();
   } else {
      auto new_size = ++callstack_t::m_cache_size;
      auto peak_size = callstack_t::m_cache_peak_size.load();

      while (new_size > peak_size)
         if (callstack_t::m_cache_peak_size.compare_exchange_strong(peak_size, new_size))
            llog::info << "Peak allocated callstack reached (" << new_size << ")!" << std::endl;
         else
            peak_size = callstack_t::m_cache_peak_size;
      m_callstack = std::make_unique<callstack_t>(m_size);
   }

   m_callstack->m_regs.rsp = m_callstack->m_stack_top;
   get_context(m_current_rsp, m_callstack->m_regs.rbp);

   //***transfer variables
   ((args = m_callstack->set(*args, byte(m_callstack->m_regs.rsp) -= sizeof(*args))),...);
   runner = m_callstack->set(*runner, byte(m_callstack->m_regs.rsp) -= sizeof(*runner));
   //***transfer variables

#ifdef __aarch64__
   m_callstack->m_regs.rsp = reinterpret_cast<void*>(reinterpret_cast<std::size_t>(--addr(m_callstack->m_regs.rsp)) & ~0xF);
#else
   m_callstack->m_regs.rsp = reinterpret_cast<void*>(reinterpret_cast<std::size_t>(addr(m_callstack->m_regs.rsp)) & ~0xF);
   m_caller_stack_rip = addr(m_callstack->m_regs.rsp) - 1;
   m_caller_stack_rbp = addr(m_callstack->m_regs.rsp) - 2;
#endif

   copy_function_epilog_to_context(create___attach_return_addr);
   copy_to_context(this_, runner, args...);

#ifdef __aarch64__
   m_caller_stack_rip = addr(m_callstack->m_regs.rsp) + 1;
   m_caller_stack_rbp = addr(m_callstack->m_regs.rsp);
#endif

   label_addr(create___start_addr);
   m_callstack->m_regs.set(create___start_addr);

   /* --------------------------------------------------------------------------------------------- */
   /* ------------------------------------------ context ------------------------------------------ */
   /* --------------------------------------------------------------------------------------------- */
   /* -- */                                                                                   /* -- */
   /* -- */   this_->m_terminated = false;                                                    /* -- */
   /* -- */   this_->attach();                                                                /* -- */
   /* -- */   asm volatile("create___attach_return_addr:");                                   /* -- */
   /* -- */   fno_dce return;                                                                 /* -- */
   /* -- */                                                                                   /* -- */
   /* -- */   asm volatile("create___start_addr:");                                           /* -- */
   /* -- */   this_->main(*runner, *args...);                                                 /* -- */
   /* -- */   this_->detach();                                                                /* -- */
   /* -- */                                                                                   /* -- */
   /* --------------------------------------------------------------------------------------------- */
   /* ----------------------------------------- //context ----------------------------------------- */
   /* --------------------------------------------------------------------------------------------- */

   std::terminate();
}

extern "C" void __ctype_init();

template<class ... Args_>
__attribute__((noinline))
void context_t::main(const std::function<void(context_t&, Args_&...)>& runner, Args_ const&... args) {
   __ctype_init();

   SCOPE_EXIT([&]() {
      assert(!m_on_detach);
      m_on_detach = std::make_shared<std::function<void(context_t&)>>([&](context_t& context) {
         m_callstack->m_regs = { };
         m_terminated = true;

         destroy(std::move(runner), std::move(args)...);
         callstack_t::m_cache.emplace_back(std::move(context.m_callstack));
      });

      __call_tls_dtors();
   });

   assert(m_callstack->m_allocator);
   allocator_t::set_instance(m_callstack->m_allocator);
   runner(*this, args...);
}

void context_t::throw_inside(const auto& exception) {
   assert(!terminated());
   assert(!attached());

   m_on_attach = std::make_shared<std::function<void(context_t&)>>([&](context_t&) {
      throw exception;
   });

   attach();
}

void context_t::throw_outside(const auto& exception) {
   assert(attached());

   m_on_detach = std::make_shared<std::function<void(context_t&)>>([&](context_t&) {
      throw exception;
   });

   detach();
}

template<class Arg_>
__attribute__((always_inline))
inline Arg_* callstack_t::set(Arg_ const& arg, void* stack_ptr) const {
   assert(stack_ptr >= m_begin && stack_ptr < m_stack_top);
   ::new (reinterpret_cast<Arg_*&>(stack_ptr)) Arg_ { arg };
   return reinterpret_cast<Arg_*>(stack_ptr);
}

template<class Arg_>
__attribute__((always_inline))
inline Arg_* context_t::set(Arg_ const& arg, void* stack_ptr) const {
   assert(stack_ptr >= m_caller_regs.rsp && stack_ptr <= caller_rbp());
   ::new (reinterpret_cast<Arg_*&>(stack_ptr)) Arg_ { arg };
   return reinterpret_cast<Arg_*>(stack_ptr);
}

template<class Type_>
struct destroy_helper;

template<_not_fundamental Type_>
struct destroy_helper<Type_> {
   static void destroy(std::remove_reference_t<Type_>& elem) {
      elem.~Type_();
   }
};

template<_fundamental Type_>
struct destroy_helper<Type_> {
   static void destroy(std::remove_reference_t<Type_>&) {
      // stack variable ==> no destructor
   }
};

template<class... Types_>
__attribute__((always_inline))
inline void context_t::destroy(Types_&& ... elem) const {
   (destroy_helper<Types_>::destroy(std::forward<Types_>(elem)), ...);
}

__attribute__((always_inline))
inline void context_t::get_context(void*& rsp, void*& rbp) { // @suppress("Missing const-qualification")
#ifdef __aarch64__
   asm volatile("mov %[rsp], sp" : [rsp]"=r" (rsp));
   asm volatile("ldr %[rbp], [sp]" : [rbp]"=r" (rbp));
#else
   asm volatile("mov %%rsp, %[rsp]" : [rsp]"=rm" (rsp));
   asm volatile("mov %%rbp, %[rbp]" : [rbp]"=rm" (rbp));
#endif
}

__attribute__((always_inline))
inline void context_t::copy_function_epilog_to_context(void const* const rip) {
#ifdef __aarch64__
   get_context(m_current_rsp, m_current_rbp);

   //mov rsp -> rbp
   m_callstack->m_regs.rbp = m_callstack->m_regs.rsp;

   //set local size
   byte(m_callstack->m_regs.rsp) -= (byte(m_current_rbp) - byte(m_current_rsp));

   //set rip
   m_callstack->set(rip, addr(m_callstack->m_regs.rsp) + 1);
   //set rbp
   m_callstack->set(m_callstack->m_regs.rbp, addr(m_callstack->m_regs.rsp));
#else
   get_context(m_current_rsp, m_current_rbp);

   //push rip
   m_callstack->set(rip, --addr(m_callstack->m_regs.rsp));
   //push rbp
   m_callstack->set(m_callstack->m_regs.rbp, --addr(m_callstack->m_regs.rsp));
   //mov rsp -> rbp
   m_callstack->m_regs.rbp = m_callstack->m_regs.rsp;

   //set local size
   byte(m_callstack->m_regs.rsp) -= (byte(m_current_rbp) - byte(m_current_rsp));
#endif
}

__attribute__((always_inline)) inline void callstack_t::reg_t::set(registry_t const rip) { // @suppress("Missing const-qualification")
   this->rip = rip;

#ifdef __aarch64__
   asm volatile("str x19, [%[regs], #0x00];"
                "str x20, [%[regs], #0x08];"
                "str x21, [%[regs], #0x10];"
                "str x22, [%[regs], #0x18];"
                "str x23, [%[regs], #0x20];"
                "str x24, [%[regs], #0x28];"
                "str x25, [%[regs], #0x30];"
                "str x26, [%[regs], #0x38];"
                "str x27, [%[regs], #0x40];"
                "str x28, [%[regs], #0x48];"
                "str x29, [%[regs], #0x50];"
   :: [regs]"r"(regs.data())
   : "x19", "x20", "x21", "x22", "x23", "x24", "x25", "x26", "x27", "x28", "x29");
#else
   asm volatile("mov %%rsi, 0x00(%[regs]);"
                "mov %%rdi, 0x08(%[regs]);"

//                "mov %%rax, 0x10(%[regs]);"
//                "mov %%rbx, 0x18(%[regs]);"
//                "mov %%rcx, 0x20(%[regs]);"
//                "mov %%rdx, 0x28(%[regs]);"

                "mov %%r8,  0x10(%[regs]);"
                "mov %%r9,  0x18(%[regs]);"
                "mov %%r10, 0x20(%[regs]);"
                "mov %%r11, 0x28(%[regs]);"
                "mov %%r12, 0x30(%[regs]);"
                "mov %%r13, 0x38(%[regs]);"
                "mov %%r14, 0x40(%[regs]);"
                "mov %%r15, 0x48(%[regs]);"
   :: [regs]"rm"(regs.data())
   : "rsi", "rdi",
//     "rax", "rbx", "rcx", "rdx",
     "r8", "r9", "r10", "r11", "r12", "r13", "r14");
#endif
} // rbx r12

__attribute__((always_inline))
inline void callstack_t::reg_t::restore() const {
#ifdef __aarch64__
   asm volatile("ldr x19, [%[regs], #0x00];"
                "ldr x20, [%[regs], #0x08];"
                "ldr x21, [%[regs], #0x10];"
                "ldr x22, [%[regs], #0x18];"
                "ldr x23, [%[regs], #0x20];"
                "ldr x24, [%[regs], #0x28];"
                "ldr x25, [%[regs], #0x30];"
                "ldr x26, [%[regs], #0x38];"
                "ldr x27, [%[regs], #0x40];"
                "ldr x28, [%[regs], #0x48];"
                "ldr x29, [%[regs], #0x50];"
   :: [regs]"r"(regs.data())
   : "x19", "x20", "x21", "x22", "x23", "x24", "x25", "x26", "x27", "x28", "x29");
#else
   asm volatile("mov 0x00(%[regs]), %%rsi;"
                "mov 0x08(%[regs]), %%rdi;"

//                "mov 0x10(%[regs]), %%rax;"
//                "mov 0x18(%[regs]), %%rbx;"
//                "mov 0x20(%[regs]), %%rcx;"
//                "mov 0x28(%[regs]), %%rdx;"

                "mov 0x10(%[regs]), %%r8;"
                "mov 0x18(%[regs]), %%r9;"
                "mov 0x20(%[regs]), %%r10;"
                "mov 0x28(%[regs]), %%r11;"
                "mov 0x30(%[regs]), %%r12;"
                "mov 0x38(%[regs]), %%r13;"
                "mov 0x40(%[regs]), %%r14;"
                "mov 0x48(%[regs]), %%r15;"
   :: [regs]"rm"(regs.data())
   : "rsi", "rdi",
//     "rax", "rbx", "rcx", "rdx",
     "r8", "r9", "r10", "r11", "r12", "r13", "r14");
#endif
}

template<class ... Args_>
__attribute__((always_inline))
inline void context_t::copy_to_context(Args_ const& ... args) const {
   (copy_to_context(args), ...);
}

template<class Args_>
__attribute__((always_inline))
inline void context_t::copy_to_context(Args_ const& arg) const {
   ::new (get_context_addr(arg)) Args_ { arg };
}

template<class Arg_>
__attribute__((always_inline))
inline Arg_* context_t::get_context_addr(Arg_ const& arg) const {
   return reinterpret_cast<Arg_*>(byte(m_callstack->m_regs.rbp) - static_cast<std::size_t>(byte(m_current_rbp) - reinterpret_cast<uint8_t const*>(&arg)));
}

