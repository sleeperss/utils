#pragma once

#include "res_state.h"

#include <limits.h>
#include <pthread.h>
#include <stdint.h>

extern "C" {

#ifdef __aarch64__
#define TLS_TCB_AT_TP 0
#else
#define TLS_TCB_AT_TP 1
#define TLS_MULTIPLE_THREADS_IN_TCB
#endif

#define _STACK_GROWS_DOWN 1
#define _STACK_GROWS_UP 0

/* This yields the pointer that TLS support code calls the thread pointer.  */
#if TLS_TCB_AT_TP
#define TLS_DTV_AT_TP 0
#elif !TLS_TCB_AT_TP
#define TLS_DTV_AT_TP 1
#endif

void __call_tls_dtors();

struct dtv_pointer
{
  void *val;                    /* Pointer to data, or TLS_DTV_UNALLOCATED.  */
  void *to_free;                /* Unaligned pointer, for deallocation.  */
};

/* Type for the dtv.  */
union dtv_t
{
  size_t counter;
  struct dtv_pointer pointer;
};

struct __128bits
{
  int i[4];
};

#ifdef __aarch64__
typedef struct
{
  dtv_t *dtv;
  void *private_;
} tcbhead_t;
#else
struct tcbhead_t
{
  void *tcb;                /* Pointer to the TCB.  Not necessarily the
                           thread descriptor used by libpthread.  */
  dtv_t *dtv;
  void *self;                /* Pointer to the thread descriptor.  */
  int multiple_threads;
  int gscope_flag;
  uintptr_t sysinfo;
  uintptr_t stack_guard;
  uintptr_t pointer_guard;
  unsigned long int vgetcpu_cache[2];
  /* Bit 0: X86_FEATURE_1_IBT.
     Bit 1: X86_FEATURE_1_SHSTK.
   */
  unsigned int feature_1;
  int __glibc_unused1;
  /* Reservation of some values for the TM ABI.  */
  void *__private_tm[4];
  /* GCC split stack support.  */
  void *__private_ss;
  /* The lowest address of shadow stack,  */
  unsigned long long int ssp_base;
  /* Must be kept even if it is no longer used by glibc since programs,
     like AddressSanitizer, depend on the size of tcbhead_t.  */
  __128bits __glibc_unused2[8][4] __attribute__ ((aligned (32)));
  void *__padding[8];
};
#endif

/* Internal: doubly linked lists.  */
/* Basic type for the double-link list.  */
struct list_t
{
  struct list_head *next;
  struct list_head *prev;
};

#define PTHREAD_KEY_2NDLEVEL_SIZE 32

/* We need to address PTHREAD_KEYS_MAX key with PTHREAD_KEY_2NDLEVEL_SIZE
   keys in each subarray.  */
#define PTHREAD_KEY_1STLEVEL_SIZE \
  ((PTHREAD_KEYS_MAX + PTHREAD_KEY_2NDLEVEL_SIZE - 1) \
   / PTHREAD_KEY_2NDLEVEL_SIZE)


/* Events reportable by the thread implementation.  */
enum td_event_e
{
  TD_ALL_EVENTS,                 /* Pseudo-event number.  */
  TD_EVENT_NONE = TD_ALL_EVENTS, /* Depends on context.  */
  TD_READY,                         /* Is executable now. */
  TD_SLEEP,                         /* Blocked in a synchronization obj.  */
  TD_SWITCHTO,                         /* Now assigned to a process.  */
  TD_SWITCHFROM,                 /* Not anymore assigned to a process.  */
  TD_LOCK_TRY,                         /* Trying to get an unavailable lock.  */
  TD_CATCHSIG,                         /* Signal posted to the thread.  */
  TD_IDLE,                         /* Process getting idle.  */
  TD_CREATE,                         /* New thread created.  */
  TD_DEATH,                         /* Thread terminated.  */
  TD_PREEMPT,                         /* Preempted.  */
  TD_PRI_INHERIT,                 /* Inherited elevated priority.  */
  TD_REAP,                         /* Reaped.  */
  TD_CONCURRENCY,                 /* Number of processes changing.  */
  TD_TIMEOUT,                         /* Conditional variable wait timed out.  */
  TD_MIN_EVENT_NUM = TD_READY,
  TD_MAX_EVENT_NUM = TD_TIMEOUT,
  TD_EVENTS_ENABLE = 31                /* Event reporting enabled.  */
};

#define TD_EVENTSIZE        2

/* Bitmask of enabled events. */
struct td_thr_events_t
{
  uint32_t event_bits[TD_EVENTSIZE];
};


/* Data structure used by the kernel to find robust futexes.  */
struct robust_list_head
{
  void *list;
  long int futex_offset;
  void *list_op_pending;
};



/* @@@ The IA-64 ABI uses uint64 throughout.  Most places this is
   inefficient for 32-bit and smaller machines.  */
using _Unwind_Word = unsigned __attribute__((__mode__(__unwind_word__)));

using _Unwind_Sword =  signed __attribute__((__mode__(__unwind_word__)));

#if defined(__ia64__) && defined(__hpux__)
using _Unwind_Ptr = unsigned __attribute__((__mode__(__word__)));
#else
using _Unwind_Ptr = unsigned __attribute__((__mode__(__pointer__)));
#endif

using _Unwind_Internal_Ptr = unsigned __attribute__((__mode__(__pointer__)));
/* @@@ The IA-64 ABI uses a 64-bit word to identify the producer and
   consumer of an exception.  We'll go along with this for now even on
   32-bit machines.  We'll need to provide some other option for
   16-bit machines and for machines with > 8 bits per byte.  */

using _Unwind_Exception_Class =  unsigned __attribute__((__mode__(__DI__)));
/* The unwind interface uses reason codes in several contexts to
   identify the reasons for failures or other actions.  */

enum _Unwind_Reason_Code
{
  _URC_NO_REASON = 0,
  _URC_FOREIGN_EXCEPTION_CAUGHT = 1,
  _URC_FATAL_PHASE2_ERROR = 2,
  _URC_FATAL_PHASE1_ERROR = 3,
  _URC_NORMAL_STOP = 4,
  _URC_END_OF_STACK = 5,
  _URC_HANDLER_FOUND = 6,
  _URC_INSTALL_CONTEXT = 7,
  _URC_CONTINUE_UNWIND = 8
};

/* The unwind interface uses a pointer to an exception header object
   as its representation of an exception being thrown. In general, the
   full representation of an exception object is language- and
   implementation-specific, but it will be prefixed by a header
   understood by the unwind interface.  */
struct _Unwind_Exception;

using _Unwind_Exception_Cleanup_Fn = void (*) (_Unwind_Reason_Code,
                                              struct _Unwind_Exception *);

struct _Unwind_Exception
{
  _Unwind_Exception_Class exception_class;
  _Unwind_Exception_Cleanup_Fn exception_cleanup;
  _Unwind_Word private_1;
  _Unwind_Word private_2;
  /* @@@ The IA-64 ABI says that this structure must be double-word aligned.
     Taking that literally does not make much sense generically.  Instead we
     provide the maximum alignment required by any type for the machine.  */
} __attribute__((__aligned__));

/* Structure containing event data available in each thread structure.  */
struct td_eventbuf_t
{
  td_thr_events_t eventmask;        /* Mask of enabled events.  */
  td_event_e eventnum;                /* Number of last event.  */
  void *eventdata;                /* Data associated with event.  */
};

//#define HP_TIMING_INLINE 1

#define MAXNS                         3        /* max # name servers we'll track */
#define MAXDFLSRCH                    3        /* # default domain levels to try */
#define MAXDNSRCH                     6        /* max # domains in search path */
#define MAXRESOLVSORT                10        /* number of net to sort on */
using hp_timing_t = uint64_t;

#define TCB_ALIGNMENT                64
struct pthread
{
  union
  {

#if TLS_TCB_AT_TP
    /* This overlaps the TCB as used for TLS without threads (see tls.h).  */
    tcbhead_t header;

#else

    struct
    {
      /* multiple_threads is enabled either when the process has spawned at
         least one thread or when a single-threaded process cancels itself.
         This enables additional code to introduce locking before doing some
         compare_and_exchange operations and also enable cancellation points.
         The concepts of multiple threads and cancellation points ideally
         should be separate, since it is not necessary for multiple threads to
         have been created for cancellation points to be enabled, as is the
         case is when single-threaded process cancels itself.
         Since enabling multiple_threads enables additional code in
         cancellation points and compare_and_exchange operations, there is a
         potential for an unneeded performance hit when it is enabled in a
         single-threaded, self-canceling process.  This is OK though, since a
         single-threaded process will enable async cancellation only when it
         looks to cancel itself and is hence going to end anyway.  */
      int multiple_threads;
      int gscope_flag;
    } header;

#endif

    /* This extra padding has no special purpose, and this structure layout
       is private and subject to change without affecting the official ABI.
       We just have it here in case it might be convenient for some
       implementation-specific instrumentation hack or suchlike.  */
    void *__padding[24];
  };

  /* This descriptor's link on the `stack_used' or `__stack_user' list.  */
  list_t list;

  /* Thread ID - which is also a 'is this thread descriptor (and
     therefore stack) used' flag.  */
  pid_t tid;

  /* Ununsed.  */
  pid_t pid_ununsed;

  /* List of robust mutexes the thread is holding.  */
#if __PTHREAD_MUTEX_HAVE_PREV

  void *robust_prev;

  struct robust_list_head robust_head;

  /* The list above is strange.  It is basically a double linked list
     but the pointer to the next/previous element of the list points
     in the middle of the object, the __next element.  Whenever
     casting to __pthread_list_t we need to adjust the pointer
     first.
     These operations are effectively concurrent code in that the thread
     can get killed at any point in time and the kernel takes over.  Thus,
     the __next elements are a kind of concurrent list and we need to
     enforce using compiler barriers that the individual operations happen
     in such a way that the kernel always sees a consistent list.  The
     backward links (ie, the __prev elements) are not used by the kernel.
     FIXME We should use relaxed MO atomic operations here and signal fences
     because this kind of concurrency is similar to synchronizing with a
     signal handler.  */
#else

  union
  {
    __pthread_slist_t robust_list;
    struct robust_list_head robust_head;
  };

#endif

  /* List of cleanup buffers.  */
  struct _pthread_cleanup_buffer *cleanup;

  /* Unwind information.  */
  struct pthread_unwind_buf *cleanup_jmp_buf;

  /* Flags determining processing of cancellation.  */
  int cancelhandling;

  /* Flags.  Including those copied from the thread attribute.  */
  int flags;

  /* We allocate one block of references here.  This should be enough
     to avoid allocating any memory dynamically for most applications.  */
  struct pthread_key_data
  {

    /* Sequence number.  We use uintptr_t to not require padding on
       32- and 64-bit machines.  On 64-bit machines it helps to avoid
       wrapping, too.  */
    uintptr_t seq;

    /* Data pointer.  */
    void *data;
  } specific_1stblock[PTHREAD_KEY_2NDLEVEL_SIZE];

  /* Two-level array for the thread-specific data.  */
  struct pthread_key_data *specific[PTHREAD_KEY_1STLEVEL_SIZE];

  /* Flag which is set when specific data is set.  */
  bool specific_used;

  /* True if events must be reported.  */
  bool report_events;

  /* True if the user provided the stack.  */
  bool user_stack;

  /* True if thread must stop at startup time.  */
  bool stopped_start;

  /* The parent's cancel handling at the time of the pthread_create
     call.  This might be needed to undo the effects of a cancellation.  */
  int parent_cancelhandling;

  /* Lock to synchronize access to the descriptor.  */
  int lock;

  /* Lock for synchronizing setxid calls.  */
  unsigned int setxid_futex;

#if HP_TIMING_INLINE
  hp_timing_t cpuclock_offset_ununsed;
#endif

  /* If the thread waits to join another one the ID of the latter is
     stored here.
     In case a thread is detached this field contains a pointer of the
     TCB if the thread itself.  This is something which cannot happen
     in normal operation.  */
  struct pthread *joinid;

  /* The result of the thread function.  */
  void *result;

  /* Scheduling parameters for the new thread.  */
  struct sched_param schedparam;

  int schedpolicy;
  /* Start position of the code to be executed and the argument passed
     to the function.  */

  void *(*start_routine) (void *);

  void *arg;

  /* Debug state.  */
  td_eventbuf_t eventbuf;

  /* Next descriptor with a pending event.  */
  struct pthread *nextevent;

  /* Machine-specific unwind info.  */
  struct _Unwind_Exception exc;

  /* If nonzero, pointer to the area allocated for the stack and guard. */
  void *stackblock;

  /* Size of the stackblock area including the guard.  */
  size_t stackblock_size;

  /* Size of the included guard area.  */
  size_t guardsize;

  /* This is what the user specified and what we will report.  */
  size_t reported_guardsize;

  /* Thread Priority Protection data.  */
  struct priority_protection_data *tpp;

  /* Resolver state.  */
  struct __res_state res;

  /* Indicates whether is a C11 thread created by thrd_creat.  */
  bool c11;

  /* This member must be last.  */
  char end_padding[];

} __attribute ((aligned (TCB_ALIGNMENT)));

extern "C" {
void* _dl_allocate_tls (void *mem);
}

struct pthread_attr {

  /* Scheduler parameters and priority.  */
  struct sched_param schedparam;

  int schedpolicy;

  /* Various flags like detachstate, scope, etc.  */
  int flags;

  /* Size of guard area.  */
  size_t guardsize;

  /* Stack handling.  */
  void *stackaddr;

  size_t stacksize;

  /* Affinity map.  */
  cpu_set_t *cpuset;

  size_t cpusetsize;

};

#ifdef __aarch64__
/* This is the size of the initial TCB.  */
# define TLS_INIT_TCB_SIZE        sizeof (tcbhead_t)
/* Alignment requirements for the initial TCB.  */
# define TLS_INIT_TCB_ALIGN        __alignof__ (struct pthread)
/* This is the size of the TCB.  */
# define TLS_TCB_SIZE                sizeof (tcbhead_t)
/* This is the size we need before TCB.  */
# define TLS_PRE_TCB_SIZE        sizeof (struct pthread)
/* Alignment requirements for the TCB.  */
# define TLS_TCB_ALIGN                __alignof__ (struct pthread)
#else
/* This is the size of the initial TCB.  Can't be just sizeof (tcbhead_t),
   because NPTL getpid, __libc_alloca_cutoff etc. need (almost) the whole
   struct pthread even when not linked with -lpthread.  */
#define TLS_INIT_TCB_SIZE sizeof(struct pthread)
/* Alignment requirements for the initial TCB.  */
#define TLS_INIT_TCB_ALIGN __alignof__(struct pthread)
/* This is the size of the TCB.  */
#define TLS_TCB_SIZE sizeof(struct pthread)
/* Alignment requirements for the TCB.  */
#define TLS_TCB_ALIGN __alignof__(struct pthread)
#endif

/* This yields the pointer that TLS support code calls the thread pointer.  */
#if TLS_TCB_AT_TP
#define TLS_TPADJ(pd) (reinterpret_cast<struct pthread *>(pd))
#define TP_TLSADJ(pd) (reinterpret_cast<struct pthread *>(pd))
#define CTLS_TPADJ(pd) (reinterpret_cast<struct pthread const *>(pd))
#define CTP_TLSADJ(pd) (reinterpret_cast<struct pthread const *>(pd))
#else
# define TLS_TPADJ(pd) (reinterpret_cast<struct pthread*>(reinterpret_cast<char*>(pd) + TLS_PRE_TCB_SIZE + 0x100))
# define TP_TLSADJ(pd) (reinterpret_cast<struct pthread*>(reinterpret_cast<char*>(pd) - TLS_PRE_TCB_SIZE - 0x100))
# define CTLS_TPADJ(pd) (reinterpret_cast<struct pthread const*>(reinterpret_cast<char const*>(pd) + TLS_PRE_TCB_SIZE + 0x100))
# define CTP_TLSADJ(pd) (reinterpret_cast<struct pthread const*>(reinterpret_cast<char const*>(pd) - TLS_PRE_TCB_SIZE - 0x100))
#endif

#define ATTR_FLAG_STACKADDR                0x0008

void __deallocate_stack(struct pthread* pd);
int allocate_stack(const struct pthread_attr* attr, struct pthread** pdp, void **stack);

}

#define CLOBBERED_BY_SYSCALL "memory", "rcx", "r11", "cc"
#define SYS_x64_getsetfsgs 158

void* sys_x64_get_fs();
int64_t sys_x64_set_fs(void const* const value);
