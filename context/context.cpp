#include "context.hpp"
#include "context.inl"
#include "pthread_.h"

#include <utils/utils.inl>
#include <logger/logger.h>
#include <allocator/allocator.h>

#include <algorithm>
#include <array>
#include <atomic>
#include <cmath>
#include <cstddef>
#include <exception>
#include <filesystem>
#include <functional>
#include <memory>
#include <new>
#include <ostream>
#include <regex>
#include <stdexcept>
#include <string>
#include <string.h>
#include <sys/mman.h>
#include <execinfo.h>
#include <ios>
#include <sstream>
#include <ucontext.h>
#include <vector>

callstack_t::callstack_t(std::size_t const size) {
   m_allocator = allocator_t::get_instance();
   realloc(size);

   pthread_attr attr {};
   attr.stacksize = size;
   attr.stackaddr = m_end;
   attr.flags = ATTR_FLAG_STACKADDR;

   const int error = allocate_stack(&attr, &m_thread_ptr, &m_stack_top);
   assert(!error);

#if TLS_TCB_AT_TP//todo cached
   /* Reference to the TCB itself.  */
   m_thread_ptr->header.self = m_thread_ptr;
   /* Self-reference for TLS.  */
   m_thread_ptr->header.tcb = m_thread_ptr;
#endif

}

callstack_t::~callstack_t() {
   if (m_thread_ptr)
      __deallocate_stack(m_thread_ptr);

   if (m_allocated_ptr) {
      unprotect();
      auto const allocator = allocator_t::get_instance();
      SCOPE_EXIT([&]() {
         allocator_t::set_instance(allocator);
      });

      m_allocator->free(m_allocated_ptr);
   }
}

void callstack_t::unprotect() {
   if (mprotect(byte(m_begin) - protect_size, protect_size, PROT_READ | PROT_WRITE)) {
      llog::error << "[callstack_t] unable to unprotect memory (" << errno << "): " << std::string(strerror(errno)) << std::endl;
      throw std::bad_alloc();
   }
}

void callstack_t::protect() {
   if (mprotect(byte(m_begin) - protect_size, protect_size, PROT_NONE)) {
      llog::error << "[callstack_t] unable to protect memory (" << errno << "): " << std::string(strerror(errno)) << std::endl;
      throw std::bad_alloc();
   }
}

void callstack_t::extends(std::size_t overflow_size) {
   realloc(std::max(m_size * 2, m_size + 5 * overflow_size));
}

__attribute__((no_sanitize_address))
void callstack_t::realloc(std::size_t const size_) {
   m_old_size = m_size;
   m_size = static_cast<std::size_t>(std::ceil((static_cast<float>(size_)) / page_size) * page_size);
   assert(m_size > m_old_size);
   assert(!m_old_size || m_begin);

   void* const old_end = byte(m_end);

   {
      auto const allocator = allocator_t::get_instance();
      SCOPE_EXIT([&]() {
         allocator_t::set_instance(allocator);
      });
      allocator_t::set_instance(m_allocator);

      if (m_old_size) {
         unprotect();
         m_allocated_ptr = m_allocator->lextend(m_allocated_ptr, m_size + protect_size + page_size);
      } else
         m_allocated_ptr = m_allocator->alloc(m_size + protect_size + page_size);
   }

   m_begin = byte(((reinterpret_cast<std::size_t>(m_allocated_ptr) + page_size - 1) / page_size) * page_size);
   m_end = byte(m_begin) + protect_size + m_size;

   assert(!m_old_size || byte(m_end) == byte(old_end));

   byte(m_begin) += protect_size;
   protect();
}

const std::size_t callstack_t::page_size = sysconf(_SC_PAGESIZE);
const std::size_t callstack_t::protect_size = std::ceil(
   static_cast<float>(std::max(callstack_t::page_size, 8'192ul)) / callstack_t::page_size) * callstack_t::page_size;

context_t::context_t(std::size_t size, const std::function<void(context_t&)>& on_unterminated_free) : // @suppress("Missing const-qualification")
      m_size { size }, m_on_unterminated_free(on_unterminated_free) {
}

context_t::~context_t() {
   if (!terminated()) {
      try {
         throw_inside(unterminated_context {});
      } catch (...) {
      }

      m_on_unterminated_free(*this);
   }

   assert(!m_callstack);
}
void * __builtin_thread_pointer();
__attribute__((noinline, no_sanitize_address, optimize("O0")))
void context_t::attach() {
   assert(!m_attached);

   if (terminated())
      throw std::runtime_error("trying to attach to a terminated context...");

   m_attached = true;
   m_on_detach = nullptr;

   m_current_context = this;

   label_addr(detach__return_addr);
   label_addr(end_context__return_addr);

   {
      m_callstack->m_caller_thptr = TP_TLSADJ(sys_x64_get_fs());

      if (!m_callstack->m_caller_thptr) {
         llog::error << "could not get tp" << std::endl;
         std::terminate();
      }

      m_callstack->m_thread_ptr->tid = m_callstack->m_caller_thptr->tid;

      if (sys_x64_set_fs(TLS_TPADJ(m_callstack->m_thread_ptr))) {
         llog::error << "could not set thread pointer" << std::endl;
         std::terminate();
      }
   }

   m_caller_regs.set(detach__return_addr);
   get_context(m_caller_regs.rsp, m_caller_regs.rbp);
   m_caller_stack_rip[0] = end_context__return_addr;
   #ifdef __aarch64__
   m_caller_stack_rbp[0] = m_caller_regs.rsp;
   asm volatile("mov sp, %[rsp];"
             "mov " fp ", %[rsp];"
             "mov " lr ", %[rip];"
             "br " lr ";"
   :: [rsp]"rm"(m_callstack->m_regs.rsp),
      [rip]"rm"(m_callstack->m_regs.rip)
   : fp);
   #else
   m_caller_stack_rbp[0] = m_caller_regs.rbp;
   asm volatile("mov %[rsp], %%rsp;"
                "mov %[rbp], %%rbp;"
                "jmp *%[rip];"
   :: [rsp]"rm"(m_callstack->m_regs.rsp),
      [rbp]"rm"(m_callstack->m_regs.rbp),
      [rip]"rm"(m_callstack->m_regs.rip));
   #endif

   fno_dce std::terminate();
   try {
      fno_dce throw std::runtime_error(std::string("hum, we shouldn't be there ") + __FILE__ + " [" + std::to_string(__LINE__) + "]...");
      asm volatile("end_context__return_addr:");
      fno_dce return;
   } catch(...) {
#ifdef __aarch64__
      asm volatile("mov sp, " fp ";");
#endif
      assert(attached());
      m_on_detach = std::make_shared<std::function<void(context_t&)>>([exception = std::current_exception()](context_t&) {
         throw exception;
      });
      throw;
   }

   assert(m_on_detach);
   detach();

   asm volatile("detach__return_addr:");
   m_caller_regs.restore();
   {
      assert(m_callstack->m_caller_thptr);
      if (sys_x64_set_fs(CTLS_TPADJ(m_callstack->m_caller_thptr))) {
         llog::error << "could not revert thread pointer" << std::endl;
         std::terminate();
      }
      m_callstack->m_caller_thptr = nullptr;
   }

   if (m_on_detach)
      (*m_on_detach)(*this);
}

__attribute__((noinline, no_sanitize_address, optimize("O0")))
void context_t::detach() {
   assert(attached());

   m_attached = false;
   m_on_attach = nullptr;

   label_addr(attach__return_addr);
   m_callstack->m_regs.set(attach__return_addr);
   get_context(m_callstack->m_regs.rsp, m_callstack->m_regs.rbp);
   #ifdef __aarch64__
      asm volatile("mov sp, %[caller_rsp];"
                "mov " fp ", %[caller_rsp];"
                "br %[caller_rip];"
      :: [caller_rsp]"r" (m_caller_regs.rsp),
         [caller_rip]"r" (caller_rip())
      : fp);
   #else
      asm volatile("mov %[caller_rsp], %%rsp;"
                "mov %[caller_rbp], %%rbp;"
                "jmp *%[caller_rip];"
      :: [caller_rsp]"rm" (m_caller_regs.rsp),
         [caller_rbp]"rm" (caller_rbp()),
         [caller_rip]"rm" (caller_rip()));
   #endif

   asm volatile("attach__return_addr:");
   m_callstack->m_regs.restore();

   if (m_on_attach)
      (*m_on_attach)(*this);
}

bool context_t::attached() const {
   return m_attached;
}

bool context_t::terminated() const {
   return m_terminated;
}

void const* context_t::getRsp() const {
   return m_callstack->m_regs.rsp;
}

void const* context_t::getRbp() const {
   return m_callstack->m_regs.rbp;
}

void const* context_t::getTop() const {
   return m_callstack->m_stack_top;
}

void const* context_t::getBottom() const {
   return m_callstack->m_begin;
}

std::shared_ptr<context_t> context_t::current_context() {
   return m_current_context->lock();//fixme valgrind more than 7 threads ??
}

void context_t::panic_mode(void* faulty, ucontext_t& ucontext) {
#ifdef __aarch64__
   void*& seg_rbp = ptr(ucontext.uc_mcontext.regs[29]);
//   void*& seg_rip = ptr(ucontext.uc_mcontext.pc);
   void*& seg_rsp = ptr(ucontext.uc_mcontext.sp);
#else
   void*& seg_rbp = ptr(ucontext.uc_mcontext.gregs[REG_RBP]);
//   void*& seg_rip = ptr(ucontext.uc_mcontext.gregs[REG_RIP]);
   void*& seg_rsp = ptr(ucontext.uc_mcontext.gregs[REG_RSP]);
#endif

   if ((byte(m_callstack->m_begin) - m_callstack->protect_size <= faulty) &&
       (faulty < m_callstack->m_begin)) {
      // inside safezone

      m_callstack->m_regs.rsp = seg_rsp;
      m_callstack->m_regs.rbp = seg_rbp;
      m_callstack->extends(byte(m_callstack->m_begin) - byte(faulty));

      throw_outside(stack_overflow { *this, faulty });
      return;
   }
   
   std::vector<void*> backtrace_array {};
   backtrace_array.resize(45);
   backtrace_array.resize(backtrace(backtrace_array.data(), backtrace_array.size()));

   std::stringstream ss {};

   auto const bp = (byte(m_callstack->m_regs.rbp) - byte(m_callstack->m_regs.rsp));
   auto const top = (byte(m_callstack->m_stack_top) - byte(m_callstack->m_regs.rsp));
   auto const btm = (byte(m_callstack->m_begin) - byte(m_callstack->m_regs.rsp));
   auto const flt = (byte(faulty) - byte(m_callstack->m_regs.rsp));

   llog::error << std::flush
      << llog::tag_t { "segfault", llog::Red }
      << llog::tag_t { "sp:" + (ss << m_callstack->m_regs.rsp, ss.str()), llog::Red }
      << llog::tag_t { "bp:" + (ss.str(""), ss << (bp < 0 ? "-" : "+")   << std::hex << std::abs(bp), ss.str()),  llog::Red } << llog::stl_rst
      << llog::tag_t { "top:" + (ss.str(""), ss << (top < 0 ? "-" : "+") << std::hex << std::abs(top), ss.str()), llog::Red } << llog::stl_rst
      << llog::tag_t { "btm:" + (ss.str(""), ss << (btm < 0 ? "-" : "+") << std::hex << std::abs(btm), ss.str()), llog::Red } << llog::stl_rst
      << llog::tag_t { "flt:" + (ss.str(""), ss << (flt < 0 ? "-" : "+") << std::hex << std::abs(flt), ss.str()), llog::Red } << llog::stl_rst;

   llog::error << llog::bgd_dark_grey << llog::fgd_white << "\n \nbacktrace : \n " << llog::stl_rst;
   for (std::size_t index = 6; index < backtrace_array.size(); ++index) {
      auto const& addr = backtrace_array.at(index);
      llog::error << llog::fgd_light_yellow << "\n" << addr << llog::fgd_white << ": ";

      Dl_info info {};
      if (dladdr(addr, &info)) {
         int status { 0 };
         char *demangled = abi::__cxa_demangle(info.dli_sname, nullptr, 0, &status);
         SCOPE_EXIT([&]() {
            free(demangled);
         });

         llog::error << llog::fgd_light_magenta << std::string(status || !demangled ? (info.dli_sname ? info.dli_sname : "?(" + std::to_string(status) + ")") : demangled);

         if (info.dli_fname) {
            std::stringstream ss;
            ss << addr;
            std::string addr2line = "addr2line " + ss.str() + " -e " + std::string(info.dli_fname);

            std::unique_ptr<FILE, decltype(&pclose)> const pipe(popen(addr2line.c_str(), "r"), pclose);
            if (pipe) {
               std::string result { };
               std::array<char, 128> buff { };

               while (fgets(buff.data(), buff.size(), pipe.get()) != nullptr)
                  result += buff.data();

               std::regex const regex { "([^:]+/)([^/]+):([0-9]+).*" };//todo to be moved in logger
               std::smatch match {};//todo addr2line lib

               if (std::regex_search(result, match, regex) && match.size() == 4) {
                  std::filesystem::path const path { match[1].str() };
                  auto path_str = std::filesystem::relative(path).string();
                  llog::error << "\n\t" << llog::fgd_magenta << (path_str.front() == '.' ? "" : "./") << path_str << llog::fgd_light_green << match[2] << llog::fgd_white << ":" << llog::fgd_cyan << match[3];
               } else
                  llog::error << "\n\t" << llog::fgd_magenta << result;
            }
         } else if (info.dli_fbase) {
            llog::error << "\n\t" << llog::fgd_light_cyan << "0x" << std::hex << (byte(addr) - byte(info.dli_saddr)) << llog::fgd_white << "]";
         }
      } else {
         char** const backtrace_names = backtrace_symbols(backtrace_array.data() + index, 1);
         SCOPE_EXIT([&]() {
            free(backtrace_names);
         });
         llog::error << llog::fgd_light_magenta << llog::stl_bold << backtrace_names[0] ? std::string(backtrace_names[0]) : "?";
      }
   }
   llog::error << llog::bgd_dark_grey << "\n \n \n " << std::endl;


   std::terminate();
}

std::size_t context_t::get_size() const {
   return m_callstack ? m_callstack->m_size : 0;
}

std::size_t context_t::get_old_size() const {
   return m_callstack ? m_callstack->m_old_size : 0;
}

uncatched_exception::uncatched_exception(const std::string& what) :
   m_what(what) {
}

const char* uncatched_exception::what() const _GLIBCXX_TXN_SAFE_DYN _GLIBCXX_NOTHROW {
   return m_what.c_str();
}

thread_local context_t* context_t::m_current_context { nullptr };
thread_local std::vector<std::unique_ptr<callstack_t>> callstack_t::m_cache {};
std::atomic<std::size_t> callstack_t::m_cache_size = 0;
std::atomic<std::size_t> callstack_t::m_cache_peak_size = 0;

