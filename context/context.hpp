#pragma once

#include <allocator/allocator.h>
#include <context/pthread_.h>
#include <logger/logger.h>
#include <utils/utils.hpp>

#include <array>
#include <cstddef>
#include <cxxabi.h>
#include <dlfcn.h>
#include <exception>
#include <functional>
#include <memory>
#include <stdlib.h>
#include <string>
#include <sys/mman.h>
#include <type_traits>
#include <ucontext.h>
#include <unistd.h>
#include <vector>

using address_t = void*;
using registry_t = address_t;

struct context_t;
class callstack_t {
private:
   using fd_t = int;

   static const std::size_t page_size;
   static const std::size_t protect_size;

   struct mapping_t {
      address_t begin;
      address_t end;
   };

   static thread_local std::vector<std::unique_ptr<callstack_t>> m_cache;
   static std::atomic<std::size_t> m_cache_size;
   static std::atomic<std::size_t> m_cache_peak_size;
   alloc_initializer_t m_allocator { nullptr };

   std::size_t m_size { 0 };
   std::size_t m_old_size { 0 };
   address_t m_allocated_ptr { nullptr };
   address_t m_begin { nullptr };
   address_t m_end { nullptr };
   address_t m_stack_top { nullptr };

   pthread* m_thread_ptr { nullptr };
   pthread const* m_caller_thptr { nullptr };

   struct reg_t {
      registry_t rsp { nullptr };
      registry_t rbp { nullptr };
      registry_t rip { nullptr };

      std::array<uint64_t,
#ifdef __aarch64__
                 11
#else
                 10
#endif
                 >
          regs {};

      __attribute__((always_inline)) void set(registry_t const rip);

      __attribute__((always_inline)) void restore() const;
   } m_regs {};

public:
   callstack_t(callstack_t const&) = delete;
   callstack_t(callstack_t&&) = delete;

   callstack_t(std::size_t const size);
   ~callstack_t();

private:
   void unprotect();
   void protect();

   // m_rsp must be set
   void extends(std::size_t overflow_size);

   // m_rsp must be set
   void realloc(std::size_t const size_);

   template<class Arg_>
   inline Arg_* set(Arg_ const& arg, registry_t stack_ptr) const;

   friend class context_t;
};

struct unterminated_context : std::exception {
   using std::exception::exception;
};

struct uncatched_exception : std::exception {
   std::string m_what;

   using std::exception::exception;

   uncatched_exception(const std::string& what = "");

   /** Returns a C-style character string describing the general cause
    *  of the current error.  */
   virtual const char* what() const _GLIBCXX_TXN_SAFE_DYN _GLIBCXX_NOTHROW override;
};

struct stack_overflow : std::exception {
   context_t& context;
   const address_t faulty;

   stack_overflow(context_t& context, const address_t faulty) :
       context(context),
       faulty(faulty) {
   }
};

struct segfault : std::exception {
   context_t& context;

   segfault(context_t& context) :
       context(context) {
   }
};

static volatile uint8_t const fno_dce__ { 1 };
#define fno_dce if (fno_dce__)

#ifdef __aarch64__
#define fp "x29" // @suppress("Obsolete object-like macro")
#define lr "x30" // @suppress("Obsolete object-like macro")

#define label_addr(labelName)                       \
   static address_t const labelName = []() {        \
      address_t label { nullptr };                  \
      asm volatile("ldr %[label], =" #labelName ";" \
                   : [label] "=r"(label));          \
      return label;                                 \
   }();

#define llabel_addr(labelName, index)           \
   static address_t const labelName = []() {    \
      address_t label { nullptr };              \
      asm volatile("ldr %[label], =" #index ";" \
                   : [label] "=r"(label));      \
      return label;                             \
   }()

#else
#define label_addr(labelName)                          \
   static address_t const labelName = []() {           \
      address_t label { nullptr };                     \
      asm volatile("lea " #labelName "(%%rip), %%rax;" \
                   "mov %%rax, %[label];"              \
                   : [label] "=rm"(label)::"rax");     \
      return label;                                    \
   }()

#define llabel_addr(labelName, index) /* @suppress("Unused Macro") // @suppress("Obsolete function-like macro") */ \
   static address_t const labelName = []() {                                                                       \
      address_t label { nullptr };                                                                                 \
      asm volatile("lea " #index "(%%rip), %%rax;"                                                                 \
                   "mov %%rax, %[label];"                                                                          \
                   : [label] "=rm"(label)::"rax");                                                                 \
      return label;                                                                                                \
   }()
#endif

class context_t : public sharable<context_t> {
private:
   static thread_local context_t* m_current_context;

   std::unique_ptr<callstack_t> m_callstack { nullptr };
   std::size_t m_size;

   callstack_t::reg_t m_caller_regs {};

   registry_t* m_caller_stack_rbp { nullptr };
   registry_t* m_caller_stack_rip { nullptr };

   registry_t m_current_rbp { nullptr };
   registry_t m_current_rsp { nullptr };

   bool m_attached { false };
   bool m_terminated { true };

   std::shared_ptr<std::function<void(context_t&)>> m_on_detach { nullptr };
   std::shared_ptr<std::function<void(context_t&)>> m_on_attach { nullptr };

   std::function<void(context_t&)> m_on_unterminated_free;

   context_t(
       std::size_t size = 5000, const std::function<void(context_t&)>& on_unterminated_free = [](context_t&) {
          llog::error << "Deleting unterminated context" << std::endl;
          std::terminate();
       });

   context_t(context_t const&) = delete;
   context_t& operator=(context_t const&) = delete;

public:
   virtual ~context_t();
   using sharable_type_t::create;

   template<class... Args_>
   void start(const std::function<void(context_t&, Args_&...)>& runner, Args_ const&... args);

   bool attached() const;
   bool terminated() const;

   static std::shared_ptr<context_t> current_context();

   void const* getRsp() const;
   void const* getRbp() const;
   void const* getTop() const;
   void const* getBottom() const;

   std::size_t get_size() const;
   std::size_t get_old_size() const;

private:
   template<class... Types_>
   inline void destroy(Types_&&... elem) const;

   template<class... Args_>
   void init(const std::function<void(context_t&, Args_&...)>* runner, Args_*... args);

   template<class... Args_>
   void main(const std::function<void(context_t&, Args_&...)>& runner, Args_ const&... args);

   __attribute__((always_inline)) inline registry_t const& caller_rip() const {
      return m_caller_regs.rip;
   }
   __attribute__((always_inline)) inline registry_t const& caller_rbp() const {
      return m_caller_regs.rbp;
   }

   __attribute__((always_inline)) inline registry_t const& caller_rip(registry_t const caller_rip) {
      return m_caller_regs.rip = m_caller_stack_rip[0] = caller_rip;
   }
   __attribute__((always_inline)) inline registry_t const& caller_rbp(registry_t const caller_rbp) {
      return m_caller_regs.rbp = m_caller_stack_rbp[0] = caller_rbp;
   }

public:
   void panic_mode(registry_t faulty, ucontext_t& ucontext);

   void throw_inside(const auto&);
   void throw_outside(const auto&);

private:
   template<class Arg_>
   inline Arg_* set(Arg_ const& arg, registry_t stack_ptr) const;

   inline void copy_function_epilog_to_context(void const* const rip);

public:
   void attach();
   void detach();

private:
   template<class... Args_>
   inline void copy_to_context(Args_ const&... args) const;

   template<class Args_>
   inline void copy_to_context(Args_ const& arg) const;

   template<class Arg_>
   inline Arg_* get_context_addr(Arg_ const& arg) const;

   static inline void get_context(registry_t& rsp, registry_t& rbp);

   friend sharable_type_t;
};
