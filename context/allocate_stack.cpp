#include "pthread_.h"

#include <stddef.h>
#include <assert.h>
#include <errno.h>
#include <signal.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/param.h>

#include <logger/logger.h>

extern "C" {
/* Initializer for lock.  */
#define LLL_LOCK_INITIALIZER                (0)
#define LLL_LOCK_INITIALIZER_LOCKED        (1)
#define LLL_LOCK_INITIALIZER_WAITERS        (2)

/* Define a variable with the head and tail of the list.  */
#define LIST_HEAD(name) \
  list_t name = { &(name), &(name) }

/* Default alignment of stack.  */
#ifndef STACK_ALIGN
# define STACK_ALIGN __alignof__ (long double)
#endif

/* Default value for minimal stack size after allocating thread
 descriptor and guard.  */
#ifndef MINIMAL_REST_STACK
# define MINIMAL_REST_STACK        4096
#endif

void _dl_get_tls_static_info (size_t *sizep, size_t *alignp);

/* Returns a usable stack for a new thread either by allocating a
 new stack or reusing a cached stack of sufficient size.
 ATTR must be non-NULL and point to a valid pthread_attr.
 PDP must be non-NULL.  */
int allocate_stack(const struct pthread_attr* attr, struct pthread** pdp, void **stack) {
   struct pthread* pd;
   size_t size;
   size_t pagesize_m1 = getpagesize() - 1;
   assert(powerof2 (pagesize_m1 + 1));
   assert(TCB_ALIGNMENT >= STACK_ALIGN);

   size = attr->stacksize;

   uintptr_t adj;
   char* stackaddr = (char*) attr->stackaddr;

   /* Assume the same layout as the _STACK_GROWS_DOWN case, with struct
    pthread at the top of the stack block.  Later we adjust the guard
    location and stack address to match the _STACK_GROWS_UP case.  */
   if (_STACK_GROWS_UP)
      stackaddr += attr->stacksize;

   size_t __static_tls_size {};
   size_t static_tls_align {};
   size_t __static_tls_align_m1 {};
   _dl_get_tls_static_info (&__static_tls_size, &static_tls_align);
   /* Make sure the size takes all the alignments into account.  */
   if (STACK_ALIGN > static_tls_align)
     static_tls_align = STACK_ALIGN;
   __static_tls_align_m1 = static_tls_align - 1;
   __static_tls_size = roundup (__static_tls_size, static_tls_align);

   /* If the user also specified the size of the stack make sure it
    is large enough.  */
   if (attr->stacksize != 0 && attr->stacksize < (__static_tls_size + MINIMAL_REST_STACK))
      return EINVAL;
   /* Adjust stack size for alignment of the TLS block.  */
#if TLS_TCB_AT_TP
   adj = ((uintptr_t) stackaddr - TLS_TCB_SIZE) & __static_tls_align_m1;
   assert(size > adj + TLS_TCB_SIZE);
#elif TLS_DTV_AT_TP
      adj = ((uintptr_t) stackaddr - __static_tls_size)
            & __static_tls_align_m1;
      assert (size > adj);
#endif
   /* The user provided some memory.  Let's hope it matches the
    size...  We do not allocate guard pages if the user provided
    the stack.  It is the user's responsibility to do this if it
    is wanted.  */
#if TLS_TCB_AT_TP
   pd = (struct pthread*) ((uintptr_t) stackaddr - TLS_TCB_SIZE - adj);
#elif TLS_DTV_AT_TP
      pd = (struct pthread *) (((uintptr_t) stackaddr
                                - __static_tls_size - adj)
                               - TLS_PRE_TCB_SIZE);
#endif
   /* The user provided stack memory needs to be cleared.  */
   memset(pd, '\0', sizeof(struct pthread));
   /* The first TSD block is included in the TCB.  */
   pd->specific[0] = pd->specific_1stblock;
   /* Remember the stack-related values.  */
   pd->stackblock = (char*) stackaddr - size;
   pd->stackblock_size = size;
   /* This is a user-provided stack.  It will not be queued in the
    stack cache nor will the memory (except the TLS memory) be freed.  */
   pd->user_stack = true;
   /* This is at least the second thread.  */
   pd->header.multiple_threads = 1;
#ifndef TLS_MULTIPLE_THREADS_IN_TCB
//   __pthread_multiple_threads = *__libc_multiple_threads_ptr = 1;
#endif
#ifdef NEED_DL_SYSINFO
      SETUP_THREAD_SYSINFO (pd);
#endif
   /* Don't allow setxid until cloned.  */
   pd->setxid_futex = 0;
   /* Allocate the DTV for this thread.  */
   if (_dl_allocate_tls(TLS_TPADJ(pd)) == nullptr) {
      /* Something went wrong.  */
      assert(errno == ENOMEM);
      return errno;
   }

   /* Initialize the lock.  We have to do this unconditionally since the
    stillborn thread could be canceled while the lock is taken.  */
   pd->lock = LLL_LOCK_INITIALIZER;
   /* The robust mutex lists also need to be initialized
    unconditionally because the cleanup for the previous stack owner
    might have happened in the kernel.  */
   pd->robust_head.futex_offset = (offsetof(pthread_mutex_t, __data.__lock) - offsetof(pthread_mutex_t, __data.__list.__next));
   pd->robust_head.list_op_pending = nullptr;

#if __PTHREAD_MUTEX_HAVE_PREV
   pd->robust_prev = &pd->robust_head;
#endif

   pd->robust_head.list = &pd->robust_head;

   /* We place the thread descriptor at the end of the stack.  */
   *pdp = pd;

#if _STACK_GROWS_DOWN
   void* stacktop;

# if TLS_TCB_AT_TP
   /* The stack begins before the TCB and the static TLS block.  */
   stacktop = ((char*) (pd + 1) - __static_tls_size);
# elif TLS_DTV_AT_TP
  stacktop = (char *) (pd - 1);
# endif

# ifdef NEED_SEPARATE_REGISTER_STACK
  *stack = pd->stackblock;
  *stacksize = stacktop - *stack;
# else
   *stack = stacktop;
# endif

#else
  *stack = pd->stackblock;
#endif
   return 0;
}

void _dl_deallocate_tls (void *tcb, bool dealloc_tcb);
void __deallocate_stack(struct pthread* pd) {
   /* Free the memory associated with the ELF TLS.  */
      _dl_deallocate_tls(TLS_TPADJ(pd), false);
}

}

#ifdef __aarch64__
void* sys_x64_get_fs() {
   void* fs = nullptr;

   asm volatile(
      "mrs %0,tpidr_el0"
      : "=r"(fs)
   );

   return fs;
}

int64_t sys_x64_set_fs(void const* const value) {
   asm volatile(
      "msr tpidr_el0,%0"
      :: "r"(value)
   );

   return 0;
}
#else
void* sys_x64_get_fs() {
   void* fs = nullptr;
   int64_t err_code = 0;

   register uint64_t const rdi asm("rdi") = static_cast<uint64_t>(0x1003);
   register uint64_t const rsi asm("rsi") = reinterpret_cast<uint64_t>(&fs);

   asm volatile(
      "syscall;"
      : "=a"(err_code)
      : "0"(SYS_x64_getsetfsgs), "r"(rdi), "r"(rsi)
      : CLOBBERED_BY_SYSCALL
   );

   if (err_code)
      return nullptr;

   return fs;
}

int64_t sys_x64_set_fs(void const* const value) {
   int64_t err_code = 0;

   register uint64_t const rdi asm("rdi") = static_cast<uint64_t>(0x1002);
   register uint64_t const rsi asm("rsi") = reinterpret_cast<uint64_t>(value);

   asm volatile(
      "syscall;"
      : "=a"(err_code)
      : "0"(SYS_x64_getsetfsgs), "r"(rdi), "r"(rsi)
      : CLOBBERED_BY_SYSCALL
   );

   return err_code;
}
#endif
